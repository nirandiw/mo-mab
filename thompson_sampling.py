import random

__author__ = 'nirandikawanigasekara'

import numpy as np
import matplotlib.pyplot as py
import pandas as pd

dataset = pd.read_csv("xyv")

import math

N =10000
d =10
selected_ads =[]
number_of_rewrds_1 =[0]*d
number_of_rewrds_0 = [0]*d

total_reward =0
for n in range(0,N):
    ad =0
    maximum_random_draw =0
    for i in range(0,d):
        random_beta = random.betavariate(number_of_rewrds_1[i]+1, number_of_rewrds_0[i]+1)
        if(maximum_random_draw < random_beta):
            maximum_random_draw = random_beta;
            ad = i
    selected_ads.append(maximum_random_draw)
    reward = dataset.values[n, ad]
    if reward == 0:
        number_of_rewrds_0[ad] += 1
    else:
        number_of_rewrds_1[ad] +=1
    total_reward += reward
