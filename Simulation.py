import random
import TripAdvisorModule.Constants as cnst
import numpy as np


class Simulation:

    def __init__(self, seed):
        if seed ==-1:
            self.prng = np.random.RandomState()
        else:
            self.prng = np.random.RandomState(seed)
            random.seed(seed)

    def make_rand_vector(self, dims):
        vec = [random.gauss(0, 1) for i in range(dims)]
        mag = sum(x ** 2 for x in vec) ** .5
        return [x / mag for x in vec]

    def get_optimal_coefficient_vector(self):
        dict_m = {}
        dict_c = {}
        for arm in range(0, cnst.ARMS_COUNT):
            dict_key = arm
            m = self.prng.uniform(cnst.linear_m_lower_bound, cnst.linear_m_upper_bound,(cnst.OBJECTIVES_i,
                                                                                        cnst.OBJECTIVES_CONTEXT_DIMS_L))
            c = self.prng.uniform(cnst.linear_c_lower_bound, cnst.linear_c_upper_bound, cnst.OBJECTIVES_i)

            dict_m[dict_key] = m
            dict_c[dict_key] = c

        print("Theta* ", dict_m, dict_c)
        return dict_m, dict_c

    def get_optimal_user_coefficient_vectors(self):
        dict_beta_stars = {}
        for user in range(0, cnst.NO_OF_USER):
            dict_key = user
            list_of_coeffcients = self.make_rand_vector(cnst.OBJECTIVES_i)
            dict_beta_stars[dict_key] = list_of_coeffcients
        print("Beta* ", dict_beta_stars)
        return dict_beta_stars

    def sample_context_vector(self):
        return self.prng.uniform(0.0, 1.0, (cnst.OBJECTIVES_i, cnst.OBJECTIVES_CONTEXT_DIMS_L))

    def get_random_val_from_list(self, lst):
        return self.prng.choice(lst)

    def get_random_coefficient_vector(self, dim):
        return self.prng.rand(dim)

    def get_random_uniform(self, s=cnst.error_lower_bound, e=cnst.error_upper_bound):
        return self.prng.uniform(s, e)




