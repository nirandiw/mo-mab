import pandas as pd
from sklearn import preprocessing
import MORLogUCB_Tripadvisor
import MOR_LinUCB_wop_Simulation_Tripadvisor
import ParetoUCB1_Simulation_Tripadvisor
import Ranked_LinUCB_Tripadvisor
import Constants as cnst
import numpy as np
# from plotting_scripts import plot_1
import Epsilon_greegy_Simulation
import json
import datetime

scalar = preprocessing.MinMaxScaler()

# Loading the sequence of data
df_sequence = pd.read_json("./data//relevant_country_filled_reduced.json", orient='index')

# Processing the user data
df_sequence[['a_num_helpful_votes', 'num_cities', 'num_reviews', 'num_type_reviews', 'via_mobile']] = df_sequence[
    ['a_num_helpful_votes', 'num_cities', 'num_reviews', 'num_type_reviews', 'via_mobile']].fillna(value=0)
df_sequence[
    ['a_num_helpful_votes', 'num_cities', 'num_reviews', 'num_type_reviews', 'via_mobile']] = scalar.fit_transform(
    df_sequence[['a_num_helpful_votes', 'num_cities', 'num_reviews', 'num_type_reviews', 'via_mobile']])
df_user_embedding = pd.read_csv("./data//user_embedding.txt", delim_whitespace=True, header=None, index_col=0)

# generating hotel features
df_hotel_embedding = pd.read_csv("./data//hotel_embedding.txt", delim_whitespace=True, header=None, index_col=0)
df_hotels = pd.read_json('./data//offering_features_short.json', orient='index')
# df_hotels.index = df_hotels.index.map(int)
df_hotels.columns = ['raw1', 'raw2']
df_hotels = pd.concat((df_hotels, df_hotel_embedding), axis=1)
assert df_hotels.shape[0] == df_hotels.shape[0]
dict_hotel_features = df_hotels.T.to_dict('list')

# loading location embedding
df_location_embedding = pd.read_csv("./data//country_embedding.txt", delim_whitespace=True, header=None, index_col=0)

# Loading user rewards for each hotel
df_rewards = pd.read_json('./data//rewards.json', orient='index').fillna(value=0)

# Loading user clusters
df_user_clusters = pd.read_json('./data/df_user_clusters.json', orient='index')

for s in range(0, 2):
    # Algorithm initialization
    morlogucb = MORLogUCB_Tripadvisor.MORLogUCB(df_hotels.index.values)
    morlinucb_wop = MOR_LinUCB_wop_Simulation_Tripadvisor.MORLinUCBwopSim(df_hotels.index.values)
    paretoucb = ParetoUCB1_Simulation_Tripadvisor.ParetoUCB1Sim(df_hotels.index.values)
    sorlinucb = Ranked_LinUCB_Tripadvisor.SORSim(df_hotels.index.values, 1)
    epsilongreedy = Epsilon_greegy_Simulation.EpsilonGreedySim(df_hotels.index.values, 0.5)

    algos = [#morlinucb_wop,
             sorlinucb,
             paretoucb,
             morlogucb, epsilongreedy
             ]

    a_id_seq = []
    algo_seq = []
    rank1_seq = []
    rank2_seq = []

    for t in range(0, cnst.T):
        print("Trial: ", t)
        review_t = df_sequence.sample(1)
        a_id = review_t['a_id'].values[0]
        if a_id == 'CATID_':
            print('continue')
            continue

        user_cluster_id = df_user_clusters.loc[a_id, 0]
        item_id = review_t.loc[:'offering_id']
        a_loc = review_t['country'].values[0]
        a_loc_embedding = np.array(cnst.country_code[a_loc]).reshape(1,-1) # df_location_embedding.loc[a_loc].values.reshape(1, -1)

        features = np.concatenate(
            (review_t[['a_num_helpful_votes', 'num_cities', 'num_reviews', 'num_type_reviews', 'via_mobile']].values,
             df_user_embedding.loc[a_id].values.reshape(1, -1), a_loc_embedding), axis=1)

        x_list = [features[0] for _ in range(0, cnst.OBJECTIVES_i)]

        idx_obj = 0
        exp = False
        if t < cnst.TAU * cnst.OBJECTIVES_i:
            exp = True
            idx_obj = t / cnst.TAU

        for algo in algos:
            ranked_list = algo.recommend(x_list, user_cluster_id, dict_hotel_features, [exp, idx_obj])

            a_id_seq.append(a_id)
            algo_seq.append(algo.name)
            rank1_seq.append(int(ranked_list[0].keys()[0]))
            rank2_seq.append(int(ranked_list[1].keys()[0]))

            df_user_rewards = df_rewards[df_rewards['a_id'] == a_id]
            rank = 0
            print("User id : ", a_id, "Cluster id : ", user_cluster_id)
            for dict_item in ranked_list:
                recommended_a = dict_item.keys()[0]
                if recommended_a is None:
                    print('check recommended_a is none')
                df_user_offering_rates = df_user_rewards[df_user_rewards['offering_id'] == int(recommended_a)]

                is_click = 0
                array_rates = [0,0,0,0,0]#algo.estimated_rewards_t[recommended_a]
                if not df_user_offering_rates.empty:
                    array_rates = df_user_offering_rates[
                        [# 'business_service_(e_g_internet_access)', 'check_in_front_desk',
                         'cleanliness',
                         'location_rating', 'rooms', 'service',
                         # 'sleep_quality',
                         'value']].iloc[0].values
                    if df_user_offering_rates[['overall']].iloc[0].values[0] >= 2.5:
                        is_click = 1
                    print(array_rates)
                    if algo.name == "MOU-UCB ":
                        algo.estimated_reward_for_recommened_a.append(algo.estimated_rewards_t[recommended_a].tolist())
                        algo.real_reward_for_recommended_a.append(array_rates.tolist())
                algo.update(rank, dict_item, array_rates, is_click, x_list, dict_hotel_features, t, user_cluster_id)
                rank = rank + 1

    for algo in algos:
        algo.dump_results()
        algo.get_ctr()
    logdetail = {'user_id':a_id_seq,
                 'algo': algo_seq,
                 'rank1': rank1_seq,
                 'rank2': rank2_seq}
    filename = str(datetime.datetime.now().strftime('%Y-%m-%d-%H-%M-%S_{fname}').format(fname="rankedlist.txt"))
    with open(filename, 'a') as f:
        json.dump(logdetail, f)
    f.close()
