import matlab.engine
import Constants as cnst
import math
import json
import datetime
import matlab.engine
import numpy as np


class ParetoUCB1ArmStruct:

    def __init__(self, item_ids):
        self.items = item_ids
        self.counts = [0 for col in range(len(self.items))]
        self.values = [[0.0] * cnst.OBJECTIVES_i for col in range(len(self.items))]
        self.item_to_keynum_map = ['' for n in range(len(self.items))]
        for key_num, id in enumerate(self.items):
            self.item_to_keynum_map[key_num] = id

    def select_arm(self, eng, unselelected_ids):
        n_arms = len(self.items)
        for arm in range(len(self.items)):
            if self.counts[arm] == 0 and self.item_to_keynum_map[arm] in unselelected_ids:
                # print arm
                return self.item_to_keynum_map[arm]

        ucb_values = [[0.0] * cnst.OBJECTIVES_i for arm in range(n_arms)]
        total_counts = sum(self.counts)
        for arm in range(n_arms):
            if self.counts[arm] > 0:
                # bonus = math.sqrt((2 * math.log(total_counts * ((cnst.OBJECTIVES_i * len(self.items)) ** (0.25)))) / float(
                bonus = math.sqrt(
                    (2 * math.log(cnst.T * ((cnst.OBJECTIVES_i * len(self.items)) ** (0.25)))) / float(
                self.counts[arm]))
            else:
                assert self.item_to_keynum_map[arm] not in unselelected_ids
                bonus = 0
            ucb_values[arm] = map(lambda v: v + bonus, self.values[arm])

        idx_pareto_arms = self.pareto_front(ucb_values, eng)
        pareto_ids =[]
        for idx in idx_pareto_arms:
            pareto_ids.append(self.item_to_keynum_map[int(idx)])
        pesudo_pareto_front = list(set(unselelected_ids).intersection(pareto_ids))
        assert len(pareto_ids) == len(set(pareto_ids))
        # print("pareto indexs must be unique ", pareto_ids)
        # print(pesudo_pareto_front)
        # print(unselelected_ids)

        if len(pesudo_pareto_front) >0:
            picked_arm = np.random.choice(pesudo_pareto_front)
        else:
            picked_arm = np.random.choice(unselelected_ids)
        #print(picked_arm)
        return picked_arm

    def update(self, chosen_arm_name, reward):
        chosen_arm = self.item_to_keynum_map.index(chosen_arm_name)
        self.counts[chosen_arm] += 1
        n = self.counts[chosen_arm]
        value = self.values[chosen_arm]
        new_value = []
        for indx, v in enumerate(value):
            new_value.append((n - 1) / float(n) * v + (1 / float(n)) * reward[indx])
        self.values[chosen_arm] = new_value

    def pareto_front(self, O_t, eng):  # TODO : implement Pareto front
        assert len(O_t) == len(self.items)
        matlab_O_t = matlab.double(O_t)
        pareto_front = eng.paretoFront(matlab_O_t)  # indexed from 1
        # print("A_prime", np.array(pareto_front))
        # if not isinstance(pareto_front, list):
        #    return pareto_front - 1.0
        # else:
        return np.subtract(np.array(pareto_front).flatten(),1.0)


class ParetoUCB1RankedBanditAlgorithm:
    def __init__(self, pos, item_ids):
        self.K = pos
        self.dict_MABs = {}
        self.items = item_ids
        for k in range(0, self.K):
            self.dict_MABs[k] = ParetoUCB1ArmStruct(item_ids)

    def get_a_k(self, k, ranked_a_list, matlab_engine):
        paretoucb1_k = self.dict_MABs[k]
        ranked_arms = set().union(*(a.keys() for a in ranked_a_list))
        dict_a_k = {}
        f = 0
        unselected_a_ids = list(set(self.items) - ranked_arms)
        a_k_hat = paretoucb1_k.select_arm(matlab_engine, unselected_a_ids)  # TODO change for many users

        #if a_k_hat in ranked_arms:
        #    a_k = np.random.choice(unselected_a)
        #else:
        a_k = a_k_hat
        f = 1
        dict_a_k[a_k] = f
        return dict_a_k

    def update_mab_k(self, arm, click_reward_boolean, k):
        paretoucb1_k = self.dict_MABs[k]
        paretoucb1_k.update(arm, click_reward_boolean)


class ParetoUCB1Sim:

    def __init__(self, item_ids):
        self.name = "PARETOUCB1 "
        self.eng = matlab.engine.start_matlab()

        self.article_click_count = [0]
        self.RBA_position_click_count = [[0] * cnst.RANKING_POSITIONS for _ in range(0, cnst.T)]
        self.click_position_count = [[0] * cnst.RANKING_POSITIONS for _ in range(0, cnst.T)]

        self.ranker = ParetoUCB1RankedBanditAlgorithm(cnst.RANKING_POSITIONS, item_ids)

        self.estimated_rewards_t = {}
        for item in item_ids:
            self.estimated_rewards_t[item] = np.zeros(cnst.OBJECTIVES_i)

    def recommend(self, x_list, user_id, dict_arm_features, isexplore):
        A_tilde = []
        for k in range(0, cnst.RANKING_POSITIONS):
            a_k = self.ranker.get_a_k(k, A_tilde, self.eng)
            A_tilde.append(a_k)

        print(self.name, A_tilde)
        return A_tilde

    def update(self, k_rank, a, r_a, is_click, x_list, dict_arm_features, t, user_id):
        arm_id = a.keys()[0]
        if is_click == 1:
            self.click_position_count[t][k_rank] = 1
            if a[arm_id] == 1:
                f_k = 1
                self.RBA_position_click_count[t][k_rank] += 1

        self.ranker.update_mab_k(int(arm_id), r_a, k_rank)

    def get_ctr(self):
        return cnst.calculate_ctr(self.click_position_count, cnst.RANKING_POSITIONS)

    def dump_results(self):

        filename = str(datetime.datetime.now().strftime('%Y-%m-%d-%H-%M-%S_{fname}').format(fname="ParetoUCB1.txt"))
        with open(filename, 'w') as f:
            f.write(json.dumps({
                'ParetoUCB1_RBA': self.RBA_position_click_count,
                'ParetoUCB1_click_position': self.click_position_count,
                'ParetoUCB1_click_count': self.article_click_count,
                'ParetoUCB1_ctr': self.get_ctr(),
                '#Objectives': cnst.OBJECTIVES_i,
                'ContextDim': cnst.OBJECTIVES_CONTEXT_DIMS_L,
                'Arms': cnst.ARMS_COUNT,
                'RankingPositions': cnst.RANKING_POSITIONS,
                'User': cnst.NO_OF_USER,
                'Lambda': cnst.LAMBDA,
                'Trials': cnst.T,
                'mor_lin_ucb_alpha': cnst.mor_lin_ucb_alpha,
                'sor_lin_ucb_alpha': cnst.sor_lin_ucb_alpha,
                'Beta': cnst.BETA}))
