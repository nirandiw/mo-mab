import math
# from util_functions import vectorize, matrixize
from sklearn.linear_model import LogisticRegression
import numpy as np
import Constants as cnst


def sigmoid(x):
    return 1 / (1 + math.exp(-x))


class LogisticUserStruct:
    def __init__(self, featureDimension, userID, lambda_, RankoneInverse):
        self.userID = userID
        self.A = lambda_ * np.identity(n=featureDimension)

        self.AInv = np.linalg.inv(self.A)
        self.UserTheta = np.random.rand(featureDimension)
        self.RankoneInverse = RankoneInverse

        self.totalCount = 0
        self.totalClick = 0
        self.CTR = 0.0
        #self.alpha = cnst.BETA

        self.LogiModel = LogisticRegression(solver='saga')
        self.X = []
        self.Y = []

    def updateParameters(self, articlePicked, click):
        #self.totalCount += 1
        #if click == 1:
        #    self.totalClick += 1
        # self.CTR = float(self.totalClick) / self.totalCount

       # self.alpha = 0.1 * np.sqrt(math.log(self.totalCount + 1))

        self.X.append(articlePicked)
        self.Y.append(click)

        featureVector = articlePicked
        self.A += np.outer(featureVector, featureVector)

    def updateAInv(self, featureVector):
        if self.RankoneInverse:
            temp = np.dot(self.AInv, featureVector)
            self.AInv = self.AInv - (np.outer(temp, temp)) / (1.0 + np.dot(np.transpose(featureVector), temp))
        else:
            self.AInv = np.linalg.inv(self.A)

    def getVar(self, featureVector):
        var = np.sqrt(np.dot(np.dot(featureVector, self.AInv), featureVector))
        if math.isnan(var):
            print("check")
        return var

    def getClickProb_arr(self, alpha, users, article_Feature_arr):
        if len(self.X) == 0:
            ClickProb_arr = np.zeros(len(article_Feature_arr))
        else:
            if 1 not in self.Y:
                ClickProb_arr = np.zeros(len(article_Feature_arr))
            elif 0 not in self.Y:
                ClickProb_arr = np.ones(len(article_Feature_arr))
            else:
                self.LogiModel.fit(self.X, self.Y)
                # print(article_Feature_arr)
                Prob_arr = self.LogiModel.predict_proba(article_Feature_arr)
                ClassList = self.LogiModel.classes_.tolist()
                ClickIndex = ClassList.index(1)
                ClickProb_arr = Prob_arr.T[ClickIndex]
            # print ClickProb_arr
        return ClickProb_arr


class reward_GLMUCBAlgorithm:
    def __init__(self, dimension, beta, lambda_, usealphaT=False, RankoneInverse=False,):
        self.users = {}
        self.dimension = dimension
        self.beta = beta
        self.lambda_ = lambda_

        self.usealphaT = usealphaT
        self.RankoneInverse = RankoneInverse
        self.CanEstimateUserPreference = True
        self.CanEstimateReturn = False

    def decide(self, pool_articles_ids, userID, est_rewards):
        if userID not in self.users:
            self.users[userID] = LogisticUserStruct(self.dimension, userID, self.lambda_, self.RankoneInverse)

        maxUCB = float('-inf')
        articlePicked = None
        returnProb = 1.0
        ClickProb_arr = self.users[userID].getClickProb_arr(self.beta, self.users, est_rewards.values())

        if self.usealphaT:
            self.beta = self.users[userID].alpha
        # print self.alpha
        i = 0
        for x in pool_articles_ids:
            articleFeature = est_rewards[x]
            # get mean of click probability
            ClickProb = ClickProb_arr[i]
            i += 1
            # get variance of click probability
            var = self.users[userID].getVar(articleFeature)
            # get ucb of click probabilit
            click_ucb = ClickProb + self.beta * var
            if maxUCB < click_ucb:
                articlePicked = x
                maxUCB = click_ucb
        if articlePicked is None:
            print("Picked article in None! Check")
        return articlePicked

    def updateParameters(self, articlePicked, click, userID):
        if userID not in self.users:
            self.users[userID] = LogisticUserStruct(self.dimension, userID, self.lambda_, self.RankoneInverse)
        self.users[userID].updateParameters(articlePicked, click)
        self.users[userID].updateAInv(articlePicked)

    def getTheta(self, userID):
        return self.users[userID].UserTheta


class RankedBanditLogUCBAlgorithm:
    def __init__(self, pos):
        self.K = pos
        self.dict_MABs = {}
        #self.sim = sim_
        for k in range(0, self.K):
            self.dict_MABs[k] = reward_GLMUCBAlgorithm(cnst.OBJECTIVES_i, cnst.BETA, cnst.LAMBDA)

    def get_a_k_log(self, k, ranked_a_list, estimated_rewards, user_id, dict_article_pool):
        if dict_article_pool is None:
            article_pool_ids = range(0, cnst.ARMS_COUNT)
        else:
            article_pool_ids = dict_article_pool.keys()

        linucb_k = self.dict_MABs[k]
        ranked_arms = set().union(*(a.keys() for a in ranked_a_list))
        dict_a_k = {}
        f = 0

        unselected_a_ids = list(set(article_pool_ids).symmetric_difference(ranked_arms))
        a_k_hat = linucb_k.decide(unselected_a_ids, user_id, estimated_rewards)


        #if a_k_hat in ranked_arms:
        #    a_k = np.random.choice(unselected_a)#self.sim.get_random_val_from_list(unselected_a)
        #    assert a_k != a_k_hat
        #else:
        a_k = a_k_hat
        f = 1
        dict_a_k[a_k] = f
        return dict_a_k

    def update_mab_k_wop(self, reward_vector, click_reward_boolean, k, user_id):
        linucb_k = self.dict_MABs[k]
        linucb_k.updateParameters(reward_vector, click_reward_boolean, user_id)
