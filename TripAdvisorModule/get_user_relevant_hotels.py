import pandas as pd
import json

df_sequence = pd.read_json("./data//relevant_country_filled_reduced.json", orient='index')
user_ids = df_sequence['a_id'].unique()

user_relevant_hotels = {}

for user in user_ids:
    user_relevant_hotels[user] = df_sequence[df_sequence['a_id'] == user]['offering_id'].unique().tolist()

with open("./data//user_relevant_hotel.json",'w') as f:
    json.dump(user_relevant_hotels, f)
f.close()