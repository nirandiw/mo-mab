import math

import numpy as np
import Constants as cnst


class LinUCBArmStruct:
    def __init__(self, feature_dimension, lambda_, _id, init="random"):
        self.d = feature_dimension
        self.A = [lambda_ * np.identity(n=self.d) for _ in range(0,cnst.OBJECTIVES_i)]
        self.b = np.zeros((cnst.OBJECTIVES_i, self.d))
        self.AInv = [np.linalg.inv(a) for a in self.A]

        if init == "random":
            self.ArmTheta = np.random.rand(cnst.OBJECTIVES_i, self.d)
        else:
            self.ArmTheta = np.zeros((cnst.OBJECTIVES_i, self.d))
        self.time = 0
        self.id = _id

    def update_parameters(self, x_list, click):
        rewards = np.copy(click)
        if x_list.shape[0] != rewards.shape[0]:
            print("Shapes dont match ")

        self.b = np.add(self.b, np.multiply(x_list, rewards.reshape(-1,1)))
        # print(self.b.shape)
        for i in range(0, cnst.OBJECTIVES_i):
            self.A[i] = np.add(self.A[i], np.outer(x_list[i], x_list[i]))
            self.AInv[i] = np.linalg.inv(self.A[i])
            self.ArmTheta[i] = np.dot(self.AInv[i], self.b[i])
        self.time += 1

        assert len(self.A) == len(self.AInv)
        assert len(self.A) == self.b.shape[0]
        assert self.A[0].shape == self.AInv[0].shape


    #def update_time(self):
    #    self.time += 1

    # def getTheta(self):
    #    return self.ArmTheta

    # def getA(self):
    #    return self.A

    def getProb(self, alpha, x_list):
        if alpha == -1:
            alpha = 0.1 * np.sqrt(np.log(self.time + 1))  # check if the log value is correct
        mean = np.zeros((cnst.OBJECTIVES_i))
        var = np.zeros((cnst.OBJECTIVES_i))

        for obj in range(0, cnst.OBJECTIVES_i):
            mean[obj] = np.dot(x_list[obj], self.ArmTheta[obj])
            var[obj] = np.sqrt(np.dot(np.dot(x_list[obj], self.AInv[obj]), x_list[obj].T))
            if math.isnan(var[obj]) or math.isnan(mean[obj]):
                print('Mean or Variance is Nan')
                exit(-1)
        pta = mean + alpha * var

        return pta, mean, alpha * var


class LinUCBMultiObjectiveAlgorithm:
    def __init__(self, item_ids):
        self.alpha = cnst.mor_lin_ucb_alpha
        self.ARMS = {}
        if item_ids is not None:
            for item_id in item_ids:
                self.ARMS[item_id] = LinUCBArmStruct(cnst.OBJECTIVES_CONTEXT_DIMS_L, cnst.LAMBDA, item_id)
        else:
            for id in range(0, cnst.ARMS_COUNT):
                self.ARMS[id] = LinUCBArmStruct(cnst.OBJECTIVES_CONTEXT_DIMS_L, cnst.LAMBDA, id)

    def get_estimated_rewards_for_all_arms(self, x_list, dict_arm_featurs):

        est_rewards = {} # key = arm id , value = list of rewards for each objective#

        for a in self.ARMS.values():
            if dict_arm_featurs is not None:
                x_list_new = self.get_out_product_feature(dict_arm_featurs[a.id], x_list)
            else:
                x_list_new = x_list
            rewards, means, variances = a.getProb(self.alpha, x_list_new)
            if len(rewards) != cnst.OBJECTIVES_i:
                print("Rewards vector larger than number of objectives")

            for r in rewards:
                if math.isnan(r):
                    print('NaN value is estimated rewards')
                    exit(-1)
            est_rewards[a.id] = rewards

        return est_rewards

    def update_multiobjective_linucb(self, arm_id, x_list, r_a, dict_arm_features):
            temp = self.ARMS[arm_id]
            x_list_new = x_list
            if dict_arm_features is not None:
                x_list_new = self.get_out_product_feature(dict_arm_features[temp.id], x_list)
            temp.update_parameters(x_list_new, r_a)

    def get_out_product_feature(self, arm_features, old_x_list):
        new_x_list = []
        for i in range(0, cnst.OBJECTIVES_i):
            # new_x_list.append(np.outer(old_x_list[i], arm_features).flatten())
            new_x_list.append(np.concatenate((old_x_list[i], arm_features)))
        return np.array(new_x_list)



