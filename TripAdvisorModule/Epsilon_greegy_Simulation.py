import Constants as cnst
import json
import datetime
import numpy as np
import random


class EpsilonGreedy():
    def __init__(self, itemids, epsilon):
        self.items = itemids
        self.epsilon = epsilon # probability of explore
        self.counts = [0 for col in range(len(self.items))]
        self.values = [0.0 for col in range(len(self.items))]
        self.item_to_keynum_map = ['' for n in range(len(self.items))]
        for key_num, id in enumerate(self.items):
            self.item_to_keynum_map[key_num] = id

    def ind_max(self, x):
        m = max(x)
        return x.index(m)

    def select_arm(self, unselected):
        check =True
        while check:
            if random.random() > self.epsilon:
                idx = self.ind_max(self.values)
            else:
                idx = random.randrange(len(self.values))
            arm = self.item_to_keynum_map[idx]
            if arm in unselected:
                check = False
        return arm

    def update(self, chosen_arm_name, reward):
        chosen_arm = self.item_to_keynum_map.index(chosen_arm_name)
        self.counts[chosen_arm] = self.counts[chosen_arm] + 1
        n = self.counts[chosen_arm]

        value = self.values[chosen_arm]
        new_value = ((n-1) / float(n)) * value + (1 / float(n)) * reward # weighted average of the previously estimated value and the reward we just received
        self.values[chosen_arm] = new_value
        return


class EpsilonGreedyRankedBanditAlgorithm:
    def __init__(self, pos, item_ids, epsilon):
        self.K = pos
        self.dict_MABs = {}
        self.items = item_ids
        for k in range(0, self.K):
            self.dict_MABs[k] = EpsilonGreedy(item_ids, epsilon)

    def get_a_k(self, k, ranked_a_list):
        epsilongreedy_k = self.dict_MABs[k]
        ranked_arms = set().union(*(a.keys() for a in ranked_a_list))
        dict_a_k = {}
        f = 0
        unselected_a_ids = list(set(self.items) - ranked_arms)
        a_k_hat = epsilongreedy_k.select_arm(unselected_a_ids)  # TODO change for many users


        #if a_k_hat in ranked_arms:
        #    a_k = np.random.choice(unselected_a)
        #else:
        a_k = a_k_hat
        f = 1
        dict_a_k[a_k] = f
        return dict_a_k

    def update_mab_k(self, arm, click_reward_boolean, k):
        epsilongreedy_k = self.dict_MABs[k]
        epsilongreedy_k.update(arm, click_reward_boolean)


class EpsilonGreedySim:

    def __init__(self, item_ids, epsilon):
        self.name = "Epsilon"

        self.article_click_count = [0]
        self.RBA_position_click_count = [[0] * cnst.RANKING_POSITIONS for _ in range(0, cnst.T)]
        self.click_position_count = [[0] * cnst.RANKING_POSITIONS for _ in range(0, cnst.T)]

        self.ranker = EpsilonGreedyRankedBanditAlgorithm(cnst.RANKING_POSITIONS, item_ids, epsilon)

        self.estimated_rewards_t = {}
        for item in item_ids:
            self.estimated_rewards_t[item] = np.zeros(cnst.OBJECTIVES_i)

    def recommend(self, x_list, user_id, dict_arm_features, isexplore):
        A_tilde = []
        for k in range(0, cnst.RANKING_POSITIONS):
            a_k = self.ranker.get_a_k(k, A_tilde)
            A_tilde.append(a_k)

        print(self.name, A_tilde)
        return A_tilde

    def update(self, k_rank, a, r_a, is_click, x_list, dict_arm_features, t, user_id):
        arm_id = a.keys()[0]
        f_k = 0
        if is_click == 1:
            self.click_position_count[t][k_rank] = 1
            if a[arm_id] == 1:
                f_k = 1
                self.RBA_position_click_count[t][k_rank] += 1

        self.ranker.update_mab_k(int(arm_id), f_k, k_rank)


    def get_ctr(self):
        return cnst.calculate_ctr(self.click_position_count, cnst.RANKING_POSITIONS)

    def dump_results(self):

        filename = str(datetime.datetime.now().strftime('%Y-%m-%d-%H-%M-%S_{fname}').format(fname="EpsilonGreedy.txt"))
        with open(filename, 'w') as f:
            f.write(json.dumps({
                'EpsilonGreedy_RBA': self.RBA_position_click_count,
                'EpsilonGreedy_click_position': self.click_position_count,
                'EpsilonGreedy_click_count': self.article_click_count,
                'EpsilonGreedy_ctr': self.get_ctr(),
                '#Objectives': cnst.OBJECTIVES_i,
                'ContextDim': cnst.OBJECTIVES_CONTEXT_DIMS_L,
                'Arms': cnst.ARMS_COUNT,
                'RankingPositions': cnst.RANKING_POSITIONS,
                'User': cnst.NO_OF_USER,
                'Lambda': cnst.LAMBDA,
                'Trials': cnst.T,
                'Beta': cnst.BETA}))
