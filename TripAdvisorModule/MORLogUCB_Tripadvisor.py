from __future__ import division

import numpy as np
import json
import datetime
import Constants as cnst
import LinUCB
import LogUCBRanker as rnk


def getPoolArticleArr(est_rewards):
    article_arr = []
    for x in est_rewards.keys():
        article_arr.append(np.array(est_rewards[x]))
    return np.array(article_arr)


class MORLogUCB:
    def __init__(self, item_ids):
        self.name = "MOU-UCB "
        self.item_ids = item_ids

        self.article_click_count = [[[0] * cnst.RANKING_POSITIONS for n in range(0,
                                                                                cnst.T)]]  # counts the number of total clicks in T trials.
        self.RBA_position_click_count = [[0] * cnst.RANKING_POSITIONS for n in range(0,
                                                                                cnst.T)]  # counts the accurate recommendations by each MAB instance and records per position.
        self.click_position_count = [[0] * cnst.RANKING_POSITIONS for n in
                                range(0, cnst.T)]  # counts the number of clicks per ranking position

        self.algo = LinUCB.LinUCBMultiObjectiveAlgorithm(self.item_ids)

        self.ranker = rnk.RankedBanditLogUCBAlgorithm(cnst.RANKING_POSITIONS)

        self.estimated_rewards_t = {}
        self.estimated_reward_for_recommened_a =[]
        self.real_reward_for_recommended_a =[]

    def recommend(self, x_list, user_id, dict_arm_features, isexplore):

        est_reward_vectors_for_all_A = self.algo.get_estimated_rewards_for_all_arms(x_list, dict_arm_features)
        if dict_arm_features is not None:
            assert len(est_reward_vectors_for_all_A) == len(dict_arm_features)

        A_tilde = [] # the ranked list

        a_k = None
        if isexplore[0]:
             #idx_sort_all = np.argsort(np.array(est_reward_vectors_for_all_A.values()), axis=0)
             #single_obj_idx_sorted = idx_sort_all[isexplore[1]]
             #top_k = single_obj_idx_sorted[:cnst.RANKING_POSITIONS]
             #for idx in top_k:
             #   arm = est_reward_vectors_for_all_A.keys()[idx]
             #   a_k = {arm: 1}
             #A_tilde.append(a_k)
            for k in range(0, cnst.RANKING_POSITIONS):
                ranked_arms = set().union(*(a.keys() for a in A_tilde))
                article_pool_ids = self.item_ids
                unselected_a_ids = list(set(article_pool_ids).symmetric_difference(ranked_arms))

                a_k = {np.random.choice(unselected_a_ids):1}
                if a_k is None:
                    print("Error in selecting a_k")
                    break
                else:
                    A_tilde.append(a_k)
        else:
            for k in range(0, cnst.RANKING_POSITIONS):
                a_k = self.ranker.get_a_k_log(k, A_tilde, est_reward_vectors_for_all_A, user_id, dict_arm_features)
                if a_k is None:
                    print("Error in selecting a_k")
                    break
                else:
                    A_tilde.append(a_k)
        print(self.name, A_tilde)
        self.estimated_rewards_t = est_reward_vectors_for_all_A
        return A_tilde

    def update(self, k_rank, a, r_a, is_click, x_list, dict_arm_features, t, user_id):
        arm_id = a.keys()[0]
        f_k = 0
        if is_click == 1:
            self.click_position_count[t][k_rank] = 1
            if a[arm_id] == 1:
                f_k = 1
                self.RBA_position_click_count[t][k_rank] += 1
        self.algo.update_multiobjective_linucb(arm_id, x_list, r_a, dict_arm_features)
        self.ranker.update_mab_k_wop(r_a, f_k, k_rank, user_id)


    def get_ctr(self):
         return cnst.calculate_ctr(self.click_position_count, cnst.RANKING_POSITIONS)

    def dump_results(self):
        filename = str(datetime.datetime.now().strftime('%Y-%m-%d-%H-%M-%S_{fname}').format(fname="MOR_LogUCB.txt"))

        with open(filename, 'w') as f:
            f.write(json.dumps({
                'MOR_LogUCB_wop_RBA':self.RBA_position_click_count,
                'MOR_LogUCB_wop_click_position':self.click_position_count,
                'MOR_LogUCB_wop_click_count': self.article_click_count,
                'MOR_LogUCB_wop_ctr':self.get_ctr(),
                '#Objectives':cnst.OBJECTIVES_i,
                'ContextDim':cnst.OBJECTIVES_CONTEXT_DIMS_L,
                'Arms':cnst.ARMS_COUNT,
                'RankingPositions':cnst.RANKING_POSITIONS,
                'User':cnst.NO_OF_USER,
                'Lambda':cnst.LAMBDA,
                'Trials':cnst.T,
                'mor_lin_ucb_alpha': cnst.mor_lin_ucb_alpha,
                'sor_lin_ucb_alpha': cnst.sor_lin_ucb_alpha,
                'Beta':cnst.BETA,
                'estimated_reward_for_recommened_a': self.estimated_reward_for_recommened_a,
                'real_reward_for_recommended_a': self.real_reward_for_recommended_a
            }))
        f.close()

