import numpy as np
import math

# For the simulation
user_features = 19
arm_features = 7
OBJECTIVES_i = 5
TAU = 200
OBJECTIVES_CONTEXT_DIMS_L = user_features + arm_features
ARMS_COUNT = 1836
RANKING_POSITIONS = 10
NO_OF_USER = 33

LAMBDA = 3

mor_lin_ucb_alpha = 0.2
sor_lin_ucb_alpha = 0.2

BETA = 0.3

T = (TAU*OBJECTIVES_i)+9000

f_k_threshold = 0.8
error_lower_bound = -0.01
error_upper_bound = 0.001

linear_m_upper_bound = 1
linear_m_lower_bound = -1
linear_c_upper_bound = 1
linear_c_lower_bound = -1

country_code = {"USA":      [1, 0, 0, 0, 0, 0, 0, 0, 0],
               "UK":        [0, 1, 0, 0, 0, 0, 0, 0, 0],
               "Italy":     [0, 0, 1, 0, 0, 0, 0, 0, 0],
               "Japan":     [0, 0, 0, 1, 0, 0, 0, 0, 0],
               "Germany":   [0, 0, 0, 0, 1, 0, 0, 0, 0],
               "AUS":       [0, 0, 0, 0, 0, 1, 0, 0, 0],
               "Canada":    [0, 0, 0, 0, 0, 0, 1, 0, 0],
               "Switzerland":[0, 0, 0, 0, 0, 0, 0, 1,0],
               "France":    [0, 0, 0, 0, 0, 0, 0, 0, 1],
               }

# common static methods

def sigmoid(x):
    return 1 / (1 + math.exp(-x))


def get_real_f_k(real_r_a, user_beta):
    click_reward = sigmoid(np.dot(np.array(real_r_a), np.array(user_beta)))
    if click_reward > f_k_threshold:
        return 1
    else:
        return 0


def get_objective_rewards(dict_m, arm_id, x_list, dict_c, sim):
    val = np.add(np.sum(np.multiply(dict_m[arm_id], x_list), axis=1), dict_c[arm_id]) + sim.get_random_uniform()
    return val


def calculate_ctr(article_click_count, pos):
    assert len(article_click_count[0]) > 0
    avg_combined_clicks = [np.divide(sum(l),(pos*1.0)) for l in article_click_count]
    cum_article_clicks = np.cumsum(avg_combined_clicks)
    ctr = np.divide(cum_article_clicks, np.arange(1, len(cum_article_clicks)+1), dtype=float)
    return ctr.tolist()


