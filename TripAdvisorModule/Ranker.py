import numpy as np
import Constants as cnst


class LinUCBUserStruct:
    def __init__(self, featureDimension, lambda_, init="random"):
        self.d = featureDimension
        self.A = lambda_ * np.identity(n=self.d)
        self.b = np.zeros(self.d)
        self.AInv = np.linalg.inv(self.A)
        if (init == "random"):
            self.UserTheta = np.random.rand(self.d)
        else:
            self.UserTheta = np.zeros(self.d)
        self.time = 0

    def updateParameters(self, articlePicked_FeatureVector, click):
        feat = np.array(articlePicked_FeatureVector)
        self.A += np.outer(feat, feat)
        self.b += feat * click
        self.AInv = np.linalg.inv(self.A)
        self.UserTheta = np.dot(self.AInv, self.b)
        self.time += 1

    def getTheta(self):
        return self.UserTheta

    def getA(self):
        return self.A

    def getProb(self, alpha, article_FeatureVector):
        if alpha == -1:
            alpha = 0.1 * np.sqrt(np.log(self.time + 1))
        mean = np.dot(self.UserTheta, article_FeatureVector)
        var = np.sqrt(np.dot(np.dot(article_FeatureVector, self.AInv), article_FeatureVector))
        pta = mean + alpha * var
        return pta

    def getProb_plot(self, alpha, article_FeatureVector):
        mean = np.dot(self.UserTheta, article_FeatureVector)
        var = np.sqrt(np.dot(np.dot(article_FeatureVector, self.AInv), article_FeatureVector))
        pta = mean + alpha * var
        return pta, mean, alpha * var


class N_LinUCBAlgorithm:
    def __init__(self, dimension, alpha, lambda_, n, init="random"):  # n is number of users
        self.users = {}
        # algorithm have n users, each user has a user structure

        self.lambda_ = lambda_
        self.dimension = dimension
        self.alpha = alpha

        self.CanEstimateUserPreference = False
        self.CanEstimateCoUserPreference = True
        self.CanEstimateW = False
        self.CanEstimateV = False


    def decide(self, pool_articles, userID, est_rewards):
        if userID not in self.users:
            self.users[userID] = LinUCBUserStruct(self.dimension, self.lambda_, init='random')
        maxPTA = float('-inf')
        articlePicked = None

        for x in pool_articles:
            x_pta = self.users[userID].getProb(self.alpha, est_rewards[x])
            # pick article with highest Prob
            if maxPTA < x_pta:
                articlePicked = x
                maxPTA = x_pta

        return articlePicked

    def getProb(self, pool_articles, userID):
        means = []
        vars = []
        for x in pool_articles:
            x_pta, mean, var = self.users[userID].getProb_plot(self.alpha, x.contextFeatureVector[:self.dimension])
            means.append(mean)
            vars.append(var)
        return means, vars

    def updateParameters(self, articlePicked, click, userID):
        self.users[userID].updateParameters(articlePicked, click)

    def getCoTheta(self, userID):
        return self.users[userID].UserTheta

'''
class RankedBanditAlgorithm:
    def __init__(self, pos, sim_):
        self.K =pos
        self.dict_MABs ={}
        self.sim = sim_
        for k in range(0,self.K):
            self.dict_MABs[k]= N_LinUCBAlgorithm(cnst.OBJECTIVES_i, cnst.BETA, cnst.LAMBDA, cnst.NO_OF_USER, sim_)

    def get_a_k(self, A_prime, k, ranked_a_list, estimated_rewards, user_id):
        linucb_k = self.dict_MABs[k]
        ranked_arms = set().union(*(a.keys() for a in ranked_a_list))
        dict_a_k ={}
        f =0
        if k >= len(A_prime):
            # select from non pareto front articles
            non_pareto_list = list(set(range(0,cnst.ARMS_COUNT)).symmetric_difference(A_prime))
            a_k_hat = linucb_k.decide(non_pareto_list, user_id, estimated_rewards)
            unselected_a = list(set(non_pareto_list)- ranked_arms)
        else:
            a_k_hat = linucb_k.decide(A_prime, user_id, estimated_rewards)
            unselected_a = list(set(A_prime).symmetric_difference(ranked_arms))

        if a_k_hat in ranked_arms:
            a_k = self.sim.get_random_val_from_list(unselected_a)
            assert a_k != a_k_hat
        else:
            a_k = a_k_hat
            f = 1
        dict_a_k[a_k] = f
        return dict_a_k

    def update_mab_k(self, user_context, click_reward_boolean, k, user_id):
        linucb_k = self.dict_MABs[k]
        linucb_k.updateParameters(user_context, click_reward_boolean, user_id)
'''

class RankedBanditWopAlgorithm:
    def __init__(self, pos):
        self.K =pos
        self.dict_MABs ={}
        for k in range(0,self.K):
            self.dict_MABs[k]= N_LinUCBAlgorithm(cnst.OBJECTIVES_i, cnst.BETA, cnst.LAMBDA, cnst.NO_OF_USER)

    def get_a_k_wop(self, k, ranked_a_list, estimated_rewards, user_id,dict_article_pool):
        if dict_article_pool is None:
            article_pool_ids = range(0, cnst.ARMS_COUNT)
        else:
            article_pool_ids = dict_article_pool.keys()


        linucb_k = self.dict_MABs[k]
        ranked_arms = set().union(*(a.keys() for a in ranked_a_list))
        dict_a_k ={}
        f = 0

        unselected_a_ids = list(set(article_pool_ids).symmetric_difference(ranked_arms))
        a_k_hat = linucb_k.decide(unselected_a_ids, user_id, estimated_rewards)


        #if a_k_hat in ranked_arms:
        #    a_k = np.random.choice(unselected_a)
        #    assert a_k != a_k_hat
        #else:
        a_k = a_k_hat
        f = 1
        dict_a_k[a_k] = f
        return dict_a_k

    def update_mab_k_wop(self, user_context, click_reward_boolean, k, user_id):
        linucb_k = self.dict_MABs[k]
        linucb_k.updateParameters(user_context, click_reward_boolean, user_id)







