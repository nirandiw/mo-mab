from __future__ import division

import Constants as cnst
import SORanker as rnk
#import matlab.engine
import json
import datetime
import numpy as np


class SORSim:
    def __init__(self, item_ids, concat_features):
        self.name = "SOR-LinUCB "

        self.item_ids = item_ids

        self.article_click_count = [0]  # counts the number of total clicks in T trials.
        self.RBA_position_click_count = [[0] * cnst.RANKING_POSITIONS for _ in range(0,
                                                                                cnst.T)]  # counts the accurate recommendations by each MAB instance and records per position.
        self.click_position_count = [[0] * cnst.RANKING_POSITIONS for _ in
                                range(0, cnst.T)]  # counts the number of clicks per ranking position

        self.concat_size = concat_features
        self.ranker = rnk.RankedLinUCBBanditAlgorithm(cnst.RANKING_POSITIONS, item_ids, self.concat_size)

        self.estimated_rewards_t = {}
        for item in item_ids:
            self.estimated_rewards_t[item] = np.zeros(cnst.OBJECTIVES_i)

    def recommend(self, x_lists, user_id, dict_arm_features, isexplore):

        A_tilde = [] # the ranked list
        x_list = np.concatenate(x_lists[0:self.concat_size])  # TODO Check other permutations
        for k in range(0, cnst.RANKING_POSITIONS):

            a_k = self.ranker.get_a_k_so(k, A_tilde, x_list, dict_arm_features)
            if a_k is None:
                print("Pareto front smaller than 10")
                break
            else:
                A_tilde.append(a_k)

        print(self.name, A_tilde)
        return A_tilde

    def update(self, k_rank, a, r_a, is_click, x_list, dict_arm_features, t, user_id):

        arm_id = a.keys()[0]
        f_k = 0
        if is_click == 1:
            self.click_position_count[t][k_rank] = 1
            if a[arm_id] == 1:
                f_k = 1
                self.RBA_position_click_count[t][k_rank] += 1
        x_list = np.concatenate(x_list[0:self.concat_size])
        self.ranker.update_mab_k_so(x_list, f_k, k_rank, arm_id, dict_arm_features)



    def get_ctr(self):
        return cnst.calculate_ctr(self.click_position_count, cnst.RANKING_POSITIONS)

    def dump_results(self):
        filename = "SOR_LinUCB"+str(self.concat_size)+".txt"
        filename = str(datetime.datetime.now().strftime('%Y-%m-%d-%H-%M-%S_{fname}').format(fname=filename))


        with open(filename, 'w') as f:
            f.write(json.dumps({
                'SOR_LinUCB_RBA': self.RBA_position_click_count,
                'SOR_LinUCB_click_position': self.click_position_count,
                'SOR_LinUCB_click_count': self.article_click_count,
                'SOR_LinUCB_ctr': self.get_ctr(),
                '#Objectives': cnst.OBJECTIVES_i,
                'ContextDim': cnst.OBJECTIVES_CONTEXT_DIMS_L,
                'Arms': cnst.ARMS_COUNT,
                'RankingPositions': cnst.RANKING_POSITIONS,
                'User': cnst.NO_OF_USER,
                'Lambda': cnst.LAMBDA,
                'Trials': cnst.T,
                'mor_lin_ucb_alpha': cnst.mor_lin_ucb_alpha,
                'sor_lin_ucb_alpha': cnst.sor_lin_ucb_alpha,
                'Beta': cnst.BETA}))

