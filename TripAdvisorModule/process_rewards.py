import pandas as pd

df = pd.read_json('D:\pycharm_projects\yelp-master\\relevant_dummy.json', orient='index')

df_ratings = pd.DataFrame(df['ratings'].tolist(), index= df.index)

df_ratings.rename(columns={'location':'location_rating'}, inplace=True)

print(df_ratings.columns)

df_concat = pd.concat((df_ratings, df), axis=1)

assert  df_concat.shape[0] == df_ratings.shape[0] == df.shape[0]

df_reward = df_concat[['a_id', 'offering_id','business_service_(e_g_internet_access)', 'check_in_front_desk','cleanliness', 'location_rating', 'overall', 'rooms', 'service','sleep_quality', 'value']]

assert  df_concat.shape[0] == df_ratings.shape[0] == df.shape[0] == df_reward.shape[0]

print(df_reward.columns)
df_reward.fillna(0)
df_reward.to_json('D:\pycharm_projects\yelp-master\\rewards.json', orient='index')
