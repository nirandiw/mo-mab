from pandas import DataFrame
from etl_utils import ETLUtils
import numpy
import json


def remove_empty_user_reviews(reviews):
    """
    Returns a copy of the original reviews list without the reviews made by
    users who have an empty ID

    :param reviews: a list of reviews
    :return: a copy of the original reviews list without the reviews made by
    users who have an empty ID
    """
    filtered_reviews = [review for review in reviews if
                        review['user_id'] != '']
    return filtered_reviews


def extract_fields(reviews):
    ratings_criteria = [
        'cleanliness',
        'location',
        'rooms',
        'service',
        # 'sleep_quality',
        'value'
    ]

    for review in reviews:
        # review['user_id'] = get_dictionary_subfield(review, ['author', 'id'])
        review['user_id'] = review['author']['id']
        review['overall_rating'] = review['ratings']['overall']

        ratings = review['ratings']
        for criterion in ratings_criteria:
            if criterion in ratings:
                review[criterion + '_rating'] = review['ratings'][criterion]


def remove_users_with_low_reviews(reviews, min_reviews):
    """
    Returns a copy of the original reviews list without the reviews made by
    users who have made less than min_reviews reviews

    :param reviews: a list of reviews
    :param min_reviews: the minimum number of reviews a user must have in order
    not to be removed from the reviews list
    :return: a copy of the original reviews list without the reviews made by
    users who have made less than min_reviews reviews
    """
    users = get_user_list(reviews, min_reviews)
    return ETLUtils.filter_records(reviews, 'user_id', users)


def get_item_list(reviews, min_reviews):
    """
    Returns the list of items that have at least min_reviews

    :param reviews: the list of reviews
    :param min_reviews: the minimum number of reviews
    :return: a list of item IDs
    """
    data_frame = DataFrame(reviews)
    column = 'offering_id'
    counts = data_frame.groupby(column).size()
    filtered_counts = counts[counts >= min_reviews]
    # print(filtered_counts)
    num_items = len(filtered_counts)
    num_reviews = filtered_counts.sum()

    print('Number of items: %i' % num_items)
    print('Number of reviews: %i' % num_reviews)

    items = filtered_counts.index.values.tolist()
    return items


def remove_single_review_hotels(reviews):
    """
    TODO: Missing, this method is just a stub
    Returns a copy of the original reviews list without the reviews of hotels
    that just have been reviewed once

    :param reviews: a list of reviews
    :return: a copy of the original reviews list without the reviews of hotels
    that just have been reviewed once
    """
    items = get_item_list(reviews, 2)
    return ETLUtils.filter_records(reviews, 'offering_id', items)


def get_user_list(reviews, min_reviews):
    """
    Returns the list of users that have reviewed at least min_reviews hotels

    :param reviews: the list of reviews
    :param min_reviews: the minimum number of reviews
    :return: a list of user IDs
    """
    data_frame = DataFrame(reviews)
    column = 'user_id'
    counts = data_frame.groupby(column).size()
    filtered_counts = counts[counts >= min_reviews]
    # print(filtered_counts)
    num_users = len(filtered_counts)
    num_reviews = filtered_counts.sum()

    print('Number of users: %i' % num_users)
    print('Number of reviews: %i' % num_reviews)

    users = filtered_counts.index.values.tolist()
    return users


def create_ratings_matrix(reviews):
    """
    Returns (ratings_matrix, overall_ratings_list), where ratings_matrix is a
    list of lists containing the values for all the rating criteria (except
    the overall rating), so for each review a list of rating values is returned.
    overall_ratings_list is a list containing the overall rating for each of the
    reviews. This function verifies that reviews don't have any missing ratings,
    in case there are ratings missing, those are not included in the returning
    values

    :param reviews: a list of reviews
    :return: (ratings_matrix, overall_ratings_list), where ratings_matrix is a
    list of lists containing the values for all the rating criteria (except
    the overall rating), and overall_ratings_list is a list containing the
    overall rating for each of the reviews
    """
    ratings_matrix = []
    overall_ratings_list = []

    missing_count = 0

    for review in reviews:
        ratings = review['ratings']
        ratings_list = []

        rating_criteria = [
            'cleanliness',
            'location',
            'rooms',
            'service',
            # 'sleep_quality',
            'value'
        ]
        contains_missing_rating = 0

        for criterion in rating_criteria:
            if criterion in ratings:
                ratings_list.append(ratings[criterion])

        missing_count += contains_missing_rating

        # If there are not missing ratings, we add the ratings to the matrix
        # In other words, we are ignoring reviews with missing ratings
        if not contains_missing_rating:
            ratings_matrix.append(ratings_list)
            overall_ratings_list.append(ratings['overall'])
            review['ratings_list'] = ratings_list
            # review['overall_rating'] = ratings['overall']

    # print(missing_count)
    return ratings_matrix, overall_ratings_list


def get_criteria_weights(reviews, user_id):
    """
    Obtains the weights for each of the criterion of the given user

    :param reviews: a list of all the available reviews
    :param user_id: the ID of the user
    :return: a list with the weights for each of the criterion of the given user
    """
    filtered_reviews = ETLUtils.filter_records(reviews, 'user_id', [user_id])

    ratings_matrix, overall_ratings_list = create_ratings_matrix(
        filtered_reviews)

    overall_ratings_matrix = numpy.vstack(
        [overall_ratings_list, numpy.ones(len(overall_ratings_list))]).T
    m, c = numpy.linalg.lstsq(overall_ratings_matrix, ratings_matrix)[0]

    return m


def get_significant_criteria(criteria_weights):
    """
    Returns (significant_criteria, cluster_name) where significant_criteria is a
    dictionary with the criteria that are significant and their values.
    cluster_name is the name of the cluster in which a user with the obtained
    significant criteria must belong

    :param criteria_weights: a list with the weights for each criterion
    :return: (significant_criteria, cluster_name) where significant_criteria is
    a dictionary with the criteria that are significant and their values.
    cluster_name is the name of the cluster in which a user with the obtained
    significant criteria must belong
    """
    rating_criteria = [
        'cleanliness',
        'location',
        'rooms',
        'service',
        # 'sleep_quality',
        'value'
    ]

    cluster_name = ''

    significant_criteria = {}
    for index, value in enumerate(criteria_weights):
        if (0.7 < value < 1.3) or (-1.3 < value < -0.7):
            significant_criteria[rating_criteria[index]] = value
            cluster_name += '1'
        else:
            cluster_name += '0'

    return significant_criteria, cluster_name


def build_user_clusters(reviews):
    """
    Builds a series of clusters for users according to their significant
    criteria. Users that have exactly the same significant criteria will belong
    to the same cluster.

    :param reviews: the list of reviews
    :return: a dictionary where all the keys are the cluster names and the
    values for those keys are list of users that belong to that cluster
    """

    min_reviews = 10
    user_list = get_user_list(reviews, min_reviews)
    user_cluster_dictionary = {}

    for user in user_list:
        weights = get_criteria_weights(reviews, user)
        significant_criteria, cluster_name = get_significant_criteria(weights)

        if cluster_name in user_cluster_dictionary:
            user_cluster_dictionary[cluster_name].append(user)
        else:
            user_cluster_dictionary[cluster_name] = [user]

    for key in user_cluster_dictionary.keys():
        print(key, len(user_cluster_dictionary[key]))

    return user_cluster_dictionary


def verify_rating_criteria(review):
    """
    Verifies if the given review contains all the ratings criteria required.
    Returns True in case the review contains all the necessary keys. Returns
    False otherwise. For example, if a review contains
    {'ratings': {'cleanliness':3, 'location':4, 'rooms': 5, 'service':3, 'value': 4}, ...}
    It will return True because all the desired criteria is present. But if a
    review contains {'ratings': {'cleanliness':3, 'value': 4}, ...} it will
    return False because it doesn't contain values for 'location', 'rooms' and
    'service'

    :param review: a dictionary with the review information
    :return: True in case all the desired ratings criteria are present in the
    review, False otherwise
    """
    expected_criteria = [
        'cleanliness',
        'location',
        'rooms',
        'service',
        # 'sleep_quality',
        'value'
    ]
    expected_criteria = set(expected_criteria)
    actual_criteria = set(review['ratings'])
    return expected_criteria.issubset(actual_criteria)


def remove_missing_ratings_reviews(reviews):
    """
    Returns a copy of the original reviews list without the reviews that have
    missing ratings

    :param reviews: a list of reviews
    :return: a copy of the original reviews list without the reviews that have
    missing ratings
    """
    filtered_reviews = [review for review in reviews if
                        verify_rating_criteria(review)]
    return filtered_reviews


def clean_reviews(reviews):
    """
    Returns a copy of the original reviews list with only that are useful for
    recommendation purposes

    :param reviews: a list of reviews
    :return: a copy of the original reviews list with only that are useful for
    recommendation purposes
    """
    filtered_reviews = remove_empty_user_reviews(reviews)
    filtered_reviews = remove_missing_ratings_reviews(filtered_reviews)
    print('Finished remove_missing_ratings_reviews')
    filtered_reviews = remove_users_with_low_reviews(filtered_reviews, 10)
    print('Finished remove_users_with_low_reviews')
    filtered_reviews = remove_single_review_hotels(filtered_reviews)
    print('Finished remove_single_review_hotels')
    return filtered_reviews


def pre_process_reviews():
    """
    Returns a list of preprocessed reviews, where the reviews have been filtered
    to obtain only relevant data, have dropped any fields that are not useful,
    and also have additional fields that are handy to make calculations

    :return: a list of preprocessed reviews
    """
    data_folder = 'D:\pycharm_projects\yelp-master\\'
    review_file_path = data_folder + 'review.txt'
    # review_file_path = data_folder + 'review-short.json'
    reviews = ETLUtils.load_json_file(review_file_path)

    select_fields = ['ratings', 'author', 'offering_id']
    reviews = ETLUtils.select_fields(select_fields, reviews)
    extract_fields(reviews)
    ETLUtils.drop_fields(['author'], reviews)
    reviews = clean_reviews(reviews)

    return reviews


if __name__ == '__main__':
    print("Test")
    reviews = pre_process_reviews()
    user_cluster_dictionary = build_user_clusters(reviews)
    print(user_cluster_dictionary)
    f = open('D:\pycharm_projects\yelp-master\user-cluster.json', 'w')
    json.dump(user_cluster_dictionary, f)
    f.close()
    # mean_absolute_error = calculate_mean_average_error(reviews, user_cluster_dictionary)
    # print('Mean Absolute error: %f' % mean_absolute_error)
