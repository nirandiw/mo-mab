import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns
df_sequence = pd.read_json("./data//relevant_country_filled_reduced.json", orient='index')
df_user_clusters = pd.read_json('./data/df_user_clusters.json', orient='index')

x = np.arange(1,6)
sns.set(font_scale=1.6)
#fig, ax = plt.subplots(figsize=(6,6),dpi=300)

df_100 = df_user_clusters[df_user_clusters[0]==100]
df_100_sequence = df_sequence[df_sequence['a_id'].isin(df_100.index.tolist())]

df_100_ratings = pd.DataFrame.from_dict(df_100_sequence['ratings'].tolist())

df_100_positive = df_100_ratings[df_100_ratings['overall']>=2.5]
df_100_positive_data = df_100_positive[['cleanliness','location', 'rooms', 'service','value']].mean().tolist()
df_100_negative = df_100_ratings[df_100_ratings['overall']<2.5]
df_100_negative_data = np.multiply(df_100_negative[['cleanliness','location', 'rooms', 'service','value']].mean(), -1).tolist()

#plt.matshow(df_100_ratings[['cleanliness','location', 'rooms', 'service','value','overall']].corr())
corr100 = df_100_ratings[['cleanliness','location', 'rooms', 'service','value','overall']].corr()
sns.heatmap(corr100, xticklabels=corr100.columns, yticklabels=corr100.columns, cbar=False)
plt.savefig("df_100.pdf")
#============

df_10000 = df_user_clusters[df_user_clusters[0]==10000]
df_10000_sequence = df_sequence[df_sequence['a_id'].isin(df_10000.index.tolist())]

df_10000_ratings = pd.DataFrame.from_dict(df_10000_sequence['ratings'].tolist())

df_10000_positive = df_10000_ratings[df_10000_ratings['overall']>=2.5]
df_10000_positive_data = df_10000_positive[['cleanliness','location', 'rooms', 'service','value']].mean().tolist()
df_10000_negative = df_10000_ratings[df_10000_ratings['overall']<2.5]
df_10000_negative_data = np.multiply(df_10000_negative[['cleanliness','location', 'rooms', 'service','value']].mean(), -1).tolist()
plt.clf()
corr10000 = df_10000_ratings[['cleanliness','location', 'rooms', 'service','value','overall']].corr()
sns.heatmap(corr10000, xticklabels=corr10000.columns, yticklabels=corr10000.columns, cbar=False)
plt.savefig("df_10000.pdf")
#===========

df_1000 = df_user_clusters[df_user_clusters[0]==1000]
df_1000_sequence = df_sequence[df_sequence['a_id'].isin(df_1000.index.tolist())]

df_1000_ratings = pd.DataFrame.from_dict(df_1000_sequence['ratings'].tolist())

df_1000_positive = df_1000_ratings[df_1000_ratings['overall']>=2.5]
df_1000_positive_data = df_1000_positive[['cleanliness','location', 'rooms', 'service','value']].mean().tolist()
df_1000_negative = df_1000_ratings[df_1000_ratings['overall']<2.5]
df_1000_negative_data = np.multiply(df_1000_negative[['cleanliness','location', 'rooms', 'service','value']].mean(), -1).tolist()
plt.clf()
corr1000 = df_1000_ratings[['cleanliness','location', 'rooms', 'service','value','overall']].corr()
sns.heatmap(corr1000, xticklabels=corr1000.columns, yticklabels=corr1000.columns, cbar=False)
plt.savefig("df_1000.pdf")
#=====

df_10 = df_user_clusters[df_user_clusters[0]==10]
df_10_sequence = df_sequence[df_sequence['a_id'].isin(df_10.index.tolist())]

df_10_ratings = pd.DataFrame.from_dict(df_10_sequence['ratings'].tolist())

df_10_positive = df_10_ratings[df_10_ratings['overall']>=2.5]
df_10_positive_data = df_10_positive[['cleanliness','location', 'rooms', 'service','value']].mean().tolist()
df_10_negative = df_10_ratings[df_10_ratings['overall']<2.5]
df_10_negative_data = np.multiply(df_10_negative[['cleanliness','location', 'rooms', 'service','value']].mean(), -1).tolist()
plt.clf()
corr10 = df_10_ratings[['cleanliness','location', 'rooms', 'service','value','overall']].corr()
sns.heatmap(corr10, xticklabels=corr10.columns, yticklabels=corr10.columns)
plt.tight_layout()
plt.savefig("df_10.pdf")
#====

df_1 = df_user_clusters[df_user_clusters[0]==1]
df_1_sequence = df_sequence[df_sequence['a_id'].isin(df_1.index.tolist())]

df_1_ratings = pd.DataFrame.from_dict(df_1_sequence['ratings'].tolist())

df_1_positive = df_1_ratings[df_1_ratings['overall']>=2.5]
df_1_positive_data = df_1_positive[['cleanliness','location', 'rooms', 'service','value']].mean().tolist()
df_1_negative = df_1_ratings[df_1_ratings['overall']<2.5]
df_1_negative_data = np.multiply(df_1_negative[['cleanliness','location', 'rooms', 'service','value']].mean(), -1).tolist()
plt.clf()
corr1 = df_1_ratings[['cleanliness','location', 'rooms', 'service','value','overall']].corr()
sns.heatmap(corr1, xticklabels=corr1.columns, yticklabels=corr1.columns)
plt.tight_layout()
plt.savefig("df_1.pdf")

'''
fig= plt.figure()
ax = plt.subplot(111)
width =0.18
ax.bar(x,df_10000_negative_data,width= width,color='r')
ax.bar(x, df_10000_positive_data, width=width, color='salmon')
ax.bar(x+width,df_100_negative_data,width= width,color='b')
ax.bar(x+width, df_100_positive_data, width=width, color='lightblue')
ax.bar(x+width*2,df_1000_negative_data,width= width,color='y')
ax.bar(x+width*2, df_1000_positive_data, width=width, color='lightyellow')
ax.bar(x+width*3,df_10_negative_data,width= width,color='g')
ax.bar(x+width*3, df_10_positive_data, width=width, color='lightgreen')
ax.bar(x+width*4,df_1_negative_data,width= width,color='gray')
ax.bar(x+width*4, df_1_positive_data, width=width, color='lightgray')
'''

df_10000_sequence = df_sequence[df_sequence['a_id'].isin(df_10000.index.tolist())]

df_10000_ratings = pd.DataFrame.from_dict(df_10000_sequence['ratings'].tolist())

df_10000_positive = df_10000_ratings[df_10000_ratings['overall']>=2.5]
df_10000_positive_data = df_10000_positive[['cleanliness','location', 'rooms', 'service','value']].mean().tolist()
df_10000_negative = df_10000_ratings[df_10000_ratings['overall']<2.5]
df_10000_negative_data = np.multiply(df_10000_negative[['cleanliness','location', 'rooms', 'service','value']].mean(), -1).tolist()
plt.clf()
corr10000 = df_10000_ratings[['cleanliness','location', 'rooms', 'service','value','overall']].corr()
sns.heatmap(corr10000, xticklabels=corr10000.columns, yticklabels=corr10000.columns, cbar=False)
plt.savefig("df_10000.pdf")


#heatmap_data = [corr10000, corr1000, corr100, corr10]
#sns.heatmap(heatmap_data, xticklabels=corr10000.columns, yticklabels=corr10000.columns)