import numpy as np
import Constants as cnst


class LinUCBArmStruct:
    def __init__(self, featureDimension, lambda_, init="random"):
        self.d = featureDimension
        self.A = lambda_ * np.identity(n=self.d)
        self.b = np.zeros(self.d)
        self.AInv = np.linalg.inv(self.A)
        if (init == "random"):
            self.ArmTheta = np.random.rand(self.d)
        else:
            self.ArmTheta = np.zeros(self.d)
        self.time = 0

    def updateParameters(self, feat, click):
        # feat = np.array(articlePicked_FeatureVector)
        self.A += np.outer(feat, feat)
        self.b += feat * click
        self.AInv = np.linalg.inv(self.A)
        self.ArmTheta = np.dot(self.AInv, self.b)
        self.time += 1

    def getTheta(self):
        return self.ArmTheta

    def getA(self):
        return self.A

    def getProb(self, alpha, article_FeatureVector):
        if alpha == -1:
            alpha = 0.1 * np.sqrt(np.log(self.time + 1))
        mean = np.dot(self.ArmTheta, article_FeatureVector)
        var = np.sqrt(np.dot(np.dot(article_FeatureVector, self.AInv), article_FeatureVector))
        pta = mean + alpha * var
        return pta, mean, alpha * var


class N_LinUCBAlgorithm:
    def __init__(self, dimension, alpha, lambda_, item_ids, init="random"):  # n is number of users
        self.arms = {}
        # algorithm have n users, each user has a user structure
        for i in item_ids:
            self.arms[i]= LinUCBArmStruct(dimension, lambda_, init)

        self.dimension = dimension
        self.alpha = alpha

        self.CanEstimateUserPreference = False
        self.CanEstimateCoUserPreference = True
        self.CanEstimateW = False
        self.CanEstimateV = False

    def decide(self, pool_articles, user_features, dict_arm_features):
        maxPTA = float('-inf')
        articlePicked = None

        for x in pool_articles:
            if dict_arm_features is not None:
                features = np.concatenate((user_features, dict_arm_features[x]))
                # features = np.outer(user_features, dict_arm_features[x]).flatten()
            else:
                features = user_features
            x_pta = self.arms[x].getProb(self.alpha, features)
            # pick article with highest Prob
            if maxPTA < x_pta:
                articlePicked = x
                maxPTA = x_pta

        return articlePicked

    def getProb(self, pool_articles):
        means = []
        vars = []
        for x in pool_articles:
            x_pta, mean, var = self.arms[x].getProb(self.alpha, x.contextFeatureVector[:self.dimension])
            means.append(mean)
            vars.append(var)
        return means, vars

    def updateParameters(self, feature, click, itemId):
        self.arms[itemId].updateParameters(feature, click)

    def getCoTheta(self, itemId):
        return self.arms[itemId].ArmTheta


class RankedLinUCBBanditAlgorithm:
    def __init__(self, pos, item_ids, number_objs):
        self.K = pos
        self.dict_MABs = {}
        for k in range(0, self.K):
            self.dict_MABs[k] = N_LinUCBAlgorithm(cnst.OBJECTIVES_CONTEXT_DIMS_L * number_objs ,
                                                  cnst.sor_lin_ucb_alpha, cnst.LAMBDA,item_ids )
        self.items = item_ids

    def get_a_k_so(self, k, ranked_a_list, features, dict_article_pool):
        if dict_article_pool is None:
            article_pool_ids = range(0, cnst.ARMS_COUNT)
        else:
            article_pool_ids = dict_article_pool.keys()

        linucb_k = self.dict_MABs[k]
        ranked_arms = set().union(*(a.keys() for a in ranked_a_list))
        dict_a_k = {}
        f = 0
        unselected_a_ids = list(set(article_pool_ids).symmetric_difference(ranked_arms))
        a_k_hat = linucb_k.decide(unselected_a_ids, features, dict_article_pool)

        #if a_k_hat in ranked_arms:
        #    a_k = np.random.choice(unselected_a)
        #    assert a_k != a_k_hat
        #else:
        a_k = a_k_hat
        f = 1
        dict_a_k[a_k] = f
        return dict_a_k

    def update_mab_k_so(self, context, click_reward_boolean, k, arm_id, dict_arm_features):
        linucb_k = self.dict_MABs[k]
        if dict_arm_features is not None:
            context = np.concatenate((context, dict_arm_features[arm_id]))
            # context = np.outer(context, dict_arm_features[arm_id]).flatten()
        linucb_k.updateParameters(context, click_reward_boolean, arm_id)
