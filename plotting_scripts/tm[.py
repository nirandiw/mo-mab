import seaborn as sns
import matplotlib.pyplot as plt
tips = sns.load_dataset("fmri")
print(tips.columns)
print(tips.head(4))
sns.lineplot(x='timepoint', y='signal',
             hue='event', style='event',
             markers=True, data= tips)
plt.show()