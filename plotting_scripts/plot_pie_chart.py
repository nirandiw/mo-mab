import numpy as np
from scipy.interpolate import spline
import matplotlib.pyplot as plt
import File_Handler as fh

def get_average(filenames, val):
    data_list = []
    for f in filenames:
        data_list.append(fh.read_file_plot_1(f,val))
    a = np.array(data_list)
    avg = np.mean(a, axis=0)
    return avg

# paretoucb1_files = ["2018-01-24-10-20-42_ParetoUCB1.txt"]
# morlinucb_files = ["2018-01-24-10-19-45_MOR_LinUCB.txt"]


paretoucb1_files = ["2018-01-23-23-32-07_ParetoUCB1.txt", "2018-01-23-23-37-57_ParetoUCB1.txt",
                    "2018-01-23-23-39-22_ParetoUCB1.txt", "2018-01-23-23-40-37_ParetoUCB1.txt",
                    "2018-01-23-23-41-46_ParetoUCB1.txt"]
morlinucb_files =["2018-01-23-23-23-55_MOR_LinUCB.txt", "2018-01-23-23-36-39_MOR_LinUCB.txt",
                  "2018-01-23-23-38-27_MOR_LinUCB.txt","2018-01-23-23-39-50_MOR_LinUCB.txt",
                  "2018-01-23-23-41-01_MOR_LinUCB.txt"]

# morlinucb_one_user_files = ["2018-01-23-23-50-12_MOR_LinUCB.txt", "2018-01-23-23-51-33_MOR_LinUCB.txt",
# "2018-01-23-23-52-56_MOR_LinUCB.txt","2018-01-23-23-54-18_MOR_LinUCB.txt","2018-01-24-00-10-51_MOR_LinUCB.txt"]
# paretoucb1_one_user_files =["2018-01-23-23-51-04_ParetoUCB1.txt","2018-01-23-23-52-27_ParetoUCB1.txt",
# "2018-01-23-23-53-51_ParetoUCB1.txt","2018-01-23-23-55-11_ParetoUCB1.txt","2018-01-24-00-12-58_ParetoUCB1.txt"]

morlinucb_data = get_average(morlinucb_files, "MOR_LinUCB_ctr")
paretoucb1_data = get_average(paretoucb1_files, "ParetoUCB1_ctr")
# morlinucb_one_user_data = get_average(morlinucb_one_user_files, "MOR_LinUCB_ctr")
# paretoucb1_one_user_data = get_average(paretoucb1_one_user_files, "ParetoUCB1_ctr")

number_of_trials = 1001
x = np.array(range(1, number_of_trials))
x_smooth = np.linspace(x.min(), x.max(), number_of_trials)

morlinucb_smooth = spline(x, morlinucb_data, x_smooth)
paretoucb1_smooth = spline(x, paretoucb1_data, x_smooth)
# morlinucb_one_smooth = spline(x, morlinucb_one_user_data, x_smooth)
# paretoucb1_one_smooth = spline(x, paretoucb1_one_user_data, x_smooth)

plt.figure(figsize=(5, 3), dpi=300)
plt.plot(x_smooth, morlinucb_smooth, label="MOR_LinUCB", linestyle='-', marker='*', markevery=100)
plt.plot(x_smooth, paretoucb1_smooth, label="ParetoUCB1", linestyle='-', marker='.', markevery=100)
# plt.plot(x_smooth, morlinucb_one_smooth, label="MOR_LinUCB:1_user", linestyle='-', marker='*', markevery=100)
# plt.plot(x_smooth, paretoucb1_one_smooth, label="ParetoUCB1:1_user", linestyle='-', marker='*', markevery=100)
plt.grid()
leg = plt.legend(fontsize=9, prop={'size': 6}, loc=0)
leg.get_frame().set_linewidth(0.0)
plt.xlabel('Number of trials')
plt.ylabel('CTR')
plt.ylim(ymin=0)

plt.savefig('weibo_ctr_1.pdf', bbox_inches='tight')  # fig with label
plt.show()
