import pandas as pd
import json
import os
import glob
import numpy as np
class Recall():

    def __init__(self, path, file_regex='*rankedlist.txt'):
        os.chdir(path)
        self.user_relevant_hotels = None
        self.rankedlist_files = glob.glob(file_regex)

        with open('D:\pycharm_projects\MOR-LinUCB\TripAdvisorModule\data\\user_relevant_hotel.json', 'r') as f:
            self.user_relevant_hotels = json.load(f)
        f.close()

    def plot(self):
        for filename in self.rankedlist_files:
            print(filename)
            f = open(filename)
            jdata = json.load(f)
            df = pd.DataFrame(jdata)
            df['b-rank-1']= df.apply(lambda row: self.get_is_relevant(row, 'rank1'), axis=1)
            df['b-rank-2'] = df.apply(lambda row: self.get_is_relevant(row, 'rank2'), axis=1)
            df_groupby_user_algo_rank1_sum = df.groupby(['user_id', 'algo'])['b-rank-1'].sum().reset_index()
            df_groupby_user_algo_rank2_sum = df.groupby(['user_id', 'algo'])['b-rank-2'].sum().reset_index()

            df_groupby_user_algo_rank1_sum['user-recall-rank-1'] = df_groupby_user_algo_rank1_sum.apply(
                lambda row: self.get_user_recall(row,'b-rank-1'), axis=1)
            df_rank_1_recall_per_algo = df_groupby_user_algo_rank1_sum.groupby(['algo'])['user-recall-rank-1'].mean()
            df_groupby_user_algo_rank2_sum['user-recall-rank-2'] = df_groupby_user_algo_rank2_sum.apply(
                lambda row: self.get_user_recall(row, 'b-rank-2'), axis=1)
            df_rank_2_recall_per_algo = df_groupby_user_algo_rank2_sum.groupby(['algo'])['user-recall-rank-2'].mean()

            print(df_rank_1_recall_per_algo)
            print(df_rank_2_recall_per_algo)

    def get_is_relevant(self, row, rank):
        rank_rec = row[rank]
        user = row['user_id']
        if rank_rec in self.user_relevant_hotels[user]:
            return 1
        else:
            return 0

    def get_user_recall(self, row, val):
        user = row['user_id']
        no_rel_rec = row[val]
        tot_ral = len(self.user_relevant_hotels[user]) *1.0
        user_recall = np.divide(no_rel_rec, tot_ral)
        return user_recall

if __name__ == '__main__':
    #PlotOne("..\\TripAdvisorModule\\server_output\\3\\", "plot1.pdf").plot("2019-02-*.txt")

    Recall("..\\TripAdvisorModule\\output\\7\\").plot()
    # PlotOne("..\\1\\", "plot1.pdf").plot("2019-02-*.txt")