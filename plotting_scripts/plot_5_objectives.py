__author__ = 'nirandikawanigasekara'

import File_Handler as fh
import numpy as np
import matplotlib.pyplot as plt


def get_ctr_vs_objcount_dict(l_files, param_s):
    t_dict = {}
    for file_n in l_files:
        ctrlist = fh.read_file_plot_1(file_n, param_s)
        final_ctr = ctrlist[-1]
        no_obj = fh.read_file_plot_meta_data(file_n, '#Objectives')
        print(file_n)
        assert no_obj not in t_dict.keys()
        t_dict[no_obj] = final_ctr
    # print(t_dict)
    return t_dict


paretoucb1_files = ['2018-01-24-10-20-42_ParetoUCB1.txt', '2018-01-24-10-38-31_ParetoUCB1.txt',
                    '2018-01-24-10-39-59_ParetoUCB1.txt',
                    '2018-01-24-12-05-01_ParetoUCB1.txt', '2018-01-24-10-45-03_ParetoUCB1.txt',
                    '2018-01-24-10-48-17_ParetoUCB1.txt',
                    '2018-01-24-10-50-35_ParetoUCB1.txt', '2018-01-24-10-53-25_ParetoUCB1.txt',
                    '2018-01-24-10-56-15_ParetoUCB1.txt']

morlinucb_files = ['2018-01-24-10-19-45_MOR_LinUCB.txt', '2018-01-24-10-37-44_MOR_LinUCB.txt',
                   '2018-01-24-10-39-08_MOR_LinUCB.txt',
                   '2018-01-24-10-43-58_MOR_LinUCB.txt', '2018-01-24-10-47-02_MOR_LinUCB.txt',
                   '2018-01-24-10-49-13_MOR_LinUCB.txt',
                   '2018-01-24-10-52-11_MOR_LinUCB.txt', '2018-01-24-10-54-58_MOR_LinUCB.txt',
                   '2018-01-24-12-03-52_MOR_LinUCB.txt']

paretoucb1_ctr_obj = get_ctr_vs_objcount_dict(paretoucb1_files, 'ParetoUCB1_ctr')
morlinucb_ctr_obj = get_ctr_vs_objcount_dict(morlinucb_files, 'MOR_LinUCB_ctr')

number_of_objectives = 11
x = np.array(range(2, number_of_objectives))
# x_smooth = np.linspace(x.min(), x.max(), number_of_objectives)

paretoucb1_y = [paretoucb1_ctr_obj[k] for k in sorted(paretoucb1_ctr_obj)]
morlinucb_y = [morlinucb_ctr_obj[k] for k in sorted(morlinucb_ctr_obj)]

plt.figure(figsize=(5, 2), dpi=300)
plt.plot(x, morlinucb_y, label="MOR_LinUCB", linestyle='-', marker='*', markevery=1)
plt.plot(x, paretoucb1_y, label="ParetoUCB1", linestyle='-', marker='.', markevery=1)
# plt.plot(x_smooth, morlinucb_one_smooth, label="MOR_LinUCB:1_user", linestyle='-', marker='*', markevery=100)
# plt.plot(x_smooth, paretoucb1_one_smooth, label="ParetoUCB1:1_user", linestyle='-', marker='*', markevery=100)
plt.grid()
leg = plt.legend(fontsize=9, prop={'size': 6}, loc=0)
leg.get_frame().set_linewidth(0.0)
plt.xlabel('Number of objectives')
plt.ylabel('CTR')
plt.ylim(ymin=1.2)

plt.savefig('ctr_5_objectives.pdf', bbox_inches='tight')  # fig with label
plt.show()

assert paretoucb1_ctr_obj.keys() == morlinucb_ctr_obj.keys()

print(paretoucb1_ctr_obj)
print(morlinucb_ctr_obj)
