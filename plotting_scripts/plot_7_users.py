__author__ = 'nirandikawanigasekara'

__author__ = 'nirandikawanigasekara'

import File_Handler as fh
import numpy as np
import matplotlib.pyplot as plt


def get_ctr_vs_objcount_dict(l_files, param_s):
    t_dict = {}
    for file_n in l_files:
        ctrlist = fh.read_file_plot_1(file_n, param_s)
        final_ctr = ctrlist[-1]
        no_obj = fh.read_file_plot_meta_data(file_n, 'User')
        print(file_n)
        assert no_obj not in t_dict.keys()
        t_dict[no_obj] = final_ctr
    #print(t_dict)
    return t_dict


paretoucb1_files = ['2018-01-24-17-15-44_ParetoUCB1.txt', '2018-01-24-17-19-47_ParetoUCB1.txt', '2018-01-24-17-21-50_ParetoUCB1.txt',
    '2018-01-24-17-31-21_ParetoUCB1.txt', '2018-01-24-17-36-09_ParetoUCB1.txt', '2018-01-24-17-40-15_ParetoUCB1.txt',
    '2018-01-24-17-46-32_ParetoUCB1.txt', '2018-01-24-18-06-04_ParetoUCB1.txt', '2018-01-24-18-08-23_ParetoUCB1.txt',
    '2018-01-24-18-10-46_ParetoUCB1.txt']

morlinucb_files = ['2018-01-24-17-14-27_MOR_LinUCB.txt', '2018-01-24-17-18-37_MOR_LinUCB.txt', '2018-01-24-17-20-41_MOR_LinUCB.txt',
                   '2018-01-24-17-30-12_MOR_LinUCB.txt', '2018-01-24-18-38-59_MOR_LinUCB.txt', '2018-01-24-18-40-37_MOR_LinUCB.txt',
                   '2018-01-24-18-43-52_MOR_LinUCB.txt', '2018-01-24-18-45-03_MOR_LinUCB.txt', '2018-01-24-18-46-12_MOR_LinUCB.txt',
                   '2018-01-24-18-48-00_MOR_LinUCB.txt']


paretoucb1_ctr_users = get_ctr_vs_objcount_dict(paretoucb1_files, 'ParetoUCB1_ctr')
morlinucb_ctr_users = get_ctr_vs_objcount_dict(morlinucb_files, 'MOR_LinUCB_ctr')

x = sorted(paretoucb1_ctr_users)
# x_smooth = np.linspace(x.min(), x.max(), number_of_objectives)

paretoucb1_y = [paretoucb1_ctr_users[k] for k in x]
morlinucb_y = [morlinucb_ctr_users[k] for k in x]

plt.figure(figsize=(5, 2), dpi=300)
plt.plot(x[1:], morlinucb_y[1:], label="MOR_LinUCB", linestyle='-', marker='*', markevery=1)
plt.plot(x[1:], paretoucb1_y[1:], label="ParetoUCB1", linestyle='-', marker='.', markevery=1)
# plt.plot(x_smooth, morlinucb_one_smooth, label="MOR_LinUCB:1_user", linestyle='-', marker='*', markevery=100)
# plt.plot(x_smooth, paretoucb1_one_smooth, label="ParetoUCB1:1_user", linestyle='-', marker='*', markevery=100)
plt.grid()
leg = plt.legend(fontsize=9, prop={'size': 6}, loc=0)
leg.get_frame().set_linewidth(0.0)
plt.xlabel('Number of Users')
plt.ylabel('CTR')
plt.ylim(ymin=1.5)

plt.savefig('ctr_7_users.pdf', bbox_inches='tight')  # fig with label
plt.show()

assert  paretoucb1_ctr_users.keys() == morlinucb_ctr_users.keys()
print(paretoucb1_ctr_users)
# print(morlinucb_ctr_users)

