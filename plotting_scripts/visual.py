import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt

df_rewards = pd.read_json('D:\pycharm_projects\MOR-LinUCB\TripAdvisorModule\data\\rewards.json', orient='index').fillna(value=0)

df = df_rewards[df_rewards['a_id']=='9E8430C387961CD4C9D6424403214B25']

sns.pairplot(df)

plt.show()