import numpy as np
import matplotlib.pyplot as plt
from scipy import  stats
import os, glob
import seaborn as sns
import pandas as pd
sns.set(style='darkgrid')
import json
import TripAdvisorModule.Constants as cnst


class PlotOne():
    def __init__(self, path, file_name):
        os.chdir(path)
        self.t_test_ctr = {}
        self.paretoucb1_files = []
        self.epsilongreedy_files = []
        self.morlinucb_wop_files = []
        self.sorlinucb1_files = []
        self.sorlinucb2_files = []
        self.sorlinucb3_files = []
        self.morlogucb_files = []
        self.file_name = file_name

    def get_average(self, filenames, val):
        data_list = []
        # avg = None
        # val_org = val
        for filename in filenames:
            f = open(filename)
            json_string = json.load(f)
            ranking_positions = json_string['RankingPositions']
            # ctr_vals = cnst.calculate_ctr(json_string[val], ranking_positions)
            ctr_vals = json_string[val]
            print ctr_vals[-1]
            data_list.append(ctr_vals)
            # if 'SOR_LinUCB2' in filename:
            #    val =  "SOR_LinUCB2_ctr"
            # if 'SOR_LinUCB3' in filename:
            #    val = "SOR_LinUCB3_ctr"
            if val in self.t_test_ctr.keys():
                self.t_test_ctr[val].append(np.mean(ctr_vals))
            else:
                self.t_test_ctr[val] = [np.mean(ctr_vals)]
            f.close()
            # val = val_org
        a = np.array(data_list)
        avg = np.mean(a, axis=0)
        return avg

    def plot(self, file_regex):
        for filename in glob.glob(file_regex):
            print(filename)
            if "MOR_LinUCB_wop" in filename:
                self.morlinucb_wop_files.append(filename)
            elif "ParetoUCB1" in filename:
                self.paretoucb1_files.append(filename)
            elif "SOR_LinUCB1" in filename:
                self.sorlinucb1_files.append(filename)
            # elif "SOR_LinUCB2" in filename:
            #    self.sorlinucb2_files.append(filename)
            elif "SOR_LinUCB3" in filename:
                self.sorlinucb3_files.append(filename)
            elif "EpsilonGreedy" in filename:
                self.epsilongreedy_files.append(filename)
            elif "MOR_LogUCB" in filename:
                self.morlogucb_files.append(filename)

        print("MOR_LinUCB_wop files length",len(self.morlinucb_wop_files))
        print("ParetoUCB1 files length", len(self.paretoucb1_files))
        print("SOR_LinUCB1 files length", len(self.sorlinucb1_files))
        print("SOR_LinUCB2 files length", len(self.sorlinucb2_files))
        print("SOR_LinUCB3 files length", len(self.sorlinucb3_files))
        print("MOR_logUCB files length", len(self.morlogucb_files))
        print("EpsilonGreedy files length", len(self.epsilongreedy_files))

        morlogucb_data = self.get_average(self.morlogucb_files, "MOR_LogUCB_wop_ctr")
        print("mor-logucb final ctr", morlogucb_data[-1])
        paretoucb1_data = self.get_average(self.paretoucb1_files, "ParetoUCB1_ctr")
        morlinucb_wop_data = self.get_average(self.morlinucb_wop_files, "MOR_LinUCB_wop_ctr")
        sorlinucb1_data = self.get_average(self.sorlinucb1_files, "SOR_LinUCB_ctr")
        print('sorlinucb final ctr',sorlinucb1_data[-1])
        sorlinucb2_data = self.get_average(self.sorlinucb2_files, "SOR_LinUCB_ctr")
        sorlinucb3_data = self.get_average(self.sorlinucb3_files, "SOR_LinUCB_ctr")
        epsilongreedy_data = self.get_average(self.epsilongreedy_files, "EpsilonGreedy_ctr")
        print('epsilongreedy final ctr',epsilongreedy_data[-1])
        print(morlogucb_data.dtype)

        df_plot = pd.DataFrame({'Ranked-PUCB1': paretoucb1_data,
                                'MOU-UCB': morlogucb_data,
                                'Ranked-LinUCB-obj1': sorlinucb1_data,
                                # 'Ranked-LinUCB-obj2': sorlinucb2_data,
                                #'Ranked-LinUCB': sorlinucb3_data,
                                #'MORLinUCB': morlinucb_wop_data,
                                'EpsilonGreedy': epsilongreedy_data,
                                #'trial': range(1, len(morlogucb_data)+1)
                                })
        fig, ax = plt.subplots(figsize=(5,3),dpi=300)
        sns.set(font_scale=1.6)
        plt.tight_layout()
        sns.lineplot(data=df_plot)

        leg = plt.legend(fontsize=9, prop={'size': 8})
        leg.get_frame().set_linewidth(0.0)
        plt.xlabel('Number of trials')
        plt.ylabel('CTR')
        plt.ylim(ymin=0)

        plt.savefig(self.file_name)  # fig with label
        plt.show()

        print(self.t_test_ctr)

        # print(stats.ttest_rel(self.t_test_ctr['MOR_LogUCB_wop_ctr'], self.t_test_ctr['MOR_LinUCB_wop_ctr']))
        print(stats.ttest_rel(self.t_test_ctr['MOR_LogUCB_wop_ctr'], self.t_test_ctr['ParetoUCB1_ctr']))
        print(stats.ttest_rel(self.t_test_ctr['MOR_LogUCB_wop_ctr'], self.t_test_ctr['SOR_LinUCB_ctr']))
        # print(stats.ttest_rel(self.t_test_ctr['MOR_LogUCB_wop_ctr'], self.t_test_ctr['SOR_LinUCB2_ctr']))
        # print(stats.ttest_rel(self.t_test_ctr['MOR_LogUCB_wop_ctr'], self.t_test_ctr['SOR_LinUCB3_ctr']))
        print(stats.ttest_rel(self.t_test_ctr['MOR_LogUCB_wop_ctr'], self.t_test_ctr['EpsilonGreedy_ctr']))


        print(np.mean(self.t_test_ctr['MOR_LogUCB_wop_ctr']))
        # print(np.mean(self.t_test_ctr['MOR_LinUCB_wop_ctr']))
        print(np.mean(self.t_test_ctr['ParetoUCB1_ctr']))
        # print(np.mean(self.t_test_ctr['SOR_LinUCB_ctr']))
        # print(np.mean(self.t_test_ctr['SOR_LinUCB2_ctr']))
        print(np.mean(self.t_test_ctr['SOR_LinUCB_ctr']))
        print(np.mean(self.t_test_ctr['EpsilonGreedy_ctr']))


if __name__ == '__main__':
    #PlotOne("..\\TripAdvisorModule\\server_output\\3\\", "plot1.pdf").plot("2019-02-*.txt")

    PlotOne("..\\TripAdvisorModule\\output\\combined\\", "plot1.pdf").plot("*.txt")
    # PlotOne("..\\1\\", "plot1.pdf").plot("2019-02-*.txt")


