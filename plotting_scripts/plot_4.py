import numpy as np
import matplotlib.pyplot as plt
import operator

def autolabel(rects):
    """
    Attach a text label above each bar displaying its height
    """
    for rect in rects:
        height = rect.get_height()
        ax.text(rect.get_x() + rect.get_width()/2., 1.05*height,
                '%d' % int(height),
                ha='center', va='bottom', fontsize=7)


# data to plot
n_groups = 10

morlinucb = [548, 188, 204, 222, 231, 256, 292, 275, 298, 249]
paretoucb1 = [89, 263, 264, 261, 256, 256, 275, 259, 248, 269]


# create plot
fig, ax = plt.subplots(figsize=(5,3),dpi=300)
index = np.arange(n_groups)
bar_width = 0.35
opacity = 0.8

rects1 = plt.bar(index, morlinucb, bar_width,
                 alpha=opacity,
                 color='b',
                 label='MOR_LinUCB_Ranker')

rects2 = plt.bar(index + bar_width, paretoucb1, bar_width,
                 alpha=opacity,
                 color='g',
                 label='ParetoUCB1_Ranker')


plt.xlabel('Ranking Position')
plt.ylabel('User Clicks')
plt.xticks(index + bar_width, ('1', '2', '3', '4', '5', '6','7','8','9','10'))
leg= plt.legend(fontsize=9, prop={'size':6},loc=0)
leg.get_frame().set_linewidth(0.0)
plt.grid()
plt.tight_layout()
autolabel(rects1)
autolabel(rects2)

plt.savefig('ctr_4.pdf',bbox_inches='tight') # fig with label

plt.show()

