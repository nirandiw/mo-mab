import numpy as np
from matplotlib import pyplot as plt
import File_Handler as fh
from scipy import stats
import os, glob
import seaborn as sns
import json
import pandas as pd

sns.set(style='darkgrid')
#sns.set_palette("husl")


class Plot_Three_Object:
    def __init__(self, rba_, diff_):
        self.rba = rba_
        self.diff = diff_

class PlotThree():
    def __init__(self, path, file_name):
        os.chdir(path)
        self.t_test_ctr = {}
        self.paretoucb1_files = []
        self.epsilongreedy_files = []
        self.morlinucb_wop_files = []
        self.sorlinucb_files = []
        self.morlogucb_files = []
        self.file_name = file_name

        #self.avg_diff = None
        #self.avg_rba = None

        self.t_test_sum = {}
        self.length = 0


    def autolabel(self, rects):
        """
        Attach a text label above each bar displaying its height
        """
        for rect in rects:
            ax = plt.axes()
            height = rect.get_height()
            ax.text(rect.get_x() + rect.get_width()/2., 1.05*height,
                    '%d' % int(height),
                    ha='center', va='top', fontsize=7)

    def get_average(self, filenames, val_tot, val_rab):
        data_list = []

        for filename in filenames:
            f = open(filename)
            json_string = json.load(f)
            self.length = json_string['Trials']
            tot_clicks_vals = [sum(x) for x in zip(*json_string[val_tot])]
            data_list.append(tot_clicks_vals)

            if val_tot in self.t_test_ctr.keys():
                self.t_test_ctr[val_tot].append(tot_clicks_vals)
            else:
                self.t_test_ctr[val_tot] = [tot_clicks_vals]
            f.close()
        a = np.array(data_list)

        avg = np.mean(a, axis=0)
        return avg

    def get_patk(self, click_sum):
        p_at_k_list = []
        cum_v =0
        for idx, v in enumerate(click_sum):
            #cum_v += v
            p_at_k = v/float(self.length)
            p_at_k_list.append(p_at_k)
        return np.array(p_at_k_list)

    def plot(self,file_regex ):

        for filename in glob.glob(file_regex):
            print(filename)
            if "MOR_LinUCB_wop" in filename:
                self.morlinucb_wop_files.append(filename)
            elif "ParetoUCB1" in filename:
                self.paretoucb1_files.append(filename)
            elif "SOR_LinUCB" in filename:
                self.sorlinucb_files.append(filename)
            elif "MOR_LogUCB" in filename:
                self.morlogucb_files.append(filename)
            elif "EpsilonGreedy" in filename:
                self.epsilongreedy_files.append(filename)


        print("MOR_LinUCB_wop files length",len(self.morlinucb_wop_files))
        print("ParetoUCB1 files length", len(self.paretoucb1_files))
        print("SOR_LinUCB files length", len(self.sorlinucb_files))
        print("MOR_logUCB files length", len(self.morlogucb_files))
        print("EpsilonGreedy files length", len(self.epsilongreedy_files))

        paretoucb1_data = self.get_average(self.paretoucb1_files,"ParetoUCB1_click_position","ParetoUCB1_RBA" ).tolist()
        sorlinucb_data = self.get_average(self.sorlinucb_files, "SOR_LinUCB_click_position", "SOR_LinUCB_RBA").tolist()
        morlogucb_data = self.get_average(self.morlogucb_files, "MOR_LogUCB_wop_click_position", "MOR_LogUCB_wop_RBA").tolist()
        morlinucb_wop_data = self.get_average(self.morlinucb_wop_files,"MOR_LinUCB_wop_click_position", "MOR_LinUCB_wop_RBA").tolist()
        epsilongreedy_data = self.get_average(self.epsilongreedy_files,"EpsilonGreedy_click_position","EpsilonGreedy_RBA" ).tolist()

        print(len(morlogucb_data))
        df_plot = pd.DataFrame({'Ranked-PUCB1': paretoucb1_data,
                                'MOU-UCB': morlogucb_data,
                                # 'Ranked-LinUCB-obj1': sorlinucb1_data,
                                # 'Ranked-LinUCB-obj2': sorlinucb2_data,
                                'EpsilonGreedy': epsilongreedy_data,
                                'Ranked-LinUCB': sorlinucb_data,
                                # 'MORLinUCB': morlinucb_wop_data,
                                'Ranking Position': range(1, len(morlogucb_data)+1)
                                })

        df_plot_flat = pd.melt(df_plot, value_vars=['EpsilonGreedy', 'MOU-UCB', 'Ranked-LinUCB', 'Ranked-PUCB1'],
                               var_name='Algorithm', value_name='Clicks', id_vars=['Ranking Position'])

        fig, ax = plt.subplots(figsize=(5, 3), dpi=300)
        sns.set(font_scale=1.6)
        plt.tight_layout()
        sns.barplot(x='Ranking Position', y='Clicks', hue='Algorithm', data=df_plot_flat)
        leg = plt.legend(fontsize=9, prop={'size': 8})
        leg.get_frame().set_linewidth(0.0)
        # plt.xlabel('Number of trials')
        # plt.ylabel('CTR')
        plt.ylim(ymin=0)

        plt.savefig(self.file_name)  # fig with label
        plt.show()



        paretoucb_patk = self.get_patk(paretoucb1_data[:-1])
        morlogucb_patk = self.get_patk(morlogucb_data[:-1])
        # morlinucb_wop_patk = self.get_patk(morlinucb_wop_data[:-1])
        sorlinucb_patk = self.get_patk(sorlinucb_data[:-1])
        epsilongreedy_patk = self.get_patk(epsilongreedy_data[:-1])

        print('P@1')
        print(morlogucb_patk[0])
        print(paretoucb_patk[0])
        print(sorlinucb_patk[0])
        print(epsilongreedy_patk[0])

        print('P@2')
        print(morlogucb_patk[1])
        print(paretoucb_patk[1])
        print(sorlinucb_patk[1])
        print(epsilongreedy_patk[1])

        print('P@3')
        print(morlogucb_patk[2])
        print(paretoucb_patk[2])
        print(sorlinucb_patk[2])
        print(epsilongreedy_patk[2])

        print('P@4')
        print(morlogucb_patk[3])
        print(paretoucb_patk[3])
        print(sorlinucb_patk[3])
        print(epsilongreedy_patk[3])

        print('P@5')
        print(morlogucb_patk[4])
        print(paretoucb_patk[4])
        print(sorlinucb_patk[4])
        print(epsilongreedy_patk[4])



if __name__ == '__main__':
    PlotThree("..\\TripAdvisorModule\\output\\combined\\", "plot3.pdf").plot("2019-02-*.txt")

    # PlotThree("..\\1", "plot3_sim.pdf").plot("2019-02-*.txt")



