__author__ = 'nirandikawanigasekara'

import numpy as np
from scipy import stats
import os, glob
import json
import TripAdvisorModule.Constants as cnst
import seaborn as sns
import matplotlib.pyplot as plt
import pandas as pd

os.chdir("..\\1")

paretoucb1_files = []
morlinucb_files = []
# morlinucb_wop_files = []
sorlinucb_files = []
morlogucb_files =[]
epsilon_files =[]

for filename in glob.glob("2019-02-*.txt"):
    print(filename)
    if "EpsilonGreedy" in filename:
        epsilon_files.append(filename)
    elif "ParetoUCB1" in filename:
        paretoucb1_files.append(filename)
    elif "SOR_LinUCB3" in filename:
        sorlinucb_files.append(filename)
    elif "MOR_LogUCB" in filename:
        morlogucb_files.append(filename)

t_test_values ={}

def get_MRR(filenames, val):
    data_list = []
    data_list_mrr = []
    for filename in filenames:
        f = open(filename)
        json_string = json.load(f)
        click_positions = json_string[val]
        mrr_at_t = []
        for l in click_positions:
            if 1 not in l:
                reciprocal = 0
            else:
                first_click_index = l.index(1)+1.0
                reciprocal = np.divide(1.0, first_click_index)
            mrr_at_t.append(reciprocal)

        cum_mrr = np.cumsum(mrr_at_t)
        cum_mrr_over_time = np.divide(cum_mrr, np.arange(1, cnst.T + 1), dtype=float)
        data_list.append(cum_mrr_over_time[-1])
        data_list_mrr.append(mrr_at_t)

    a = np.array(data_list)
    avg = np.mean(a, axis=0)
    avg_mrr_series = np.mean(np.array(data_list_mrr), axis=0)
    t_test_values[val] = data_list
    return avg, avg_mrr_series

morlogucb_mrr_results = get_MRR(morlogucb_files, 'MOR_LogUCB_wop_click_position')
epsilon_results = get_MRR(epsilon_files, 'EpsilonGreedy_click_position')
paretoucb_results = get_MRR(paretoucb1_files, 'ParetoUCB1_click_position')
sorlinucb_results = get_MRR(sorlinucb_files, 'SOR_LinUCB_click_position')

print("MRR")
print(morlogucb_mrr_results[0])
print(paretoucb_results[0])
print(sorlinucb_results[0])
print(epsilon_results[0])

print("T-Test")
print(t_test_values)
print(stats.ttest_rel(t_test_values['MOR_LogUCB_wop_click_position'], t_test_values['ParetoUCB1_click_position']))
print(stats.ttest_rel(t_test_values['MOR_LogUCB_wop_click_position'], t_test_values['SOR_LinUCB_click_position']))
print(stats.ttest_rel(t_test_values['MOR_LogUCB_wop_click_position'], t_test_values['EpsilonGreedy_click_position']))

df_plot = pd.DataFrame({'Ranked-PUCB1': paretoucb_results[1],
                        'MOU-UCB': morlogucb_mrr_results[1],
                        # 'Ranked-LinUCB-obj1': sorlinucb1_data,
                        # 'Ranked-LinUCB-obj2': sorlinucb2_data,
                        'Ranked-LinUCB': sorlinucb_results[1],
                        # 'MORLinUCB': morlinucb_wop_data,
                        'EpsilonGreedy': epsilon_results[1]
                        })

print(morlogucb_mrr_results[1])

# plt.figure(figsize=(5, 4), dpi=300)
sns.lineplot(data=df_plot[::100], dashes=False, markers=True)

leg = plt.legend(fontsize=9, prop={'size': 8})
leg.get_frame().set_linewidth(0.0)
plt.xlabel('Number of trials')
plt.ylabel('CTR')
plt.ylim(ymin=0)

plt.savefig("..\\1\\plot8.pdf")  # fig with label
plt.show()