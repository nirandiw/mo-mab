import json
import pandas as pd
import glob
import os
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
import sklearn.metrics as skmat

class PlotInstantaneousError():

    def __init__(self, path, file_name):
        os.chdir(path)
        self.t_test_ctr = {}
        self.paretoucb1_files = []
        self.epsilongreedy_files = []
        self.morlinucb_wop_files = []
        self.sorlinucb1_files = []
        self.sorlinucb2_files = []
        self.sorlinucb3_files = []
        self.morlogucb_files = []
        self.file_name = file_name

    def plot(self, file_regex):
        for filename in glob.glob(file_regex):
            print(filename)
            if "MOR_LinUCB_wop" in filename:
                self.morlinucb_wop_files.append(filename)
            elif "ParetoUCB1" in filename:
                self.paretoucb1_files.append(filename)
            elif "SOR_LinUCB1" in filename:
                self.sorlinucb1_files.append(filename)
            # elif "SOR_LinUCB2" in filename:
            #    self.sorlinucb2_files.append(filename)
            elif "SOR_LinUCB3" in filename:
                self.sorlinucb3_files.append(filename)
            elif "EpsilonGreedy" in filename:
                self.epsilongreedy_files.append(filename)
            elif "MOR_LogUCB" in filename:
                self.morlogucb_files.append(filename)

        morlogucb_predict_data = self.avg_inst_error(self.morlogucb_files, 'estimated_reward_for_recommened_a')
        morlogucb_true_data = self.avg_inst_error(self.morlogucb_files, 'real_reward_for_recommended_a')

        RMSE_over_t = []
        abs_error_over_t = []
        s=0,
        step = 1000
        for i in range(0, len(morlogucb_predict_data), step):
            RMSE = skmat.mean_squared_error(morlogucb_true_data[s:e], morlogucb_predict_data[s:e])
            print('rmse', RMSE)
            RMSE_over_t.append(RMSE)
            abs_error = skmat.mean_absolute_error()
            print('abs: ', abs_error)
            abs_error_over_t.append(abs_error)
            s = s+step


        df_plot = pd.DataFrame({'RMSE':RMSE_over_t,
                                'ABSOLUTE_ERROR': abs_error_over_t})
        fig, ax = plt.subplots(figsize=(5, 3), dpi=300)
        sns.set(font_scale=1.6)
        plt.tight_layout()
        sns.lineplot(data=df_plot)

        leg = plt.legend(fontsize=9, prop={'size': 8})
        leg.get_frame().set_linewidth(0.0)
        plt.xlabel('Number of trials')
        plt.ylabel('CTR')
        plt.ylim(ymin=0)

        plt.savefig(self.file_name)  # fig with label
        plt.show()

    def avg_inst_error(self, filenames, val):
        data_list = []
        # avg = None
        # val_org = val
        for filename in filenames:
            f = open(filename)
            json_string = json.load(f)
            if val in json_string:
                avg_inst_error = json_string[val]
                print(avg_inst_error[-1])
                data_list.append(avg_inst_error)
            f.close()
            # val = val_org
        a = np.array(data_list)
        avg = np.mean(a, axis=0)
        return avg



if __name__ == '__main__':
    PlotInstantaneousError("..\\TripAdvisorModule\\output\\7\\", "plot_avg_err.pdf").plot("*.txt")