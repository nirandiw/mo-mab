import numpy as np
from matplotlib import pyplot as plt
import File_Handler as fh
from scipy import stats
import os, glob
import seaborn as sns
import json

sns.set(style='darkgrid')


class Plot_Three_Object:
    def __init__(self, rba_, diff_):
        self.rba = rba_
        self.diff = diff_

class PlotThree():
    def __init__(self, path, file_name):
        os.chdir(path)
        self.t_test_ctr = {}
        self.paretoucb1_files = []
        self.epsilongreedy_files = []
        self.morlinucb_wop_files = []
        self.sorlinucb_files = []
        self.morlogucb_files = []
        self.file_name = file_name

        #self.avg_diff = None
        #self.avg_rba = None

        self.t_test_sum = {}


    def get_average(self,filenames, val_tot, val_rab):
        data_list = []

        for filename in filenames:
            f = open(filename)
            json_string = json.load(f)
            tot_clicks_vals = [sum(x) for x in zip(*json_string[val_tot])]
            data_list.append(tot_clicks_vals)

            if val_tot in self.t_test_ctr.keys():
                self.t_test_ctr[val_tot].append(tot_clicks_vals)
            else:
                self.t_test_ctr[val_tot] = [tot_clicks_vals]
            f.close()
        a = np.array(data_list)
        avg = np.mean(a, axis=1)
        return avg

    def get_patk(self, click_sum):
        p_at_k_list = []
        cum_v =0
        for idx, v in enumerate(click_sum):
            cum_v += v
            p_at_k = cum_v/float(idx+1)
            p_at_k_list.append(p_at_k)
        return np.array(p_at_k_list)



    def plot(self,file_regex ):

        for filename in glob.glob(file_regex):
            print(filename)
            if "MOR_LinUCB_wop" in filename:
                self.morlinucb_wop_files.append(filename)
            elif "ParetoUCB1" in filename:
                self.paretoucb1_files.append(filename)
            elif "SOR_LinUCB" in filename:
                self.sorlinucb_files.append(filename)
            elif  "MOR_LogUCB" in filename:
                self.morlogucb_files.append(filename)


        print("MOR_LinUCB_wop files length",len(self.morlinucb_wop_files))
        print("ParetoUCB1 files length", len(self.paretoucb1_files))
        print("SOR_LinUCB files length", len(self.sorlinucb_files))
        print("MOR_logUCB files length", len(self.morlogucb_files))

        # data to plot
        n_groups = 10
        paretoucb = self.get_average(self.paretoucb1_files,"ParetoUCB1_click_position","ParetoUCB1_RBA" )
        morlogucb = self.get_average(self.morlogucb_files, "MOR_LogUCB_wop_click_position", "MOR_LogUCB_wop_RBA")
        morlinucb_wop = self.get_average(self.morlinucb_wop_files,"MOR_LinUCB_wop_click_position", "MOR_LinUCB_wop_RBA")
        sorlinucb = self.get_average(self.sorlinucb_files,"SOR_LinUCB_click_position", "SOR_LinUCB_RBA")


        paretoucb_diff = paretoucb.diff
        paretoucb_rba = paretoucb.rba
        morlogucb_diff = morlogucb.diff
        morlogucb_rba = morlogucb.rba
        morlinucb_wop_diff = morlinucb_wop.diff
        morlinucb_wop_rba = morlinucb_wop.rba
        sorlinucb_diff = sorlinucb.diff
        sorlinucb_rba = sorlinucb.rba

        paretoucb_sum = np.add(paretoucb_diff, paretoucb_rba)
        morlogucb_sum = np.add(morlogucb_diff, morlogucb_rba)
        morlinucb_wop_sum = np.add(morlinucb_wop_diff, morlinucb_wop_rba)
        sorlinucb_sum = np.add(sorlinucb_diff, sorlinucb_rba)

        paretoucb_patk = self.get_patk(paretoucb_sum)
        morlogucb_patk = self.get_patk(morlogucb_sum)
        morlinucb_wop_patk = self.get_patk(morlinucb_wop_sum)
        sorlinucb_patk = self.get_patk(sorlinucb_sum)

        # create plot
        fig, ax = plt.subplots(figsize=(5, 3), dpi=300)
        index = np.arange(n_groups)
        bar_width = 0.2
        opacity = 0.5

        #colors = pl.cm.jet(np.linspace(0,1,20))

        rects1 = plt.bar(index, morlogucb_rba, bar_width,
                         color='b',
                         label='MOR-LogUCB',
                         linewidth=0)
        rects1_1 = plt.bar(index, morlogucb_diff, bar_width,
                         color='b',
                         label='MOR-LogUCB_Random',
                           bottom=morlogucb_rba,
                           linewidth=0)

        rects3 = plt.bar(index+bar_width, morlinucb_wop_rba, bar_width,
                         color='r',
                         label='MOR-LinUCB-woP',
                         linewidth=0)
        rects3_1 = plt.bar(index+bar_width, morlinucb_wop_diff, bar_width,
                         color='r',
                         label='MOR-LinUCB_woP_Random',
                           bottom=morlinucb_wop_rba,
                           linewidth=0)
        '''
        rects2 = plt.bar(index + (2*bar_width), paretoucb_rba, bar_width,
                         color='g',
                         label='Ranked-PUCB1',
                         linewidth=0)
        
        rects2_1 = plt.bar(index + (2*bar_width), paretoucb_diff, bar_width,
                         color='g',
                         label='PUCB1-Random',
                        bottom=paretoucb_rba,
                           linewidth=0)
        
        rects4 = plt.bar(index +(3* bar_width), sorlinucb_rba, bar_width,
                        color='lightskyblue',
                         label='Ranked-LinUCB',
                         linewidth=0)
        
        rects4_1 = plt.bar(index+(3*bar_width), sorlinucb_diff, bar_width,
                           color='lightskyblue',
                         label='Ranked-LinUCB_Random',
                           bottom=sorlinucb_rba,
                           linewidth=0)
        
        '''

        x = index+bar_width

        #plt.plot(x, morlinucb_patk, label="P@k for MOR_LinUCB", linestyle='-', color='deeppink', linewidth=1.5, marker='*')
        #plt.plot(x, paretoucb_patk, label="P@k for PUCB1", linestyle='-', color='maroon', linewidth=1.5, marker='d')

        plt.xlabel('Ranking Position')
        plt.ylabel('User Clicks')
        plt.xticks(index + bar_width, ('1', '2', '3', '4', '5', '6','7','8','9','10'))
        handles, labels = ax.get_legend_handles_labels()
        ax.legend(handles, labels)
        leg = ax.legend(handles[1::2], labels[0::2], loc=9, fontsize=7)
        leg.get_frame().set_linewidth(0.0)

        # plt.grid()
        #plt.tight_layout()
        #autolabel(rects1)
        #autolabel(rects2)
        # autolabel(rects1_1)
        # autolabel(rects2_1)
        plt.savefig('ctr_ECIR_3.pdf',bbox_inches='tight') # fig with label
        plt.show()

        print('P@1')
        print(morlogucb_patk[0])
        print(morlinucb_wop_patk[0])
        print(paretoucb_patk[0])
        print(sorlinucb_patk[0])

        print('P@2')
        print(morlogucb_patk[1])
        print(morlinucb_wop_patk[1])
        print(paretoucb_patk[1])
        print(sorlinucb_patk[1])

        print('P@3')
        print(morlogucb_patk[2])
        print(morlinucb_wop_patk[2])
        print(paretoucb_patk[2])
        print(sorlinucb_patk[2])

        print('P@4')
        print(morlogucb_patk[3])
        print(morlinucb_wop_patk[3])
        print(paretoucb_patk[3])
        print(sorlinucb_patk[3])

        print('P@5')
        print(morlogucb_patk[4])
        print(morlinucb_wop_patk[4])
        print(paretoucb_patk[4])
        print(sorlinucb_patk[4])

        t_test_patk = {}

        for k in self.t_test_sum.keys():
            for iter in self.t_test_sum[k]:
                iter_patk =[]
                for idx, val in enumerate(iter):
                    iter_patk.append(val /float(1+idx))
                if k in t_test_patk.keys():
                    t_test_patk[k].append(iter_patk)
                else:
                    t_test_patk[k] = [iter_patk]

        print(t_test_patk)
        tmp = [item[0] for item in t_test_patk['MOR_LogUCB_wop_click_position']]
        tmp2 = [item[0] for item in t_test_patk['MOR_LinUCB_wop_click_position']]


        print(stats.ttest_rel([item[0] for item in t_test_patk['MOR_LogUCB_wop_click_position']], [item[0] for item in t_test_patk['MOR_LinUCB_wop_click_position']]))
        print(stats.ttest_rel([item[0] for item in t_test_patk['MOR_LogUCB_wop_click_position']], [item[0] for item in t_test_patk['ParetoUCB1_click_position']]))
        print(stats.ttest_rel([item[0] for item in t_test_patk['MOR_LogUCB_wop_click_position']], [item[0] for item in t_test_patk['SOR_LinUCB_click_position']]))




if __name__ == '__main__':
    PlotThree("..\\output-1\\","plot_3.pdf").plot("2019-01-*.txt")


