import pandas as pd
import numpy as np
filepath = '.\\track1\\'

def user_profile_general():

    user_profile_org = pd.read_csv(filepath+"user_profile.txt",  delimiter='\t', header=None)
    user_profile_org.columns =['userid', 'byear', 'gender', 'n_tweets', 'tags']
    print(user_profile_org.loc[user_profile_org.userid == 373407, 'byear'])

    print(user_profile_org.shape)
    print(len(pd.unique(user_profile_org['byear'])))
    user_profile_modified = user_profile_org
    # remove error entries and entries above the current year=2017
    user_profile_modified['byear'] = user_profile_org['byear'].replace(r"[0-9]*-[0-9]*-*", np.nan, regex=True)
    user_profile_modified['byear'] = pd.to_numeric(user_profile_modified['byear'])
    user_profile_modified.loc[user_profile_modified.byear > 2017, 'byear'] = np.nan

    # creating new column
    user_profile_modified['age'] = 2017 - user_profile_modified['byear']

    user_profile_modified['age_cat'] = user_profile_modified['age']

    user_profile_modified['age_cat'] = pd.cut(user_profile_modified['age_cat'], [0, 19, 25, 30, 35, 40, 45, 50, 60, 100, 1000], labels=[1, 2, 3, 4, 5, 6, 7, 8, 9, 10], right=True)

    user_profile_modified = pd.get_dummies(user_profile_modified, columns=['age_cat'], prefix=['age_cat'])
    print(user_profile_modified.shape)
    print(user_profile_modified.head())

    # user_action_org = pd.read_csv(filepath+"user_action.txt", delimiter='\t', header=None)
    # user_action_org.columns = ['userid', 'destine_u', 'n_at', 'n_retweet', 'n_comments']
    # print(user_action_org.head())
    # print(len(pd.unique(user_profile_org['userid'])))



# process actions - think how to model this
# process the keywords

# merge with the svd tags



def user_profile_keyword_construction():
    df_user_keyword_modified = pd.read_csv(filepath+'user_key_word_modified.txt', header=None)
    df_user_keyword_modified.columns =['userid', 'keyword', 'weight']
    print(df_user_keyword_modified.shape)
    df_user_keyword_modified = df_user_keyword_modified.drop_duplicates()
    print(df_user_keyword_modified.shape)
    print(len(pd.unique(df_user_keyword_modified.userid)))



    df_temp = df_user_keyword_modified.sort_values('weight').groupby('userid').head(1).reset_index(drop=True)
    print(df_temp.shape[0])


    df_item = pd.read_csv(filepath+'item.txt', header=None, delimiter='\t')
    df_item.columns = ['item', 'tags', 'keywords']

    df_item_keywords_weighted = df_temp[df_temp.userid.isin(df_item['item'])].reset_index(drop=True)
    print(df_item_keywords_weighted.shape)
    print(df_item_keywords_weighted.head())
    print(len(pd.unique(df_item_keywords_weighted['keyword'])))
    print(df_item_keywords_weighted.max(0))
    print(df_item_keywords_weighted.min(0))

    df_item_keywords_weighted['org_keyword'] = df_item_keywords_weighted['keyword']

    df_item_keywords_weighted = pd.get_dummies(df_item_keywords_weighted, columns=['keyword'])

    print(df_item_keywords_weighted.shape)
    print(df_item_keywords_weighted.head())


    df_user_keyword_weights = df_user_keyword_modified[df_user_keyword_modified.keyword.isin(df_item_keywords_weighted.org_keyword)].reset_index(drop=True)
    print(len(pd.unique(df_user_keyword_weights['userid'])))

    print(len(pd.unique(df_user_keyword_weights.keyword)))


    df_user_not_keyword_weights = df_user_keyword_modified[
        ~df_user_keyword_modified.keyword.isin(df_item_keywords_weighted.org_keyword)].reset_index(drop=True)
    print(len(pd.unique(df_user_not_keyword_weights.userid)))
    df_user_not_represented = df_user_not_keyword_weights[~df_user_not_keyword_weights.userid.isin(df_user_keyword_weights.userid)].reset_index(drop=True)
    print(len(pd.unique(df_user_not_represented.userid)))

    df_additional_keywords = df_temp[df_temp.userid.isin(df_user_not_represented.userid)].reset_index(drop=True)
    print(len(pd.unique(df_additional_keywords.keyword)))

if __name__ == '__main__':
    user_profile_general()







