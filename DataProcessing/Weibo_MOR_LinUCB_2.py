import pandas as pd
import os.path
from ast import literal_eval
import numpy as np

filepath = '.\\track1\\'




def generate_dim1_features(df_s_users, df_items_dim1):
    df_modified_user_keyword = pd.read_csv(filepath + "modified_user_keyword.csv")

    df_only_items_keywords = df_modified_user_keyword[
        df_modified_user_keyword.userid.isin(df_items_dim1.itemid)].reset_index(drop=True)

    assert df_only_items_keywords.shape[0] == df_items_dim1.shape[0]

    df_only_users_keywords = df_modified_user_keyword[
        df_modified_user_keyword.userid.isin(df_s_users.userid)].reset_index(drop=True)
    assert df_only_users_keywords.shape[0] == df_s_users.shape[0]

    final_columns = ['kw_1', 'kw_2', 'kw_3', 'kw_4', 'kw_5']
    df_dim1_final_features = pd.DataFrame(columns=final_columns)
    # df_dim1_final_features.set_index(['itemid','userid'])
    item_meta_data = {}

    if os.path.exists(filepath + "mergned_user_item_keywords.csv"):
        print("reading existing file for 1099 users and 675 items")
        df_merged_user_item_keywords = pd.read_csv(filepath + 'mergned_user_item_keywords.csv')
    else:
        df_only_users_keywords['track_key'] = 0
        df_only_items_keywords['track_key'] = 0
        df_merged_user_item_keywords = pd.merge(df_only_items_keywords, df_only_users_keywords, on='track_key').drop(
            ['track_key'], axis=1)
        df_merged_user_item_keywords.to_csv(filepath + 'mergned_user_item_keywords.csv', index=False)
    print(df_merged_user_item_keywords.shape)

    df_merged_user_item_keywords['u_i_w_k'] = df_merged_user_item_keywords.apply(
        lambda row: build_user_item_weights_vector_per_row(row), axis=1)

    df_merged_user_item_keywords.to_csv(filepath + 'user_item_weighted_keywords.csv', index=False)

    print(df_merged_user_item_keywords.shape)

