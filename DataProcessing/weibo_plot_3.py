import numpy as np
import matplotlib.pyplot as plt
import operator
import File_Handler as fh
from scipy import stats
import seaborn
input_file_path = ".\\track1\\outputs\\"


class Plot_Three_Object:
    def __init__(self, rba_, diff_):
        self.rba = rba_
        self.diff = diff_


def autolabel(rects):
    """
    Attach a text label above each bar displaying its height
    """
    for rect in rects:
        height = rect.get_height()
        ax.text(rect.get_x() + rect.get_width() / 2., 1.05 * height,
                '%d' % int(height),
                ha='center', va='top', fontsize=7)


avg_diff = None
avg_rba = None

t_test_tot = {}

def get_average(filenames, val_tot, val_rab):
    tot_clicks = []
    rba_clicks = []
    for f in filenames:
        f = input_file_path + f
        tot_clicks.append(fh.read_file_plot_3_tot_clicks(val_tot, f))
        rba_clicks.append(fh.read_file_plot_3_RBA_clicks(val_rab, f))

    rba_arr = np.array(rba_clicks) # np.array(fh.read_file_plot_3_RBA_clicks(val_rab, f))
    tot_arr = np.array(tot_clicks) # np.array(fh.read_file_plot_3_tot_clicks(val_tot, f))
    diff_arr = np.subtract(tot_arr, rba_arr)

    t_test_tot[val_tot] = tot_arr
    avg_diff = np.mean(diff_arr, axis=0) # diff_arr  #
    avg_rba = np.mean(rba_arr, axis=0) # rba_arr  #

    obj = Plot_Three_Object(avg_rba, avg_diff)
    return obj


def get_patk(click_sum):
    p_at_k_list = []
    cum_v = 0
    for idx, v in enumerate(click_sum):
        cum_v += v
        p_at_k = cum_v / float(idx + 1)
        p_at_k_list.append(p_at_k)
    return np.array(p_at_k_list)


paretoucb1_files = ["2018-01-25-03-12-52_Weibo_ParetoUCB1.txt", "2018-01-30-17-39-30_Weibo_ParetoUCB1.txt",
                    "2018-01-30-19-16-55_Weibo_ParetoUCB1.txt"]
morlinucb_files = ["2018-01-24-19-04-54_Weibo_MOR_LinUCB.txt", "2018-01-30-00-39-44_Weibo_MOR_LinUCB.txt",
                   "2018-01-30-18-02-51_Weibo_MOR_LinUCB.txt"]
morlinucb_wop_files = ["2018-05-10-11-57-23_Weibo_MOR_LinUCB_WO_Pareto_CIKM.txt","2018-05-20-19-50-40_Weibo_MOR_LinUCB_wop_CIKM.txt","2018-05-20-21-12-08_Weibo_MOR_LinUCB_wop_CIKM.txt" ]
sorlinucb_dim1_files = ['2018-05-11-13-14-43_Weibo_SOR_dim1_LinUCB_CIKM.txt', "2018-05-21-15-22-00_Weibo_SOR_dim1_LinUCB_CIKM.txt",
"2018-05-21-15-22-39_Weibo_SOR_dim1_LinUCB_CIKM.txt"]
sorlinucb_dim2_files = ["2018-05-13-23-24-18_Weibo_SOR_dim2_LinUCB_CIKM.txt","2018-05-21-04-25-45_Weibo_SOR_dim2_LinUCB_CIKM.txt","2018-05-21-04-27-42_Weibo_SOR_dim2_LinUCB_CIKM.txt"]
sorlinucb_dim3_files = ["2018-05-14-11-48-50_Weibo_SOR_dim3_LinUCB_CIKM.txt","2018-05-20-23-55-43_Weibo_SOR_dim3_LinUCB_CIKM.txt", "2018-05-21-03-59-16_Weibo_SOR_dim3_LinUCB_CIKM.txt"]

# data to plot
n_groups = 10
paretoucb = get_average(paretoucb1_files, "ParetoUCB1_click_position", "ParetoUCB1_RBA")
morlinucb = get_average(morlinucb_files, "MOR_LinUCB_click_position", "MOR_LinUCB_RBA")
morlinucb_wop = get_average(morlinucb_wop_files, "MOR_LinUCB_wop_click_position", "MOR_LinUCB_wop_RBA")
sorlinucb_dim1 = get_average(sorlinucb_dim1_files, "SOR_dim1_LinUCB_click_position", "SOR_dim1_LinUCB_RBA")
sorlinucb_dim2 = get_average(sorlinucb_dim2_files, "SOR_dim2_LinUCB_click_position", "SOR_dim2_LinUCB_RBA")
sorlinucb_dim3 = get_average(sorlinucb_dim3_files, "SOR_dim3_LinUCB_click_position", "SOR_dim3_LinUCB_RBA")

paretoucb_diff = paretoucb.diff
print(paretoucb_diff)
paretoucb_rba = paretoucb.rba
print(paretoucb_rba)
morlinucb_diff = morlinucb.diff
morlinucb_rba = morlinucb.rba

morlinucb_wop_diff = morlinucb_wop.diff
print(morlinucb_wop_diff)
morlinucb_wop_rba = morlinucb_wop.rba
print(morlinucb_wop_rba)

sorlinucb_dim1_diff = sorlinucb_dim1.diff
sorlinucb_dim1_rba = sorlinucb_dim1.rba

sorlinucb_dim2_diff = sorlinucb_dim2.diff
sorlinucb_dim2_rba = sorlinucb_dim2.rba

sorlinucb_dim3_diff = sorlinucb_dim3.diff
sorlinucb_dim3_rba = sorlinucb_dim3.rba

paretoucb_sum = np.add(paretoucb_diff, paretoucb_rba)
morlinucb_sum = np.add(morlinucb_diff, morlinucb_rba)
morlinucb_wop_sum = np.add(morlinucb_wop_diff, morlinucb_wop_rba)
sorlinucb_dim1_sum = np.add(sorlinucb_dim1_diff, sorlinucb_dim1_rba)
sorlinucb_dim2_sum = np.add(sorlinucb_dim2_diff, sorlinucb_dim2_rba)
sorlinucb_dim3_sum = np.add(sorlinucb_dim3_diff, sorlinucb_dim3_rba)

paretoucb_patk = get_patk(paretoucb_sum)
morlinucb_patk = get_patk(morlinucb_sum)
morlinucb_wop_patk = get_patk(morlinucb_wop_sum)
sorlinucb_dim1_patk = get_patk(sorlinucb_dim1_sum)
sorlinucb_dim2_patk = get_patk(sorlinucb_dim2_sum)
sorlinucb_dim3_patk = get_patk(sorlinucb_dim3_sum)

# create plot
fig, ax = plt.subplots(figsize=(5, 3), dpi=300)
index = np.arange(n_groups)
bar_width = 0.15
opacity = 0.5
'''
rects1 = plt.bar(index, morlinucb_rba, bar_width,
                 color='b',
                 label='MOR-LinUCB',
                 linewidth=0,

                 )
rects1_1 = plt.bar(index, morlinucb_diff, bar_width,
                   color='b',
                   label='MOR_LinUCB_Random',
                   bottom=morlinucb_rba, linewidth=0)
'''
rects3 = plt.bar(index + bar_width, morlinucb_wop_rba, bar_width,
                 color='y',
                 label='MOR-LinUCB',
                 linewidth=0
                 )
rects3_1 = plt.bar(index + bar_width, morlinucb_wop_diff, bar_width,
                   color='y',
                   label='MOR_LinUCB_Random',
                   bottom=morlinucb_wop_rba, linewidth=0)

rects2 = plt.bar(index + (2 * bar_width), paretoucb_rba, bar_width,
                 color='g',
                 label='Ranked-PUCB1', linewidth=0)
rects2_1 = plt.bar(index + (2 * bar_width), paretoucb_diff, bar_width,
                   color='g',
                   label='PUCB1_Random',
                   bottom=paretoucb_rba, linewidth=0)

rects4 = plt.bar(index + (3 * bar_width), sorlinucb_dim1_rba, bar_width,
                 color='c',
                 label='Ranked-LinUCB-obj1',
                 linewidth=0
                 )
rects4_1 = plt.bar(index + (3 * bar_width), sorlinucb_dim1_diff, bar_width,
                   color='c',
                   label='Ranked-LinUCB_obj1_Random',
                   bottom=sorlinucb_dim1_rba, linewidth=0)

rects5 = plt.bar(index + (4 * bar_width), sorlinucb_dim2_rba, bar_width,
                 color='coral',
                 label='Ranked-LinUCB-obj2',
                 linewidth=0
                 )
rects5_1 = plt.bar(index + (4 * bar_width), sorlinucb_dim2_diff, bar_width,
                   color='coral',
                   label='Ranked-LinUCB_obj2_Random',
                   bottom=sorlinucb_dim2_rba, linewidth=0)

rects6 = plt.bar(index + (5 * bar_width), sorlinucb_dim3_rba, bar_width,
                 color='darkolivegreen',
                 label='Ranked-LinUCB-obj3',
                 linewidth=0
                 )
rects6_1 = plt.bar(index + (5 * bar_width), sorlinucb_dim3_diff, bar_width,
                   color='darkolivegreen',
                   label='Ranked-LinUCB_obj3_Random',
                   bottom=sorlinucb_dim3_rba, linewidth=0)

# x = index+bar_width
# plt.plot(index, morlinucb_patk, label="P@k for MOR_LinUCB", linestyle='-', color='deeppink', linewidth=1.5, marker='*')
# plt.plot(index+bar_width, paretoucb_patk, label="P@k for PUCB1", linestyle='-', color='maroon', linewidth=1.5, marker='d')
# plt.plot(index+(2*bar_width), morlinucb_wop_patk, label="P@k for MOR_LinUCB_wop", linestyle='-', color='b', linewidth=1.5, marker='*')

plt.xlabel('Ranking Position')
plt.ylabel('User Clicks')
plt.xticks(index + bar_width, ('1', '2', '3', '4', '5', '6', '7', '8', '9', '10'))

#plt.grid()
#plt.tight_layout()
# autolabel(rects1)
# autolabel(rects2)
# autolabel(rects1_1)
# autolabel(rects2_1)
handles, labels = ax.get_legend_handles_labels()
ax.legend(handles, labels)
print(handles)
print(labels[0::2])
leg = ax.legend(handles[1::2], labels[0::2], loc=9, fontsize=7)
leg.get_frame().set_linewidth(0.0)
plt.savefig('weibo_ctr_CIKM_3.pdf', bbox_inches='tight')  # fig with label

plt.show()

print('P@k')
print("morlinucb",morlinucb_patk)
print("morlinucb_wop",morlinucb_wop_patk)
print("paretoucb",paretoucb_patk)
print("sorlinucb_Dim1",sorlinucb_dim1_patk)
print("sorlincub_dim2",sorlinucb_dim2_patk)
print("sorlincub_dim3",sorlinucb_dim3_patk)


t_test_patk = {}

for k in t_test_tot.keys():
    for iter in t_test_tot[k]:
        iter_patk =[]
        for idx, val in enumerate(iter):
            iter_patk.append(val /float(1+idx))
        if k in t_test_patk.keys():
            t_test_patk[k].append(iter_patk)
        else:
            t_test_patk[k] = [iter_patk]

print(t_test_patk)
tmp = [item[0] for item in t_test_patk['MOR_LinUCB_click_position']]
tmp2 = [item[0] for item in t_test_patk['MOR_LinUCB_wop_click_position']]

print(tmp)
print(tmp2)
print("P@1")
print(stats.ttest_rel([item[0] for item in t_test_patk['MOR_LinUCB_wop_click_position']], [item[0] for item in t_test_patk['ParetoUCB1_click_position']]))
print(stats.ttest_rel([item[0] for item in t_test_patk['MOR_LinUCB_wop_click_position']], [item[0] for item in t_test_patk['MOR_LinUCB_click_position']]))
print(stats.ttest_rel([item[0] for item in t_test_patk['MOR_LinUCB_wop_click_position']], [item[0] for item in t_test_patk['SOR_dim3_LinUCB_click_position']]))
print(stats.ttest_rel([item[0] for item in t_test_patk['MOR_LinUCB_wop_click_position']], [item[0] for item in t_test_patk['SOR_dim2_LinUCB_click_position']]))
print(stats.ttest_rel([item[0] for item in t_test_patk['MOR_LinUCB_wop_click_position']], [item[0] for item in t_test_patk['SOR_dim1_LinUCB_click_position']]))



print("P@9")
print(stats.ttest_rel([item[9] for item in t_test_patk['MOR_LinUCB_wop_click_position']], [item[9] for item in t_test_patk['ParetoUCB1_click_position']]))
print(stats.ttest_rel([item[9] for item in t_test_patk['MOR_LinUCB_wop_click_position']], [item[9] for item in t_test_patk['MOR_LinUCB_click_position']]))
print(stats.ttest_rel([item[9] for item in t_test_patk['MOR_LinUCB_wop_click_position']], [item[9] for item in t_test_patk['SOR_dim3_LinUCB_click_position']]))
print(stats.ttest_rel([item[9] for item in t_test_patk['MOR_LinUCB_wop_click_position']], [item[9] for item in t_test_patk['SOR_dim2_LinUCB_click_position']]))
print(stats.ttest_rel([item[9] for item in t_test_patk['MOR_LinUCB_wop_click_position']], [item[9] for item in t_test_patk['SOR_dim1_LinUCB_click_position']]))





