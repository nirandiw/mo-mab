import numpy as np
import Weibo_Constants as cnst

class LinUCBArmStruct:
    def __init__(self, arm_row, lambda_, sim, init="random"):
        self.d = [arm_row.loc['dim1_len'], arm_row.loc['dim2_len'], arm_row.loc['dim3_len']]
        self.A = []
        self.b = []
        self.AInv = []
        self.ArmTheta = []
        for i in range(0, cnst.OBJECTIVES_i):
            self.A.append(lambda_ * np.identity(n=self.d[i]))
            self.b.append(np.zeros(self.d[i]))
            self.AInv.append(np.linalg.inv(self.A[i]))  # TODO: remove the hack
            if init == "random":
                self.ArmTheta.append(sim.get_random_coefficient_vector(self.d[i]))
            else:
                self.ArmTheta.append(np.zeros(self.d[i]))
        self.time = 0

    def update_parameters(self, articlepicked_featurevector, click, dim):
        articlepicked_featurevector = articlepicked_featurevector[~np.isnan(articlepicked_featurevector)]
        # print(articlepicked_featurevector)
        # print(dim)
        # print(click)
        self.A[dim] += np.outer(articlepicked_featurevector, articlepicked_featurevector)
        self.b[dim] += articlepicked_featurevector * click
        self.AInv[dim] = np.linalg.inv(self.A[dim])
        self.ArmTheta[dim] = np.dot(self.AInv[dim], self.b[dim])
        self.time += 1

    #def update_time(self):
    #    self.time += 1

    # def getTheta(self):
    #    return self.ArmTheta

    # def getA(self):
    #    return self.A

    def getProb(self, alpha, article_FeatureVector, dim):
        # print('getProb')
        article_FeatureVector = article_FeatureVector[~np.isnan(article_FeatureVector)] # todo assert that nan does not happen in the middle

        # print(article_FeatureVector)

        # print(self.ArmTheta[dim])
        # print(dim)
        if alpha == -1:
            alpha = 0.1 * np.sqrt(np.log(self.time + 1))  # check if the log value is correct
            # print("Alpha = ", alpha)
        mean = np.dot(self.ArmTheta[dim], article_FeatureVector)
        var = np.sqrt(np.dot(np.dot(article_FeatureVector, self.AInv[dim]), article_FeatureVector))
        pta = mean + alpha * var
        return pta

    def getProb_plot(self, alpha, article_FeatureVector, dim):
        mean = np.dot(self.ArmTheta[dim], article_FeatureVector)
        var = np.sqrt(np.dot(np.dot(article_FeatureVector, self.AInv[dim]), article_FeatureVector))
        pta = mean + alpha * var
        return pta, mean, alpha * var


class LinUCBMultiObjectiveAlgorithm:
    def __init__(self, simu_prng, df_arms_meta_data):
        # print(LinUCBMultiObjectiveAlgorithm)
        self.alpha = cnst.ALPHA
        self.ARMS = {}
        for (arm_index, arm_row) in df_arms_meta_data.iterrows():
            self.ARMS[arm_index] = LinUCBArmStruct(arm_row, cnst.LAMBDA, simu_prng)

    def get_estimated_rewards_for_all_arms(self, user_id, t_count, df_dim1, df_dim2, df_dim3_static, df_dim3_dynamix):
        # if t_count> 500:
            # self.alpha =-1
        est_rewards = {}  # key = arm id , value = list of rewards for each objective#

        for a in self.ARMS.keys():
            x_list = []
            # print(a)
            # print(user_id)
            x_list.append(np.array(df_dim1.loc[(a, user_id),:].tolist()))
            assert len(x_list[0]) == 5
            if user_id in df_dim2.index:
                x_list.append(np.array(df_dim2.loc[user_id, ['0', '1', '2', '3', '4', '5']].tolist()))
            else:
                x_list.append(np.array([0, 0, 0, 0, 0, 0]))
            assert len(x_list[1]) == 6
            # print(df_dim3_static.loc[user_id, :])
            if (user_id,a) in df_dim3_dynamix.index:
                b = np.array(df_dim3_dynamix.loc[(user_id, a),:].tolist())
            else:
                b = [0,0,0]
            # print(b)
            x_list.append(np.append(np.array(df_dim3_static.loc[user_id, :].tolist()), b))
            assert len(x_list[2]) == 6

            # print(x_list)
            assert (len(x_list)) == 3
            for idx, x_i in enumerate(x_list):
                x_i_pta = self.ARMS[a].getProb(self.alpha, x_i, idx)
                if est_rewards.has_key(a):
                    est_rewards[a].append(x_i_pta)
                else:
                    est_rewards[a] = [x_i_pta]
        return est_rewards

    def update_multiobjective_linucb(self, arm_id, user_id, df_dim1, df_dim2, df_dim3_static, df_dim3_dynamix, r_a):
        x_list =[]
        x_list.append(np.array(df_dim1.loc[arm_id, user_id].tolist())) # todo correct the columns
        assert len(x_list[0]) == 5

        if user_id in df_dim2.index:
            x_list.append(np.array(df_dim2.loc[user_id,['0', '1', '2', '3', '4', '5']].tolist()))
        else:
            x_list.append(np.array([0,0,0,0,0,0]))
        assert len(x_list[1]) == 6

        if (user_id, arm_id) in df_dim3_dynamix.index:
            b = np.array(df_dim3_dynamix.loc[(user_id, arm_id), :].tolist())
        else:
            b = [0, 0, 0]

        x_list.append(np.append(np.array(df_dim3_static.loc[user_id, :].tolist()), b))
        assert len(x_list[2]) == 6

        for i in range(0, cnst.OBJECTIVES_i):
            temp = self.ARMS.get(arm_id)
            # print(x_list)
            temp.update_parameters(x_list[i], r_a[i], i)



