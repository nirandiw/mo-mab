import pandas as pd

filepath = '.\\track1\\'
from sklearn.feature_extraction.text import CountVectorizer
import re
from scipy import sparse
import numpy as np
from scipy import spatial


'''Change the structure of the user_key_word.txt'''
def created_list_keyword_weight():
    with open(filepath + 'user_key_word.txt', 'r') as f:
        with open(filepath + 'user_key_word_modified.txt', 'w') as f_write:
            for line in f:
                user_key_w_list = re.split('\t|:|;', line.rstrip('\n'))
                user = user_key_w_list[0]
                keywords = user_key_w_list[1::2]
                weights = user_key_w_list[2::2]
                assert len(keywords) == len(weights)
                for idx, key in enumerate(keywords):
                    str = user + ',' + key + ',' + weights[idx] + '\n'
                    f_write.write(str)
    f.close()
    f_write.close()


def create_keywords_sparse_matrix():
    try:
        df_user_keywords_org = pd.read_csv(filepath + "user_key_word.txt", header=None, delimiter='\t')
        df_user_keywords_org.columns = ['userid', 'keywords']
        df_temp = pd.DataFrame(df_user_keywords_org['keywords']).applymap(
            lambda a: re.split(':|;', a))  # keywords and weights in a list
        df_only_keywords = pd.DataFrame(df_temp['keywords']).applymap(
            lambda a: ' '.join(a[0::2]))  # just the keywords as a string
        print(df_only_keywords.head())

        # fit sparse vector for the bag of keywords
        count_vector = CountVectorizer(token_pattern='\\b\\w+\\b', binary=True)
        K = count_vector.fit_transform(df_only_keywords['keywords'].tolist())
        assert K[0].getnnz() == len(df_temp['keywords'][0][0::2])
        features_indexes = count_vector.get_feature_names()

        # get keyword, weight as a list for all the users
        df_list_keywords_weights = pd.read_csv(filepath + 'user_key_word_modified.txt', delimiter=',', header=None)
        df_list_keywords_weights.columns = ['userid', 'keyword', 'weight']
        assert df_user_keywords_org.shape[0] == len(pd.unique(df_list_keywords_weights['userid']))

        col_index_dict = {} # created a dict with keyword and index to get O(1) lookup time
        for idx, key in enumerate(features_indexes):
            col_index_dict[str(key)] = idx

        df_list_keywords_weights['col_index'] = df_list_keywords_weights['keyword'].map(lambda a: col_index_dict[str(a)])
        df_list_keywords_weights['row_indx'] = pd.Categorical(df_list_keywords_weights['userid'].astype(str)).codes

        df_list_keywords_weights.to_csv(filepath+'user_key_word_sparse.csv')
        return True
    except AssertionError as assertionerror:
        print(assertionerror)
        return False
    except:
        print("Unknown error")
        return False


def get_user_profile(group, w_vector):
    user = group.follower[0]
    items = group.followee
    M = items[0]
    for item in items[1:]:
        M +=w_vector[item]
    M = M/len(items)
    print(M)

def simple_content_recommendation():
    is_process_keywords = False # set true if file needs to be generated
    is_create_sparse = False # set true if file needs to be generated

    if is_process_keywords:
        created_list_keyword_weight()

    if is_create_sparse:
        create_keywords_sparse_matrix()


    df_users_sparse = pd.read_csv(filepath+'user_key_word_sparse.csv', index_col=0)
    df_users = df_users_sparse['userid']

    data_k = df_users_sparse['keyword'].as_matrix()
    print(data_k)
    row = df_users_sparse['userid'].as_matrix()
    col = df_users_sparse['col_index'].as_matrix()
    user_keywords_sparse = sparse.csr_matrix((data_k,(row, col)))
    data_w = df_users_sparse['weight'].as_matrix()
    user_weights_sparse = sparse.csr_matrix((data_w,(row,col)))

    assert user_weights_sparse[100001].getnnz() == len(df_users_sparse[df_users_sparse['userid'] == 100001])

    # print(user_weights_sparse)


    # df_user_sns_org = pd.read_csv(filepath+'user_sns.txt', header=None, delimiter='\t')
    # df_user_sns_org.columns = ['follower', 'followee']
    # print(df_user_sns_org.shape)

    df_rec_log_train = pd.read_csv(filepath + 'rec_log_train.txt', header=None, delimiter='\t')
    df_rec_log_train.columns = ['userid','itemid','result','time']

    # print(df_rec_log_train.groupby('result').agg('count'))

    df_rec_log_test = pd.read_csv(filepath+'rec_log_test.txt',header=None, delimiter='\t')
    df_rec_log_test.columns = ['userid','itemid','result','time']
    print(df_rec_log_test.shape)
    print(df_rec_log_test.head())


    user_profiles ={}
    correct_count =0

    for idx, input_row in df_rec_log_train.iterrows():
        # print(input_row)
        user_no = input_row['userid']
        item_no = input_row['itemid']
        result = input_row['result']

        mat_user_dense = user_weights_sparse[user_no].toarray()
        mat_item_dense = user_weights_sparse[user_no].toarray()

        theta = spatial.distance.cosine(mat_user_dense,mat_item_dense)
        print(theta)
        rec_follw =-1
        if theta >0.5:
            rec_follw =1
        print(rec_follw)

        if rec_follw == df_rec_log_train.loc[idx, 'result']:
            correct_count += 1

        print(correct_count)

if __name__ == '__main__':

    df = pd.read_csv(filepath+'user_key_word_modified.txt', header=None)

    df_items = pd.read_csv(filepath+'item.txt', header=None, delimiter='\t')

    df_user_keywords = pd.read_csv(filepath+'user_key_word.txt', header=None, delimiter='\t')



    print(df.head())

    # print(df_solution.head())
    # print(df_solution.shape)
    # print(len(pd.unique(df_solution['id'])))

    # df_dups = pd.concat(g for _, g in df_solution.groupby('id') if len(g)>2)
    # print(df_dups)

    #df_rec_log_train = pd.read_csv(filepath + 'rec_log_train.txt', header=None, delimiter='\t')
    #df_rec_log_test.columns = ['userid', 'itemid', 'result', 'time']
    #print(df_rec_log_test.shape)
    #print(df_rec_log_test.loc[:20,:])

    #df = df_rec_log_test.loc[:200000, :]

    #print(df.groupby('userid').agg(['count']))
