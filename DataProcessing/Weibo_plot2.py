'''Plots the manhatten distance between the estimated r_a and the real r_a'''
import numpy as np
from scipy.interpolate import spline
import matplotlib.pyplot as plt
import File_Handler as fh

input_file_path = ".\\track1\\outputs\\"

def get_average(filenames, val):
    data_list = []
    for f in filenames:
        f = input_file_path + f
        data_list.append(fh.read_file_plot_2(f,val))
    a = np.array(data_list)
    avg = np.mean(a, axis=0)
    return avg


paretoucb1_files = ['2018-01-30-19-16-55_Weibo_ParetoUCB1.txt']
morlinucb_files =['2018-01-30-18-02-51_Weibo_MOR_LinUCB.txt']
morlinucb_wop_files = ['2018-05-10-11-57-23_Weibo_MOR_LinUCB_WO_Pareto_CIKM.txt']
# not relevant to single objective algorithms


morlinucb_data = get_average(morlinucb_files, "MOR_LinUCB_reward_estimation")
paretoucb1_data = get_average(paretoucb1_files, "ParetoUCB1_reward_estimation")
morlinucb_wop_data = get_average(morlinucb_wop_files, "MOR_LinUCB_wop_reward_estimation")


number_of_trials = 5001
x = np.array(range(1, number_of_trials))
x_smooth = np.linspace(x.min(), x.max(), number_of_trials)

morlinucb_smooth = spline(x, morlinucb_data, x_smooth)
paretoucb1_smooth = spline(x, paretoucb1_data, x_smooth)
morlinucb_wop_smooth = spline(x, morlinucb_wop_data, x_smooth)

print(len(morlinucb_smooth))
print(len(paretoucb1_smooth))

plt.figure(figsize=(5, 2), dpi=300)
plt.plot(x_smooth, morlinucb_smooth, label="MOR-LinUCB", linestyle='-', marker='*', markevery=500, linewidth=0.5)
plt.plot(x_smooth, paretoucb1_smooth, label="Ranked-PUCB1", linestyle='-', marker='d', markevery=500, linewidth=0.5)
plt.plot(x_smooth, morlinucb_wop_smooth, label="MOR-LinUCB-woP", linestyle='-', marker='d', markevery=500, linewidth=0.5)

plt.grid()
leg = plt.legend(fontsize=9, prop={'size': 8}, loc=0)
leg.get_frame().set_linewidth(0.0)
plt.xlabel('Number of trials')
plt.ylabel('Manhattan Distance')
plt.ylim(ymin=0)

plt.savefig('weibo_ctr_CIKM_2.pdf', bbox_inches='tight')  # fig with label
plt.show()