_author__ = 'nirandikawanigasekara'
import File_Handler as fh
import numpy as np
from scipy import stats


input_file_path = ".\\track1\\outputs\\"
paretoucb1_files = ["2018-01-25-03-12-52_Weibo_ParetoUCB1.txt", "2018-01-30-17-39-30_Weibo_ParetoUCB1.txt", "2018-01-30-19-16-55_Weibo_ParetoUCB1.txt"]
morlinucb_files =["2018-01-24-19-04-54_Weibo_MOR_LinUCB.txt", "2018-01-30-00-39-44_Weibo_MOR_LinUCB.txt","2018-01-30-18-02-51_Weibo_MOR_LinUCB.txt"]
morlinucb_wop_files = ["2018-05-10-11-57-23_Weibo_MOR_LinUCB_WO_Pareto_CIKM.txt","2018-05-20-19-50-40_Weibo_MOR_LinUCB_wop_CIKM.txt","2018-05-20-21-12-08_Weibo_MOR_LinUCB_wop_CIKM.txt" ]
sorlinucb_dim1_files = ['2018-05-11-13-14-43_Weibo_SOR_dim1_LinUCB_CIKM.txt', "2018-05-21-15-22-00_Weibo_SOR_dim1_LinUCB_CIKM.txt",
"2018-05-21-15-22-39_Weibo_SOR_dim1_LinUCB_CIKM.txt"]
sorlinucb_dim2_files = ["2018-05-13-23-24-18_Weibo_SOR_dim2_LinUCB_CIKM.txt","2018-05-21-04-25-45_Weibo_SOR_dim2_LinUCB_CIKM.txt","2018-05-21-04-27-42_Weibo_SOR_dim2_LinUCB_CIKM.txt"]
sorlinucb_dim3_files = ["2018-05-14-11-48-50_Weibo_SOR_dim3_LinUCB_CIKM.txt","2018-05-20-23-55-43_Weibo_SOR_dim3_LinUCB_CIKM.txt", "2018-05-21-03-59-16_Weibo_SOR_dim3_LinUCB_CIKM.txt"]

t_test_mrr ={}
def get_MRR(filenames, val):
    data_list = []
    mrr_trials = []

    for f in filenames:
        f = input_file_path+f
        click_lists = fh.read_file_plot_8(f,val)[-1]
        mrr_val =0
        for i,v in enumerate(click_lists):
            mrr_val = mrr_val + (v/(i+1.0))

        mrr_trials.append(mrr_val/sum(click_lists)) #

    t_test_mrr[val] = np.array(mrr_trials)
    print(t_test_mrr)
    avg = np.mean(np.array(mrr_trials), axis=0)
    return avg

morlinucb_mrr = get_MRR(morlinucb_files, 'MOR_LinUCB_click_position')
morlinucb_wop_mrr = get_MRR(morlinucb_wop_files, 'MOR_LinUCB_wop_click_position')
paretoucb_mrr = get_MRR(paretoucb1_files, 'ParetoUCB1_click_position')
sorlinucb_dim1_mrr = get_MRR(sorlinucb_dim1_files, "SOR_dim1_LinUCB_click_position")
sorlinucb_dim2_mrr = get_MRR(sorlinucb_dim2_files, "SOR_dim2_LinUCB_click_position")
sorlinucb_dim3_mrr = get_MRR(sorlinucb_dim3_files, "SOR_dim3_LinUCB_click_position")


print("MOR-LinUCB", morlinucb_mrr)
print("MOR-LinUCB-WOP", morlinucb_wop_mrr)
print("PUCB1",paretoucb_mrr)

print("LinUCB1",sorlinucb_dim1_mrr)
print("LinUCB2",sorlinucb_dim2_mrr)
print("LinUCB3",sorlinucb_dim3_mrr)

print(stats.ttest_rel(t_test_mrr['MOR_LinUCB_wop_click_position'], t_test_mrr['ParetoUCB1_click_position']))
print(stats.ttest_rel(t_test_mrr['MOR_LinUCB_wop_click_position'], t_test_mrr['MOR_LinUCB_click_position']))
print(stats.ttest_rel(t_test_mrr['MOR_LinUCB_wop_click_position'], t_test_mrr['SOR_dim1_LinUCB_click_position']))
print(stats.ttest_rel(t_test_mrr['MOR_LinUCB_wop_click_position'], t_test_mrr['SOR_dim2_LinUCB_click_position']))
print(stats.ttest_rel(t_test_mrr['MOR_LinUCB_wop_click_position'], t_test_mrr['SOR_dim3_LinUCB_click_position']))


