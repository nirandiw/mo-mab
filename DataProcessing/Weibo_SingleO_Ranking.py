import matlab.engine
import numpy as np
import pandas as pd
from sklearn.decomposition import TruncatedSVD
import re
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.metrics.pairwise import linear_kernel
from sklearn.metrics.pairwise import cosine_similarity
import random
import collections

import Weibo_Constants as cnst
import Weibo_SORanker as sornk
import Simulation as simulation
import datetime
import json
import os.path
from ast import literal_eval

filepath = '.\\track1\\'
dim1_feature_file_name = 'dim1_final_features.csv'
dim2_feature_file_name = 'dim2_final_user_no_svd_features.csv'
dim3_dynamic_feature_file_name = 'dim3_user_profile_dynamic.csv'
dim3_static_feature_file_name = 'dim3_user_profile_static.csv'
arm_meta_file_name = 'arm_meta_data.csv'
ground_truth_file_name = 'ground_truth.csv'


def generate_modified_user_keyword():
    df_user_keyword = pd.read_csv(filepath + 'user_key_word.txt', header=None, delimiter='\t')
    df_user_keyword.columns = ['userid', 'keywords_w']
    print(df_user_keyword.shape)
    df_user_keyword['keywords_w'] = df_user_keyword['keywords_w'].map(lambda x: x.split(';'))
    df_user_keyword['keywords_w'] = df_user_keyword['keywords_w'].map(lambda x: ':'.join(set(x)))
    df_user_keyword['keywords_w'] = df_user_keyword['keywords_w'].map(lambda x: x.split(':'))
    df_user_keyword['keywords'] = df_user_keyword['keywords_w'].map(lambda x: x[0::2])
    df_user_keyword['keyword_len'] = df_user_keyword['keywords'].map(lambda x: len(x))
    df_user_keyword['weights'] = df_user_keyword['keywords_w'].map(lambda x: x[1::2])
    print(df_user_keyword.shape)
    print(df_user_keyword.head())
    df_user_keyword.to_csv(filepath + 'modified_user_keyword.csv', index=False)


def build_user_item_weights_vector_per_row(row):
    print(row)
    u_k_w = []
    u_w = literal_eval(row['weights_y'])
    print(u_w)
    for item_keyword in literal_eval(row['keywords_x']):
        u_k_idx = literal_eval(row['keywords_y']).index(item_keyword) if item_keyword in literal_eval(
            row['keywords_y']) else -1
        if u_k_idx > -1:
            print(u_w[u_k_idx])
            u_k_w.append(float(u_w[u_k_idx]))
        else:
            u_k_w.append(0)
    print(u_k_w)
    arr_i_k_w = np.array(map(float, literal_eval(row.loc['weights_x'])))
    arr_u_i_k_w = np.multiply(arr_i_k_w, np.array(u_k_w)).tolist()
    return arr_u_i_k_w


def generate_dim1_features(df_s_users, df_items_dim1, testing):
    final_columns = ['kw_1', 'kw_2', 'kw_3', 'kw_4']
    if (testing):  # TODO Remove this condition after testing
        df_modified_user_keyword = pd.read_csv(filepath + "modified_user_keyword.csv")

        df_only_items_keywords = df_modified_user_keyword[
            df_modified_user_keyword.userid.isin(df_items_dim1.itemid)].reset_index(drop=True)

        assert df_only_items_keywords.shape[0] == df_items_dim1.shape[0]

        df_only_users_keywords = df_modified_user_keyword[
            df_modified_user_keyword.userid.isin(df_s_users.userid)].reset_index(drop=True)
        assert df_only_users_keywords.shape[0] == df_s_users.shape[0]

        if os.path.exists(filepath + "mergned_user_item_keywords.csv"):
            print("reading existing file for 1099 users and 675 items")
            df_merged_user_item_keywords = pd.read_csv(filepath + 'mergned_user_item_keywords.csv')
        else:
            df_only_users_keywords['track_key'] = 0
            df_only_items_keywords['track_key'] = 0
            df_merged_user_item_keywords = pd.merge(df_only_items_keywords, df_only_users_keywords,
                                                    on='track_key').drop(
                ['track_key'], axis=1)
            df_merged_user_item_keywords.to_csv(filepath + 'mergned_user_item_keywords.csv', index=False)
        print(df_merged_user_item_keywords.shape)

        df_merged_user_item_keywords['u_i_w_k'] = df_merged_user_item_keywords.apply(
            lambda row: build_user_item_weights_vector_per_row(row), axis=1)

        df_merged_user_item_keywords.to_csv(filepath + 'user_item_weighted_keywords.csv', index=False)

        print(df_merged_user_item_keywords.shape)

    df_modified_user_keyword = pd.read_csv(filepath + 'user_item_weighted_keywords.csv',
                                           converters={'u_i_w_k': literal_eval, 'userid_x': int, 'userid_y': int})
    print(df_modified_user_keyword.head(1))
    grouped = df_modified_user_keyword.groupby('userid_x')
    df_dim1_final_features = pd.DataFrame()
    for name, group in grouped:
        print(name)
        svd_keywords = TruncatedSVD(n_components=4, n_iter=7, random_state=42)

        mat_X = np.array(group['u_i_w_k'].tolist())
        svd_keywords_profile = svd_keywords.fit_transform(mat_X)
        df_svd_keywords = pd.DataFrame(svd_keywords_profile, columns=final_columns).reset_index(drop=True)
        df_svd_keywords['itemid'] = name
        df_svd_keywords['userid'] = pd.Series(group['userid_y']).reset_index(drop=True)
        print(df_svd_keywords.head(2))
        df_dim1_final_features = pd.concat([df_dim1_final_features, df_svd_keywords], axis=0)
        print(df_dim1_final_features.head(2))
    df_dim1_final_features.itemid = df_dim1_final_features.itemid.astype(int)
    df_dim1_final_features.userid = df_dim1_final_features.userid.astype(int)
    df_dim1_final_features.set_index(['itemid', 'userid'], append=False, inplace=True)
    print(df_dim1_final_features.head())
    df_dim1_final_features.to_csv(filepath + dim1_feature_file_name)

    '''

    for (i_idx, item_row) in df_only_items_keywords.iterrows():
        filepath_temp = filepath + '\\dim1_raw_features\\' + str(item_row.loc['userid']) + '_svd.txt'
        is_save_raw_features = False
        X_dim1 = np.array([])
        is_round_one = True
        print(item_row.loc['userid'])
        for (u_idx, dim1_user_row) in df_only_users_keywords.iterrows():
            u_k_w = []
            user_keywords = dim1_user_row.loc['keywords']
            user_weights = dim1_user_row.loc['weights']
            has_a_match = False
            for k in item_row.loc['keywords']:
                if k in user_keywords:
                    has_a_match = True
                    u_k_w.append(user_weights[user_keywords.index(k)])
                else:
                    u_k_w.append(0)
            # print(u_k_w)
            arr_u_k_w = np.array(list(map(float, u_k_w)))
            if has_a_match:
                arr_i_k_w = np.array(list(map(float, item_row.loc['weights'])))
                arr_u_i_k_w = [np.multiply(arr_i_k_w, arr_u_k_w)]
            else:
                arr_u_i_k_w = [arr_u_k_w]
            if is_round_one:
                X_dim1 = arr_u_i_k_w
                is_round_one = False
            else:
                X_dim1 = np.append(X_dim1, arr_u_i_k_w, axis=0)
        assert X_dim1.shape[0] == df_s_users.shape[0]

        if is_save_raw_features:
            np.savetxt(filepath_temp, X_dim1, delimiter=',', fmt='%.6f')
        if X_dim1.shape[1] > len(final_columns):
            svd_keywords = TruncatedSVD(n_components=len(final_columns), n_iter=7, random_state=42)
            svd_keywords_profile = svd_keywords.fit_transform(X_dim1)
            df_svd_keywords = pd.DataFrame(svd_keywords_profile, columns=final_columns)

        else:
            df_svd_keywords = pd.DataFrame(X_dim1, columns=final_columns[:X_dim1.shape[1]])

        item_meta_data[item_row.loc['userid']] = [len(df_svd_keywords.columns), 6, 6]  # TODO: remove static dim setting

        df_svd_keywords['itemid'] = item_row.loc['userid']
        df_svd_keywords['userid'] = df_only_users_keywords['userid']
        # df_svd_keywords.set_index(['itemid', 'userid'], append=False, inplace=True)
        print(df_svd_keywords.shape)
        # df_svd_keywords.to_csv(filepath)
        previous_shape = df_dim1_final_features.shape
        df_dim1_final_features = pd.concat([df_dim1_final_features, df_svd_keywords], axis=0)
        # print(df_dim1_final_features.head(4))
        assert df_dim1_final_features.shape[0] == previous_shape[0] + df_svd_keywords.shape[0]
        assert df_dim1_final_features.shape[1] == len(final_columns)+2  # constant  svd components

    df_dim1_final_features.itemid = df_dim1_final_features.itemid.astype(int)
    df_dim1_final_features.userid = df_dim1_final_features.userid.astype(int)
    df_dim1_final_features.set_index(['itemid', 'userid'], append=False, inplace=True)
    print(df_dim1_final_features.head())
    df_dim1_final_features.to_csv(filepath + dim1_feature_file_name)

    df_item_meta_data = pd.DataFrame.from_dict(item_meta_data, orient='index')

    df_item_meta_data.columns = ['itemid', 'dim1_len', 'dim2_len', 'dim3_len']
    df_item_meta_data.set_index('itemid', inplace=True, append=False)
    df_item_meta_data.to_csv(filepath + arm_meta_file_name)
    print(item_meta_data)
    '''


def generate_dim2_features(df_s_users, df_items_dim2):
    df_items_dim2['cat_lvl_1'] = df_items_dim2['cat'].map(lambda x: x.split('.')[0])
    df_items_dim2['cat_lvl_1_dup'] = df_items_dim2['cat_lvl_1']
    df_items_with_lvl_1_cat = pd.get_dummies(df_items_dim2, columns=['cat_lvl_1_dup'])
    print(df_items_with_lvl_1_cat.columns.values)

    df_user_sns = pd.read_csv(filepath + 'user_sns.txt', header=None, delimiter='\t')
    df_user_sns.columns = ['follower', 'followee']

    df_super_user_sns = df_user_sns[df_user_sns.follower.isin(df_s_users.userid)].reset_index(drop=True)
    df_user_sns_item_only = df_super_user_sns[df_super_user_sns.followee.isin(df_items_dim2.itemid)].reset_index(
        drop=True)
    groups_sns = df_user_sns_item_only.groupby(by=['follower'])
    user_cat_profile = {}
    cat_item_count = []
    cat_columns = list(df_items_with_lvl_1_cat)[4:]

    for name, g in groups_sns:
        df_user_followed_cat = df_items_with_lvl_1_cat[df_items_with_lvl_1_cat.itemid.isin(g.followee)]
        assert len(cat_columns) == df_user_followed_cat.shape[1] - 4
        mat_tags = df_user_followed_cat.as_matrix(columns=cat_columns)
        mat_avg_user_cat_profile = np.mean(mat_tags, axis=0)
        user_cat_profile[name] = mat_avg_user_cat_profile
        cat_item_count.append(len(g))

    print(cat_item_count)
    df_user_cat_profile = pd.DataFrame.from_dict(user_cat_profile, orient='index')
    assert df_user_cat_profile.shape[0] == len(cat_item_count)
    df_user_cat_profile.reset_index(inplace=True)
    df_user_cat_profile = pd.concat([df_user_cat_profile, pd.Series(cat_item_count)], axis=1, ignore_index=True)
    df_user_cat_profile.columns = ['userid', '0', '1', '2', '3', '4', '5', 'i_count']
    df_user_cat_profile.set_index('userid', append=False, inplace=True)
    print(df_user_cat_profile.shape)
    print(len(cat_item_count))
    assert df_user_cat_profile.shape[0] == len(cat_item_count)
    assert df_user_cat_profile.shape[1] == len(cat_columns) + 1

    # TODO  SVD is unnecessary until I use more categories. But I am keeping it for future use
    has_svd = False
    if has_svd:
        all_users_cat_profile = df_user_cat_profile.as_matrix()
        svd_cat = TruncatedSVD(n_components=5, n_iter=7, random_state=42)
        svd_all_users_cat_profile = svd_cat.fit_transform(all_users_cat_profile)
        df_svd_user_cat_profiles = pd.DataFrame(svd_all_users_cat_profile,
                                                columns=['svd_cat_1', 'svd_cat_2', 'svd_cat_3', 'svd_cat4',
                                                         'svd_cat_5'])
        assert df_svd_user_cat_profiles.shape[0] == df_user_cat_profile.shape[0]

        df_svd_user_cat_profiles = pd.concat([df_user_cat_profile, df_svd_user_cat_profiles], axis=1)
        filename = 'dim2_final_svd_features.csv'
        # tODO implement for items
    else:
        df_svd_user_cat_profiles = df_user_cat_profile
        filename = dim2_feature_file_name

    print(df_svd_user_cat_profiles.head())
    df_svd_user_cat_profiles.to_csv(filepath + filename)

    df_items_with_lvl_1_cat.set_index('itemid', inplace=True, append=False)
    df_items_with_lvl_1_cat.loc[:, cat_columns].to_csv(filepath + 'dim2_final_item_no_svd_features.csv')


def generate_dim3_features(df_s_users, df_items_dim3):
    df_users_profile = pd.read_csv(filepath + 'user_profile.txt', header=None, delimiter='\t')
    df_users_profile.columns = ['userid', 'byear', 'gender', 'n_tweets', 'tags']

    df_users_dim3 = df_users_profile[df_users_profile.userid.isin(df_s_users.userid)].reset_index(drop=True)

    assert df_users_dim3.shape[0] == df_s_users.shape[0]

    # calculating the age and get categories
    df_users_dim3['byear'] = df_users_dim3['byear'].replace(r"[0-9]*-[0-9]*-*", np.nan, regex=True)
    df_users_dim3['byear'] = pd.to_numeric(df_users_dim3['byear'])
    df_users_dim3.loc[df_users_dim3.byear > 2017, 'byear'] = np.nan
    df_users_dim3['age'] = 2017 - df_users_dim3['byear']
    df_users_dim3['age_cat'] = pd.cut(df_users_dim3['age'],
                                      [0, 19, 25, 30, 35, 40, 45, 50, 60, 100, 1000],
                                      labels=[1, 2, 3, 4, 5, 6, 7, 8, 9, 10], right=True)

    # categorizing the frequency of tweets. I used the quantile values to cut the categories. Based on the frequency
    # plot lot of users have zero number of tweets
    # fig, ax = plt.subplots()
    # df_users['n_tweets'].value_counts().plot(ax=ax, kind='bar')
    # fig.savefig( filepath)
    # print(df_users.max())
    # print(df_users.min())
    # print(df_users['n_tweets'].quantile(q=[0.4, 0.9]))

    n_tweets_cut = pd.cut(df_users_dim3['n_tweets'], [0, 32, 281, 2583], labels=['Low', 'Mediem', 'High'], retbins=True)
    df_users_dim3['n_tweets_cat'] = n_tweets_cut[0]

    # get dummy variable to categorical features, reduce dimension, write the features to file
    df_static_user_features_raw = pd.get_dummies(df_users_dim3, columns=['age_cat', 'gender', 'n_tweets_cat'],
                                                 prefix=['age_cat', 'gender', 'n_tweets'])
    static_feature_cols = list(df_static_user_features_raw)[5:]
    svd_user_profile = TruncatedSVD(n_components=3, n_iter=7, random_state=42)
    X = svd_user_profile.fit_transform(df_static_user_features_raw.as_matrix(columns=static_feature_cols))
    df_static_user_features_svd = pd.DataFrame(X, columns=['static_user_f1', 'static_user_f2', 'static_user_f3'])
    assert df_users_dim3.shape[0] == X.shape[0]
    df_users_dim3 = pd.concat([df_users_dim3['userid'], df_static_user_features_svd], axis=1)
    assert df_users_dim3.shape[1] == 1 + X.shape[1]
    assert df_users_dim3.shape[0] == X.shape[0]
    df_users_dim3.set_index('userid', append=False, inplace=True)
    df_users_dim3.to_csv(filepath + dim3_static_feature_file_name)

    # load and process the dynamic features for each user. Dynamic features are n_@actions, n_retweets, n_comments
    df_user_actoins = pd.read_csv(filepath + 'user_action.txt', header=None, delimiter='\t')
    df_user_actoins.columns = ['userid', 'itemid', 'n_at', 'n_retweets', 'n_comments']
    df_super_user_actions = df_user_actoins[
        df_user_actoins.userid.isin(df_s_users.userid)]  # consider only the super users
    print(df_super_user_actions.shape)
    df_super_user_actions = df_super_user_actions[df_super_user_actions.itemid.isin(df_items_dim3.itemid)].reset_index(
        drop=True)  # consider only the items followed by the super users
    print(df_super_user_actions.shape)
    print(len(pd.unique(df_super_user_actions.userid)))
    df_super_user_actions.set_index(['userid', 'itemid'], append=False, inplace=True)
    print(df_super_user_actions.head())
    df_super_user_actions.to_csv(filepath + dim3_dynamic_feature_file_name)


def get_super_users():
    df = pd.read_csv(filepath + 'KDD_Track1_solution.csv')
    df = df.loc[~df.clicks.isnull(), :]
    super_userids = pd.Series(pd.unique(df.id)).sample(frac=0.001, random_state=10).reset_index(
        drop=True)  # increase fraction to increase the number of users
    super_userids.to_csv(filepath + 'super_users_no_nan.csv')
    return super_userids


def generate_ground_truth(df_s_users, df_items_gt):
    df_solution = pd.read_csv(filepath + 'KDD_Track1_solution.csv')
    df_solution = df_solution[df_solution.id.isin(df_s_users.userid)]

    df_rec_log_train = pd.read_csv(filepath + 'rec_log_train.txt', header=None, delimiter='\t')
    df_rec_log_train.columns = ['userid', 'itemid', 'flag', 'time']
    df_rec_log_train = df_rec_log_train[df_rec_log_train.userid.isin(df_s_users.userid)]
    df_rec_log_train = df_rec_log_train[df_rec_log_train.itemid.isin(df_items_gt.itemid)]
    df_rec_log_train = df_rec_log_train[df_rec_log_train['flag'] == 1]

    # print(df_solution.head(3))
    group = df_solution.groupby('id')
    ground_truth = {}
    for name, g in group:
        vals = filter(lambda x: str(x) != 'nan', g.clicks.tolist())
        followed_items = ' '.join(vals)
        if name in df_rec_log_train['userid']:
            followed_items_rec_log_train = df_rec_log_train.loc[df_rec_log_train['userid'] == name, 'itemid'].tolist()
            followed_items = followed_items.join(followed_items_rec_log_train)
        ground_truth[name] = followed_items

    df_ground_truth = pd.DataFrame.from_dict(ground_truth, orient='index')
    df_ground_truth.columns = ['followed_items']
    print(df_ground_truth.head(3))
    df_ground_truth.to_csv(filepath + ground_truth_file_name)


def get_pareto_front(est_rewards, eng):
    O_t = list(est_rewards.values())
    # O_t_narr = np.array(O_t)
    matlab_O_t = matlab.double(O_t)
    pareto_front = eng.paretoFront(matlab_O_t)  # indexed from 1
    pareto_flatten_indexes = []
    # print "YYYY", pareto_front
    if isinstance(pareto_front, float):
        return [est_rewards.keys()[int(pareto_front) - 1]]
    for sublist in pareto_front:
        for item in sublist:
            pareto_flatten_indexes.append(est_rewards.keys()[int(item) - 1])
    return pareto_flatten_indexes


def generate_keyword_similarity(df_s_users, df_items_keyword_sim):
    df_keywords = pd.read_csv(filepath + 'user_key_word.txt', header=None, delimiter='\t')
    df_keywords.columns = ['id', 'keywords']

    df_item_only_keywords = df_keywords[df_keywords.id.isin(df_items_keyword_sim.itemid)]
    df_users_only_keywords = df_keywords[df_keywords.id.isin(df_s_users.userid)]
    df_all_keywords = pd.concat([df_item_only_keywords, df_users_only_keywords], axis=0)
    assert (df_item_only_keywords.shape[0] + df_users_only_keywords.shape[0]) == df_all_keywords.shape[0]

    df_all_keywords = df_all_keywords.sort_values(by=['id']).reset_index(drop=True)

    df_all_keywords['keywords'] = df_all_keywords['keywords'].map(lambda a: re.split(':|;', a))
    df_all_keywords['keywords_dict'] = df_all_keywords['keywords'].map(lambda b: dict(zip(b[0::2], b[1::2])))
    df_all_keywords['k'] = df_all_keywords['keywords'].map(lambda c: ' '.join(c[0::2]))  # keywords only as a string
    print(df_all_keywords.head())
    print(df_all_keywords.loc[0, 'keywords_dict']['58013'])

    count_vector = CountVectorizer(token_pattern='\\b\\w+\\b', binary=True)
    K = count_vector.fit_transform(df_all_keywords['k'].tolist())

    feature_list = count_vector.get_feature_names()
    feature_list = [x.encode('UTF8') for x in feature_list]
    print(feature_list)

    print(df_all_keywords['keywords_dict'].keys())
    # print(df_all_keywords.loc[8381,'keywords_dict'])
    # print(df_all_keywords.loc[8394,'keywords_dict'])

    for f in feature_list:
        print(f)
        df_all_keywords[f] = df_all_keywords['keywords_dict'].map(lambda x: x.get(f) if f in x.keys() else 0)
        # np.where(f in df_all_keywords['keywords_dict'].keys(), df_all_keywords['keywords_dict'][f],0)

    assert df_all_keywords.shape[1] == K.shape[1] + 4

    arr_key = df_all_keywords.as_matrix(columns=feature_list)
    svd_keywords = TruncatedSVD(n_components=5, n_iter=7, random_state=45)
    svd_arr_key = svd_keywords.fit_transform(arr_key)

    df_svd_key_sim = pd.DataFrame(svd_arr_key, columns=['ksim1', 'ksim2', 'ksim3', 'ksim4', 'ksim5'])
    df_svd_key_sim['id'] = df_all_keywords['id']
    df_svd_key_sim.set_index('id', inplace=True, append=False)
    print(df_svd_key_sim.head())
    df_svd_key_sim.to_csv(filepath + 'dim1_similarity_svd_matrix.csv')


def get_keyword_similarity_for_reward_estimation(df_s_users, df_items_keyword_cos_sim):
    print('get_keyword_similarity_for_reward_estimatio')
    df_all_keywords = pd.read_csv(filepath + 'dim1_similarity_svd_matrix.csv')
    df_user_k = df_all_keywords[df_all_keywords.id.isin(df_s_users.userid)].reset_index(drop=True)
    df_item_k = df_all_keywords[df_all_keywords.id.isin(df_items_keyword_cos_sim.itemid)].reset_index(drop=True)

    arr_user_keywords = df_user_k[['ksim1', 'ksim2', 'ksim3', 'ksim4', 'ksim5']].as_matrix()
    arr_item_keywords = df_item_k[['ksim1', 'ksim2', 'ksim3', 'ksim4', 'ksim5']].as_matrix()
    cosine_similarity_ = cosine_similarity(arr_user_keywords, arr_item_keywords)

    df_cos_sim = pd.DataFrame(cosine_similarity_)
    assert df_cos_sim.shape[0] == df_user_k['id'].shape[0]
    assert df_cos_sim.shape[1] == df_item_k['id'].shape[0]

    df_cos_sim.columns = list(df_item_k.loc[:, 'id'])
    # df_cos_sim.reset_index(drop=True)
    df_cos_sim['userid'] = df_user_k.loc[:, 'id']
    df_cos_sim.set_index('userid', inplace=True, append=False)
    # print(df_cos_sim.shape)
    # print(df_cos_sim.head())
    # TODO: Consider calculating similarity based on followed items instead of static keyword similarity

    return df_cos_sim


def get_keyword_similarity(item_id, user, df_keyword_cos_sim):
    u_i_cos_similarity = df_keyword_cos_sim.loc[user, item_id]
    if u_i_cos_similarity > 0.5:
        return 1
    else:
        return 0


def get_category_similarity(_item_id, _user, df_cat_similarity):
    print('get_category_similarity')
    print(df_cat_similarity.head(3))
    r_dim2 = 0
    if _user in df_cat_similarity.index:
        u_i_cat_con_sim = df_cat_similarity.loc[_user, _item_id]
        if u_i_cat_con_sim > 0.5:
            r_dim2 = 1
    else:
        r_dim2 = 1
        df_item_cat = pd.read_csv(filepath + 'dim2_final_item_no_svd_features.csv')
        df_item_cat.set_index('itemid', append=False, inplace=True)
        arr_new_user_cat_profile = df_item_cat.loc[_item_id].as_matrix(
            columns=['cat_lvl_1_dup_1', 'cat_lvl_1_dup_2', 'cat_lvl_1_dup_4', 'cat_lvl_1_dup_6', 'cat_lvl_1_dup_7',
                     'cat_lvl_1_dup_8'])

        arr_item_cat_profiles = df_item_cat.as_matrix(
            columns=['cat_lvl_1_dup_1', 'cat_lvl_1_dup_2', 'cat_lvl_1_dup_4', 'cat_lvl_1_dup_6', 'cat_lvl_1_dup_7',
                     'cat_lvl_1_dup_8'])
        print(arr_item_cat_profiles.shape)

        # print(arr_new_user_cat_profile.shape)
        # print(arr_new_user_cat_profile.reshape(1,-1))
        t = df_cat_similarity.shape[0]
        h = cosine_similarity(arr_new_user_cat_profile.reshape(1, -1), arr_item_cat_profiles)
        # print(h.shape)
        df_h = pd.DataFrame(h)
        df_h.columns = df_item_cat.index
        # assert df_cos_sim.shape[0] == df_user_k['id'].shape[0]
        # assert df_cos_sim.shape[1] == df_item_k['id'].shape[0]
        df_cat_similarity = pd.concat([df_cat_similarity, df_h], axis=0)
        assert df_cat_similarity.shape[0] == t + 1
        print(df_cat_similarity.head(2))

        # TODO write the function to update everything. Coz one only 1 followed user is sufficient to skip this condition
    return (r_dim2, df_cat_similarity)


def get_category_similarity_for_reward_estimation():
    df_cat_item = pd.read_csv(filepath + 'dim2_final_item_no_svd_features.csv')
    arr_item_cats = df_cat_item.as_matrix(
        columns=['cat_lvl_1_dup_1', 'cat_lvl_1_dup_2', 'cat_lvl_1_dup_4', 'cat_lvl_1_dup_6', 'cat_lvl_1_dup_7',
                 'cat_lvl_1_dup_8'])
    df_cat_users = pd.read_csv(filepath + 'dim2_final_user_no_svd_features.csv')
    arr_user_cats = df_cat_users.as_matrix(columns=['0', '1', '2', '3', '4', '5'])

    assert arr_user_cats.shape[1] == arr_item_cats.shape[1]

    cat_cosine_similarity = cosine_similarity(arr_user_cats, arr_item_cats)

    df_cat_cos_similarity = pd.DataFrame(cat_cosine_similarity)

    assert df_cat_cos_similarity.shape[0] == df_cat_users.shape[0]
    assert df_cat_cos_similarity.shape[1] == df_cat_item.shape[0]

    df_cat_cos_similarity.columns = list(df_cat_item.loc[:, 'itemid'])
    df_cat_cos_similarity['userid'] = df_cat_users['userid']
    df_cat_cos_similarity.set_index('userid', inplace=True, append=False)

    # print(df_cat_cos_similarity.head())
    # print(df_cat_cos_similarity.shape)

    return df_cat_cos_similarity


def get_items_user_profile_similarity(_item_id, _user, df_sim):
    print('get_items_user_profile_similarity')
    print(df_sim.head(3))
    idx = [_user, _item_id]
    r_dim3 = 0
    if not df_sim.index.isin([(_user, _item_id)]).any():
        idx = [_user, 0]
    if _item_id in df_sim.columns:  # todo generate similarrity for all the items
        s = df_sim.loc[idx, _item_id].tolist()[0]
    else:
        s = 0
    if s > 0.5:
        r_dim3 = 1
    return r_dim3


def get_user_profile_similarity_for_reward_estimation(df_s_users, df_i):
    # print('get_user_profile_similarity_for_reward_estimation')
    df_s_u_static = pd.read_csv(filepath + dim3_static_feature_file_name)
    df_s_u_dynamic = pd.read_csv(filepath + dim3_dynamic_feature_file_name)
    df_s_u_both = df_s_u_static.merge(df_s_u_dynamic, how='outer')
    df_s_u_both.fillna(0, inplace=True)
    df_s_u_both['itemid'] = df_s_u_both.itemid.astype(int)
    # print(df_s_u_both.head())

    df_sns = pd.read_csv(filepath + 'user_sns.txt', header=None, delimiter='\t')
    df_sns.drop_duplicates(inplace=True)
    df_sns.columns = ['follower', 'followee']
    df_sns = df_sns[df_sns.follower.isin(df_s_users.userid)].reset_index(drop=True)
    df_sns = df_sns[df_sns.followee.isin(df_i.itemid)].reset_index(drop=True)
    # print(df_sns.shape)

    item_group = df_sns.groupby('followee')
    dict_item_profile = {}

    # based on item's followed user's we generate an average profile for each item's user based
    for item, g in item_group:
        df_st = df_s_u_both[df_s_u_both.userid.isin(g.follower)]
        arr_st_mean = np.mean(df_st.as_matrix(columns=['static_user_f1', 'static_user_f2', 'static_user_f3']), axis=0)
        assert arr_st_mean.shape[0] == 3
        df_dy = df_s_u_both[df_s_u_both.itemid.isin(g.followee)]
        if df_dy.empty:
            arr_dy_mean = np.array([0, 0, 0])
        else:
            arr_dy_mean = np.mean(df_dy.as_matrix(columns=['n_at', 'n_retweets', 'n_comments']), axis=0)
        assert arr_dy_mean.shape[0] == 3

        arr_both = np.append(arr_st_mean, arr_dy_mean)
        assert arr_both.shape[0] == 6
        dict_item_profile[item] = arr_both

    # print(dict_item_profile[516096])
    df_item_profiles = pd.DataFrame.from_dict(dict_item_profile, orient='index')

    arr_item_data = df_item_profiles.as_matrix()
    arr_user_data = df_s_u_both.as_matrix(
        columns=['static_user_f1', 'static_user_f2', 'static_user_f3', 'n_at', 'n_retweets', 'n_comments'])

    user_profile_cos_sim = cosine_similarity(arr_user_data, arr_item_data)

    df_profile_cos_sim = pd.DataFrame(user_profile_cos_sim)
    df_profile_cos_sim.columns = list(df_item_profiles.index)
    df_profile_cos_sim['userid'] = df_s_u_both.userid
    df_profile_cos_sim['itemid'] = df_s_u_both.itemid
    df_profile_cos_sim.set_index(['userid', 'itemid'], inplace=True, append=False)
    assert df_profile_cos_sim.shape == (df_s_u_both.shape[0], df_item_profiles.shape[0])

    return df_profile_cos_sim


if __name__ == '__main__':

    IS_FEATURE_GENERATION = False
    IS_GROUND_TRUTH_GENERATION = False
    GENERATE_SIMILARITY_MATRIX = False
    GENERATE_MODIFIED_USER_KEYWORD = False

    if GENERATE_MODIFIED_USER_KEYWORD:
        generate_modified_user_keyword()

    df_users = pd.read_csv(filepath + 'user_profile.txt', header=None, delimiter='\t')
    df_users.columns = ['userid', 'birthyear', 'gender', 'notweet', 'tags']

    df_items = pd.read_csv(filepath + 'item.txt', header=None, delimiter='\t')
    df_items.columns = ['itemid', 'cat', 'w_keywords']

    super_users_ids = get_super_users()
    df_super_users = df_users[df_users.userid.isin(super_users_ids)].reset_index(drop=True)

    df_temp2 = pd.read_csv(filepath + 'KDD_Track1_solution.csv')
    df_temp2 = df_temp2[df_temp2.id.isin(super_users_ids)]
    df_temp2 = df_temp2[~df_temp2.clicks.isnull()]
    super_user_items = []
    # print(df_temp2.clicks.tolist())

    '''Get the list of items clicked by all the super users'''
    for l in df_temp2.clicks.tolist():
        for i in l.split(' '):
            super_user_items.append(i)

    df_items = df_items[df_items.itemid.isin(super_user_items)]

    all_items_list = df_items['itemid'].tolist()

    print(df_super_users.shape)
    print(df_items.shape)

    if IS_FEATURE_GENERATION:
        generate_dim1_features(df_super_users, df_items, False)
        generate_dim2_features(df_super_users, df_items)
        generate_dim3_features(df_super_users, df_items)

    print("End Feaute Generation")

    if IS_GROUND_TRUTH_GENERATION:
        generate_ground_truth(df_super_users, df_items)

    print("End Ground Truth Generation")

    if GENERATE_SIMILARITY_MATRIX:
        generate_keyword_similarity(df_super_users, df_items)

    print("End Similarity Generation")

    # load the feature dataframes

    df_dim1_features = pd.read_csv(filepath + dim1_feature_file_name)  # index = itemid, userid
    df_dim1_features.set_index(['itemid', 'userid'], inplace=True, append=False)
    print(df_dim1_features.head(1))
    df_dim2_features = pd.read_csv(filepath + dim2_feature_file_name)  # index userid
    df_dim2_features.set_index('userid', inplace=True, append=False)
    print(df_dim2_features.head(1))
    df_dim3_features_static = pd.read_csv(filepath + dim3_static_feature_file_name)  # userid not an index yet
    df_dim3_features_static.set_index('userid', append=False, inplace=True)
    print(df_dim3_features_static.head(1))
    df_dim3_feature_dynamic = pd.read_csv(filepath + dim3_dynamic_feature_file_name)  # userid, itemid
    df_dim3_feature_dynamic.set_index(['userid', 'itemid'], append=False, inplace=True)
    print(df_dim3_feature_dynamic.head(1))
    df_arms_meta_data = pd.read_csv(filepath + arm_meta_file_name)
    df_arms_meta_data = df_arms_meta_data[df_arms_meta_data.itemid.isin(df_items.itemid)]
    df_arms_meta_data.set_index('itemid', inplace=True, append=False)
    print(df_arms_meta_data.head(1))
    df_ground_truth = pd.read_csv(filepath + ground_truth_file_name)
    df_ground_truth.columns = ['userid', 'followed_items']
    df_ground_truth.set_index('userid', inplace=True, append=False)
    print(df_ground_truth.head(1))

    NUMBER_OF_TRIALS = 5000

    sim = simulation.Simulation(-1)

    # eng = matlab.engine.start_matlab()

    df_keyword_cos_sim = get_keyword_similarity_for_reward_estimation(df_super_users, df_items)
    df_cat_cos_sim = get_category_similarity_for_reward_estimation()
    df_user_profile_sim = get_user_profile_similarity_for_reward_estimation(df_super_users, df_items)

    for runs in range(0, 1):

        # dimension, alpha, lambda_, n,
        ranker = sornk.SingleORankedBanditAlgorithm(cnst.RANKING_POSITIONS, sim, df_super_users.userid.tolist(), 5) # 5 since im checking for the first dimension
        click_connt = [0]
        click_position_count = [0] * cnst.RANKING_POSITIONS
        ranker_count = [0] * cnst.RANKING_POSITIONS
        empty_gt_count = 0
        l = 0
        reward_estimation_delta = []

        for t in range(0, NUMBER_OF_TRIALS):
            user_row = df_super_users.loc[random.sample(df_super_users.index, 1)]
            user = user_row.loc[user_row.index[0], 'userid']

            A_tilde = []  # the ranked list

            for k in range(0, cnst.RANKING_POSITIONS):
                # a_k = ranker.get_a_k(A_prime, k, A_tilde, est_reward_vectors_for_all_A, user, all_items_list)
                # a_k = ranker.get_a_k_3(k, A_tilde, df_dim3_features_static, df_dim3_feature_dynamic, user, all_items_list)
                # a_k = ranker.get_a_k_2(k, A_tilde, df_dim2_features, user, all_items_list)
                a_k = ranker.get_a_k_1(k, A_tilde, df_dim1_features, user, all_items_list)



                if a_k is None:
                    print('Something wrong in ranking')
                    exit(-1)
                else:
                    A_tilde.append(a_k)  # TODO check if this works as expected

            print(A_tilde)

            # start the evaluation
            if user in df_ground_truth.index:
                user_ground_truth = df_ground_truth.loc[user, 'followed_items'].split(' ')
            else:
                empty_gt_count += 1
                user_ground_truth = []
            user_ground_truth = map(int, user_ground_truth)
            print('USERS GROUND TRUTH')
            print(user_ground_truth)
            l += len(user_ground_truth)
            t_click_count = 0
            tmp_reward_estimation_delta = 0
            for k_rank, item in enumerate(A_tilde):
                f_k = 0
                item_id = item.keys()[0]
                x_list = []
                # r_a_boolean = [0, 0, 0]
                print(item_id)
                if item_id in user_ground_truth:
                    click_position_count[k_rank] = click_position_count[k_rank] + 1
                    t_click_count += 1
                    print("CORRECT RECOMMENDATION ", item_id)

                    if item[item_id] == 1:
                        fk = 1
                        ranker_count[k_rank] = ranker_count[k_rank] + 1

                #if (user, item_id) in df_dim3_feature_dynamic.index:
                #    feature_v = df_dim3_feature_dynamic.loc[(user, item_id), :].tolist()
                #else:
                  #  feature_v = [0, 0, 0]
                # feature_v = feature_v+ df_dim3_features_static.loc[user].tolist()

                if (item_id,user) in df_dim1_features.index:
                    feature_v = df_dim1_features.loc[(item_id,user), :].tolist()
                else:
                  feature_v = [0,0,0,0,0]

                ranker.update_mab_k(feature_v, f_k, k_rank, user)
            click_connt.append(click_connt[len(click_connt) - 1] + t_click_count)
            reward_estimation_delta.append(tmp_reward_estimation_delta)

        print("clicks")
        print(l)
        # assert l == click_connt
        print(ranker_count)
        print(empty_gt_count)

        ctr = []
        for i, val in enumerate(click_connt):
            y = val / (i + 1.0)
            ctr.append(y)
        print("SOR__dim1_LinUCB_ctr = ", ctr)

        output_filepath = '.\\track1\\outputs\\'
        filename = output_filepath + str(
            datetime.datetime.now().strftime('%Y-%m-%d-%H-%M-%S_{fname}').format(
                fname="Weibo_SOR_dim1_LinUCB_CIKM.txt"))

        with open(filename, 'w') as f:
            f.write("{\"SOR_dim1_LinUCB_RBA\":")
            f.write(str(ranker_count) + "}\n")
            f.write("{\"SOR_dim1_LinUCB_click_position\":")
            f.write(str(click_position_count) + "}\n")
            # f.write("{\"MOR_LinUCB_click_count\":")
            # f.write(str(article_click_count) + "}\n")
            f.write("{\"SOR_dim1_LinUCB_ctr\":")
            f.write(str(ctr) + "}\n")
            f.write("{\"SOR_dim1_LinUCB_reward_estimation\":")
            f.write(str(reward_estimation_delta) + "}\n")
            f.write(json.dumps({'#Objectives': cnst.OBJECTIVES_i,
                                'Arms': cnst.ARMS_COUNT,
                                'RankingPositions': cnst.RANKING_POSITIONS,
                                'User': cnst.NO_OF_USER,
                                'Lambda': cnst.LAMBDA,
                                'Alpha': cnst.ALPHA,
                                'Trials': cnst.T,
                                'Beta': cnst.BETA}))

    # eng.quit()

# TODO General questions
# 1. dim feature if not good get from the bileaner estimator
# 2. get_dummies drop_first = True
# check dataframe assignments
# when using "followed" data may be its better to use the whole user set.
# super users should have at least 1 followed item
# updating user features dynamically
