import numpy as np
from scipy.interpolate import spline
import matplotlib.pyplot as plt
import File_Handler as fh
from matplotlib.font_manager import FontProperties
from scipy import stats
import seaborn

input_file_path = ".\\track1\\outputs\\"

t_test_ctr = {}

def get_average(filenames, val):
    data_list = []
    i=0
    for f in filenames:
        f = input_file_path+f
        ctr_vals = fh.read_file_plot_1(f, val)[0:5001]
        data_list.append(ctr_vals)
        print(len(data_list[i]))
        i = i+1


        if val in t_test_ctr.keys():
            t_test_ctr[val].append(np.mean(ctr_vals))
        else:
            t_test_ctr[val] = [np.mean(ctr_vals)]


    a = np.array(data_list)
    print(a)
    avg = np.mean(a, axis=0)
    return avg


'''
paretoucb1_files = ["2018-01-25-03-12-52_Weibo_ParetoUCB1.txt"]
morlinucb_files =["2018-01-24-19-04-54_Weibo_MOR_LinUCB.txt"]
morlinucb_wop_files = ['2018-05-10-11-57-23_Weibo_MOR_LinUCB_WO_Pareto_CIKM.txt']
sorlinucb_dim1_files = ['2018-05-11-13-14-43_Weibo_SOR_dim1_LinUCB_CIKM.txt']
sorlinucb_dim2_files = ['2018-05-13-23-24-18_Weibo_SOR_dim2_LinUCB_CIKM.txt']
sorlinucb_dim3_files = ['2018-05-14-11-48-50_Weibo_SOR_dim3_LinUCB_CIKM.txt']
'''


paretoucb1_files = ["2018-01-25-03-12-52_Weibo_ParetoUCB1.txt", "2018-01-30-17-39-30_Weibo_ParetoUCB1.txt", "2018-01-30-19-16-55_Weibo_ParetoUCB1.txt"]
morlinucb_files =["2018-01-24-19-04-54_Weibo_MOR_LinUCB.txt", "2018-01-30-00-39-44_Weibo_MOR_LinUCB.txt","2018-01-30-18-02-51_Weibo_MOR_LinUCB.txt"]
morlinucb_wop_files = ["2018-05-10-11-57-23_Weibo_MOR_LinUCB_WO_Pareto_CIKM.txt","2018-05-20-19-50-40_Weibo_MOR_LinUCB_wop_CIKM.txt","2018-05-20-21-12-08_Weibo_MOR_LinUCB_wop_CIKM.txt" ]
sorlinucb_dim1_files = ['2018-05-11-13-14-43_Weibo_SOR_dim1_LinUCB_CIKM.txt', "2018-05-21-15-22-00_Weibo_SOR_dim1_LinUCB_CIKM.txt",
"2018-05-21-15-22-39_Weibo_SOR_dim1_LinUCB_CIKM.txt"]
sorlinucb_dim2_files = ["2018-05-13-23-24-18_Weibo_SOR_dim2_LinUCB_CIKM.txt","2018-05-21-04-25-45_Weibo_SOR_dim2_LinUCB_CIKM.txt","2018-05-21-04-27-42_Weibo_SOR_dim2_LinUCB_CIKM.txt"]
sorlinucb_dim3_files = ["2018-05-14-11-48-50_Weibo_SOR_dim3_LinUCB_CIKM.txt","2018-05-20-23-55-43_Weibo_SOR_dim3_LinUCB_CIKM.txt", "2018-05-21-03-59-16_Weibo_SOR_dim3_LinUCB_CIKM.txt"]


# morlinucb_one_user_files = ["2018-01-23-23-50-12_MOR_LinUCB.txt", "2018-01-23-23-51-33_MOR_LinUCB.txt",
# "2018-01-23-23-52-56_MOR_LinUCB.txt","2018-01-23-23-54-18_MOR_LinUCB.txt","2018-01-24-00-10-51_MOR_LinUCB.txt"]
# paretoucb1_one_user_files =["2018-01-23-23-51-04_ParetoUCB1.txt","2018-01-23-23-52-27_ParetoUCB1.txt",
# "2018-01-23-23-53-51_ParetoUCB1.txt","2018-01-23-23-55-11_ParetoUCB1.txt","2018-01-24-00-12-58_ParetoUCB1.txt"]


morlinucb_data = get_average(morlinucb_files, "MOR_LinUCB_ctr")
paretoucb1_data = get_average(paretoucb1_files, "ParetoUCB1_ctr")
morlinucb_wop_data = get_average(morlinucb_wop_files, "MOR_LinUCB_wop_ctr")
sorlinucb_dim1_data = get_average(sorlinucb_dim1_files, "SOR_dim1_LinUCB_ctr")
sorlinucb_dim2_data = get_average(sorlinucb_dim2_files, "SOR_dim2_LinUCB_ctr")
sorlinucb_dim3_data = get_average(sorlinucb_dim3_files, "SOR_dim3_LinUCB_ctr")

# paretoucb1_one_user_data = get_average(paretoucb1_one_user_files, "ParetoUCB1_ctr")

number_of_trials = 5001
x = np.array(range(1, number_of_trials))
x_smooth = np.linspace(x.min(), x.max(), number_of_trials)

morlinucb_smooth = spline(x, morlinucb_data[1:], x_smooth)
paretoucb1_smooth = spline(x, paretoucb1_data[1:], x_smooth)
morlinucb_wop_smooth = spline(x,morlinucb_wop_data[1:],x_smooth)
sorlinucb_dim1_smooth = spline(x, sorlinucb_dim1_data[1:], x_smooth)
sorlinucb_dim2_smooth = spline(x, sorlinucb_dim2_data[1:], x_smooth)
sorlinucb_dim3_smooth = spline(x, sorlinucb_dim2_data[1:], x_smooth)
# paretoucb1_one_smooth = spline(x, paretoucb1_one_user_data, x_smooth)

plt.figure(figsize=(5, 3), dpi=300)
#plt.plot(x_smooth, morlinucb_smooth, label="MOR-LinUCB", linestyle='-', marker='*', markevery=500)
plt.plot(x_smooth, paretoucb1_smooth, label="Ranked-PUCB1",  marker='d', markevery=500, color='g')
plt.plot(x_smooth, morlinucb_wop_smooth, label="MOR-LinUCB",  marker='x', markevery=500, color='y')
plt.plot(x_smooth, sorlinucb_dim1_smooth, label="Ranked-LinUCB-obj1", marker='+', markevery=500, color='c')
plt.plot(x_smooth, sorlinucb_dim2_smooth, label="Ranked-LinUCB-obj2", marker='s', markevery=500, color='coral')
plt.plot(x_smooth, sorlinucb_dim3_smooth, label="Ranked-LinUCB-obj3", marker='v', markevery=500, color='darkolivegreen')



plt.xlabel('Number of trials')
plt.ylabel('CTR')
plt.ylim(ymin=0)
plt.legend(loc=9)


plt.savefig('weibo_ctr_CIKM_1.pdf', bbox_inches='tight')  # fig with label
plt.show()


print(t_test_ctr)

print(stats.ttest_rel(t_test_ctr['MOR_LinUCB_wop_ctr'], t_test_ctr['MOR_LinUCB_ctr']))
print(stats.ttest_rel(t_test_ctr['MOR_LinUCB_wop_ctr'], t_test_ctr['ParetoUCB1_ctr']))
print(stats.ttest_rel(t_test_ctr['MOR_LinUCB_wop_ctr'], t_test_ctr['SOR_dim1_LinUCB_ctr']))
print(stats.ttest_rel(t_test_ctr['MOR_LinUCB_wop_ctr'], t_test_ctr['SOR_dim2_LinUCB_ctr']))
print(stats.ttest_rel(t_test_ctr['MOR_LinUCB_wop_ctr'], t_test_ctr['SOR_dim3_LinUCB_ctr']))


print(np.mean(t_test_ctr['MOR_LinUCB_ctr']))
print(np.mean(t_test_ctr['MOR_LinUCB_wop_ctr']))
print(np.mean(t_test_ctr['ParetoUCB1_ctr']))
print(np.mean(t_test_ctr['SOR_dim1_LinUCB_ctr']))
print(np.mean(t_test_ctr['SOR_dim2_LinUCB_ctr']))
print(np.mean(t_test_ctr['SOR_dim3_LinUCB_ctr']))


