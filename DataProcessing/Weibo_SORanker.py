import numpy as np
import Weibo_Constants as cnst


class LinUCBUserStruct:
    def __init__(self, featureDimension, lambda_, sim, init="random"):
        self.d = featureDimension
        self.A = lambda_ * np.identity(n=self.d)
        self.b = np.zeros(self.d)
        self.AInv = np.linalg.inv(self.A)
        if (init == "random"):
            self.UserTheta = sim.get_random_coefficient_vector(self.d)
        else:
            self.UserTheta = np.zeros(self.d)
        self.time = 0

    def updateParameters(self, articlePicked_FeatureVector, click):
        feat = np.array(articlePicked_FeatureVector)
        self.A += np.outer(feat, feat)
        self.b += feat * click
        self.AInv = np.linalg.inv(self.A)
        self.UserTheta = np.dot(self.AInv, self.b)
        self.time += 1


    def getProb(self, alpha, article_FeatureVector):
        if alpha == -1:
            alpha = 0.1 * np.sqrt(np.log(self.time + 1))
        mean = np.dot(self.UserTheta, article_FeatureVector)
        var = np.sqrt(np.dot(np.dot(article_FeatureVector, self.AInv), article_FeatureVector))
        pta = mean + alpha * var
        return pta




class N_LinUCBAlgorithm:
    def __init__(self, dimension, alpha, lambda_, user_list, sim, init="random"):  # n is number of users
        self.users = {}
        # algorithm have n users, each user has a user structure
        for user in user_list:
            self.users[user] = LinUCBUserStruct(dimension, lambda_, sim, init)

        self.dimension = dimension
        self.alpha = alpha

        self.CanEstimateUserPreference = False
        self.CanEstimateCoUserPreference = True
        self.CanEstimateW = False
        self.CanEstimateV = False

    def decide_3(self, pool_articles, userID, df_features_s, df_features_d):
        maxPTA = float('-inf')
        articlePicked = None

        for x in pool_articles:

            if (userID,x) in df_features_d.index:
                feature = df_features_d.loc[(userID, x), :].tolist()
            else:
                feature = [0, 0, 0]
            feature = feature + df_features_s.loc[userID,:].tolist()
            print(feature)
            x_pta = self.users.get(userID).getProb(self.alpha, feature)
            # pick article with highest Prob
            if maxPTA < x_pta:
                articlePicked = x
                maxPTA = x_pta

        return articlePicked

    def decide_2(self, pool_articles, userID, df_features_2):
        maxPTA = float('-inf')
        articlePicked = None

        for x in pool_articles:

            if (userID) in df_features_2.index:
                feature = df_features_2.loc[(userID), :].tolist()
            else:
                feature = [0, 0, 0,0,0,0,0]
            print(feature)
            x_pta = self.users.get(userID).getProb(self.alpha, feature)
            # pick article with highest Prob
            if maxPTA < x_pta:
                articlePicked = x
                maxPTA = x_pta

        return articlePicked


    def decide_1(self, pool_articles, userID, df_features_1):
        maxPTA = float('-inf')
        articlePicked = None

        for x in pool_articles:

            if (x,userID) in df_features_1.index:
                feature = df_features_1.loc[(x,userID), :].tolist()
            else:
                feature = [0,0,0,0,0]
            print(feature)
            x_pta = self.users.get(userID).getProb(self.alpha, feature)
            # pick article with highest Prob
            if maxPTA < x_pta:
                articlePicked = x
                maxPTA = x_pta

        return articlePicked

    def updateParameters(self, articlePicked, click, userID):
        self.users.get(userID).updateParameters(articlePicked, click)




class SingleORankedBanditAlgorithm:
    def __init__(self, pos, sim_, userids, dim):
        self.K = pos
        self.dict_MABs = {}
        self.sim = sim_
        for k in range(0, self.K):
            self.dict_MABs[k] = N_LinUCBAlgorithm(dim, cnst.BETA, cnst.LAMBDA, userids, sim_)

    def get_a_k_3(self, k, ranked_a_list, df_features_s, df_features_d, user_id, all_item_ids):
        linucb_k = self.dict_MABs[k]
        ranked_arms = set().union(*(a.keys() for a in ranked_a_list))
        dict_a_k = {}
        f = 0

        a_k_hat = linucb_k.decide_3(all_item_ids, user_id, df_features_s, df_features_d)
        unselected_a = list(set(all_item_ids).symmetric_difference(ranked_arms))

        if a_k_hat in ranked_arms:
            a_k = self.sim.get_random_val_from_list(unselected_a)
            assert a_k != a_k_hat
        else:
            a_k = a_k_hat
            f = 1
        dict_a_k[a_k] = f
        return


    def get_a_k_2(self, k, ranked_a_list, df_features_2, user_id, all_item_ids):
        linucb_k = self.dict_MABs[k]
        ranked_arms = set().union(*(a.keys() for a in ranked_a_list))
        dict_a_k = {}
        f = 0

        a_k_hat = linucb_k.decide_2(all_item_ids, user_id, df_features_2)
        unselected_a = list(set(all_item_ids).symmetric_difference(ranked_arms))

        if a_k_hat in ranked_arms:
            a_k = self.sim.get_random_val_from_list(unselected_a)
            assert a_k != a_k_hat
        else:
            a_k = a_k_hat
            f = 1
        dict_a_k[a_k] = f
        return dict_a_k

    def get_a_k_1(self, k, ranked_a_list, df_features_1, user_id, all_item_ids):
        linucb_k = self.dict_MABs[k]
        ranked_arms = set().union(*(a.keys() for a in ranked_a_list))
        dict_a_k = {}
        f = 0

        a_k_hat = linucb_k.decide_1(all_item_ids, user_id, df_features_1)
        unselected_a = list(set(all_item_ids).symmetric_difference(ranked_arms))

        if a_k_hat in ranked_arms:
            a_k = self.sim.get_random_val_from_list(unselected_a)
            assert a_k != a_k_hat
        else:
            a_k = a_k_hat
            f = 1
        dict_a_k[a_k] = f
        return dict_a_k




    def update_mab_k(self, user_context, click_reward_boolean, k, user_id):
        linucb_k = self.dict_MABs[k]
        linucb_k.updateParameters(user_context, click_reward_boolean, user_id)
