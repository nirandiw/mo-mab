import pandas as pd
from sklearn.decomposition import TruncatedSVD
from sklearn.feature_extraction.text import CountVectorizer

filepath = ".\\track1\\"

'''Processing the keywords for items.
    First get the dummy variables using the bagofwords
    Second reduce the dimension'''


def process_item_keywords(df):
    # 1
    df_item_keywords_dummies = pd.DataFrame(df['B'])

    df_item_keywords = pd.DataFrame(df['Keywords'])

    df_item_keywords = df_item_keywords.applymap(lambda c: c.replace(';',' '))
    count_vector = CountVectorizer(token_pattern='\\b\\w+\\b', binary=True)
    K = count_vector.fit_transform(df_item_keywords['Keywords'].tolist())
    print(K[0])

    df_item_keywords_dummies = pd.concat([df_item_keywords_dummies, pd.DataFrame(K.toarray())], axis=1)
    print(df_item_keywords_dummies.head())

    # 2
    svd = TruncatedSVD(n_components=5, n_iter=7, random_state=42)
    X = df_item_keywords_dummies.as_matrix(columns=list(df_item_keywords_dummies)[1:])
    X_new = svd.fit_transform(X)

    df_item_keywords_svd_ = pd.DataFrame(X_new, columns=['keywords_4000_1', 'keywords_4000_2', 'keywords_4000_3',
                                                         'keywords_4000_4', 'keywords_4000_5'])
    return df_item_keywords_svd_


'''Processing the categories for items.
    First, derive unique string for each category at each level
    Second, convert the categorical values to dummy variables.
    Third, dimension reduction to 4 done seperately for each cetegory level.
   '''


def process_item_cetegory(df_cat):
    # 1
    df_item_cat = df_cat['Cat'].str.split('.')
    for index, cat_list in enumerate(df_item_cat):
        try:
            assert len(cat_list) < 5
        except AssertionError:
            print(index, len(cat_list))
        temp1 = cat_list[0] + "." + cat_list[1]
        if (len(cat_list) > 2):
            temp2 = cat_list[0] + "." + cat_list[1] + "." + cat_list[2]
            cat_list[2] = temp2
        cat_list[1] = temp1

    df_item_cat = pd.DataFrame(df_item_cat.tolist(), columns=('Cat1', 'Cat2', 'Cat3', 'Cat4'), index=df_cat.index)
    df_item_cat['Cat4'] = df_cat['Cat']
    print(df_item_cat.head(), df_item_cat.shape)

    # 2
    df_item_cat_dummies = pd.get_dummies(df_item_cat, columns=['Cat1', 'Cat2', 'Cat3', 'Cat4'],
                                         prefix=['Cat1', 'Cat2', 'Cat3', 'Cat4'])
    # print(df_item_cat_dummies.head(), df_item_cat_dummies.shape)
    # 3
    svd_cat = TruncatedSVD(n_components=4, n_iter=7, random_state=42)

    CAT = df_item_cat_dummies.as_matrix()
    CAT_new = svd_cat.fit_transform(CAT)

    df_item_cat_svd_ = pd.DataFrame(CAT_new, columns=['CAT1', 'CAT2', 'CAT3', 'CAT4'])
    return df_item_cat_svd_


'''This method is used to combine the item user's features with the information given in the user_profile.txt file'''


def process_item_attributes(df_m):
    df_org_user = pd.read_csv(filepath + "user_profile.txt", delimiter='\t', header=None)
    df_org_user.columns = ['B', 'bday', 'gender', 'tweets', 'tags']
    print(df_org_user.shape)

    df_user_item_attr = pd.merge(df_m, df_org_user, on='B', how='inner')

    df_user_item_attr['age'] = 2017 - pd.to_numeric(df_user_item_attr['bday'],
                                                    errors='coerce')  # TODO coerce is incorrect
    print("User merged with item data")
    print(df_user_item_attr.head())
    print(df_user_item_attr.shape)

    df_item_attr_dummies = pd.get_dummies(df_user_item_attr, columns=['gender'], prefix=['gender'])
    return df_item_attr_dummies


def processing_items():
    filepath = ".\\track1\\"

    df_org_item = pd.read_csv(filepath + "item.txt", delimiter='\t', header=None)
    df_org_item.columns = ['B', 'Cat', 'Keywords']

    df_org_user_action = pd.read_csv(filepath + "user_action.txt", delimiter='\t', header=None)
    df_org_user_action.columns = ['A', 'B', 'action_tweet', 'retweet', 'comment']
    print(df_org_user_action.shape)
    print(df_org_user_action[df_org_user_action.duplicated('B') == True].count())

    df_item_followers_count = df_org_user_action.groupby('B', as_index=False, sort=False).size().reset_index(
        name='nofollower')
    print(df_item_followers_count.head())

    df_item_additional_features = df_org_user_action.groupby('B', as_index=False, sort=False).sum()

    df_merge = pd.merge(df_org_item, df_item_additional_features, on='B', how='inner')
    df_merge = pd.merge(df_merge, df_item_followers_count, on='B', how='inner')

    print(df_merge.head(), df_merge.shape)

    '''Processing the categories for items'''
    df_item_cat_svd = process_item_cetegory(df_merge)

    # merging
    df_merge = pd.concat([df_merge, df_item_cat_svd], axis=1)
    print(df_merge.head(), df_merge.shape)

    '''Processing the keywords for items'''
    df_item_keywords_svd = process_item_keywords(df_merge)
    # merging
    df_merge = pd.concat([df_merge, df_item_keywords_svd], axis=1)
    print(df_merge.head(), df_merge.shape)

    '''Processing the user attributes for items'''
    df_merge = process_item_attributes(df_merge)
    print(df_merge.head(), df_merge.shape)

    return df_merge


if __name__ == '__main__':



    processing_items().to_csv(filepath+'item_final.csv')




    # process actions - think how to model this
    # process the keywords

    # merge with the svd tags

