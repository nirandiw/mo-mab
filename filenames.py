import numpy as np
from scipy.interpolate import spline
import matplotlib.pyplot as plt
import File_Handler as fh
from scipy import  stats
import os, glob

t_test_ctr = {}

def get_average(filenames, val):
    data_list = []
    for f in filenames:
        ctr_vals = fh.read_file_plot_1(f,val)
        data_list.append(ctr_vals)
        if val in t_test_ctr.keys():
            t_test_ctr[val].append(np.mean(ctr_vals))
        else:
            t_test_ctr[val] = [np.mean(ctr_vals)]


    a = np.array(data_list)
    avg = np.mean(a, axis=0)
    return avg

# paretoucb1_files = ["2018-01-24-10-20-42_ParetoUCB1.txt"]
# morlinucb_files = ["2018-01-24-10-19-45_MOR_LinUCB.txt"]

'''
paretoucb1_files = ["2018-01-23-23-32-07_ParetoUCB1.txt", "2018-01-23-23-37-57_ParetoUCB1.txt", # org
                    "2018-01-23-23-39-22_ParetoUCB1.txt", "2018-01-23-23-40-37_ParetoUCB1.txt",
                    "2018-01-23-23-41-46_ParetoUCB1.txt",

                    '2018-01-24-10-20-42_ParetoUCB1.txt', '2018-01-24-10-38-31_ParetoUCB1.txt', # obj
                    '2018-01-24-10-39-59_ParetoUCB1.txt',
                    '2018-01-24-12-05-01_ParetoUCB1.txt', '2018-01-24-10-45-03_ParetoUCB1.txt',
                    '2018-01-24-10-48-17_ParetoUCB1.txt',
                    '2018-01-24-10-50-35_ParetoUCB1.txt', '2018-01-24-10-53-25_ParetoUCB1.txt',
                    '2018-01-24-10-56-15_ParetoUCB1.txt',

                    '2018-01-24-15-59-28_ParetoUCB1.txt', '2018-01-24-16-02-40_ParetoUCB1.txt', # context
                    '2018-01-24-16-09-37_ParetoUCB1.txt', '2018-01-24-10-45-03_ParetoUCB1.txt',
                    '2018-01-24-16-11-40_ParetoUCB1.txt', '2018-01-24-16-13-45_ParetoUCB1.txt',
                    '2018-01-24-16-16-25_ParetoUCB1.txt', '2018-01-24-16-18-39_ParetoUCB1.txt',
                    '2018-01-24-16-24-20_ParetoUCB1.txt',

                    #'2018-01-24-17-15-44_ParetoUCB1.txt',
                    # '2018-01-24-17-19-47_ParetoUCB1.txt', # users
                    # '2018-01-24-17-21-50_ParetoUCB1.txt', '2018-01-24-17-31-21_ParetoUCB1.txt',
                    # '2018-01-24-17-36-09_ParetoUCB1.txt', '2018-01-24-17-40-15_ParetoUCB1.txt',
                    # '2018-01-24-17-46-32_ParetoUCB1.txt', '2018-01-24-18-06-04_ParetoUCB1.txt',
                    # '2018-01-24-18-08-23_ParetoUCB1.txt','2018-01-24-18-10-46_ParetoUCB1.txt'
                    ]


morlinucb_files =["2018-01-23-23-23-55_MOR_LinUCB.txt", "2018-01-23-23-36-39_MOR_LinUCB.txt", # org
                  "2018-01-23-23-38-27_MOR_LinUCB.txt","2018-01-23-23-39-50_MOR_LinUCB.txt",
                  "2018-01-23-23-41-01_MOR_LinUCB.txt",

                  '2018-01-24-10-19-45_MOR_LinUCB.txt', '2018-01-24-10-37-44_MOR_LinUCB.txt', # obj
                   '2018-01-24-10-39-08_MOR_LinUCB.txt',
                   '2018-01-24-10-43-58_MOR_LinUCB.txt', '2018-01-24-10-47-02_MOR_LinUCB.txt',
                   '2018-01-24-10-49-13_MOR_LinUCB.txt',
                   '2018-01-24-10-52-11_MOR_LinUCB.txt', '2018-01-24-10-54-58_MOR_LinUCB.txt',
                   '2018-01-24-12-03-52_MOR_LinUCB.txt',

                  '2018-01-24-15-58-12_MOR_LinUCB.txt', '2018-01-24-16-01-30_MOR_LinUCB.txt', # context
                  '2018-01-24-16-08-31_MOR_LinUCB.txt','2018-01-24-10-43-58_MOR_LinUCB.txt',
                  '2018-01-24-16-10-35_MOR_LinUCB.txt', '2018-01-24-16-12-40_MOR_LinUCB.txt',
                  '2018-01-24-16-15-21_MOR_LinUCB.txt', '2018-01-24-16-17-33_MOR_LinUCB.txt',
                  '2018-01-24-16-23-01_MOR_LinUCB.txt'

                 # '2018-01-24-17-14-27_MOR_LinUCB.txt',
                 # '2018-01-24-17-18-37_MOR_LinUCB.txt', # users
                 # '2018-01-24-17-20-41_MOR_LinUCB.txt','2018-01-24-17-30-12_MOR_LinUCB.txt',
                 # '2018-01-24-18-38-59_MOR_LinUCB.txt', '2018-01-24-18-40-37_MOR_LinUCB.txt',
                 # '2018-01-24-18-43-52_MOR_LinUCB.txt', '2018-01-24-18-45-03_MOR_LinUCB.txt',
                 # '2018-01-24-18-46-12_MOR_LinUCB.txt','2018-01-24-18-48-00_MOR_LinUCB.txt
                  ]






morlinucb_wop_files = ["2018-05-14-23-11-13_MOR_LinUCB_wop.txt","2018-05-14-23-12-39_MOR_LinUCB_wop.txt",
                       "2018-05-14-23-12-49_MOR_LinUCB_wop.txt", "2018-05-14-23-13-00_MOR_LinUCB_wop.txt",
                       "2018-05-14-23-13-12_MOR_LinUCB_wop.txt"]




sorlinucb_files = ["2018-05-15-11-10-41_SOR_LinUCB.txt", "2018-05-15-11-13-02_SOR_LinUCB.txt",
                   "2018-05-20-10-13-11_SOR_LinUCB.txt", "2018-05-20-10-16-52_SOR_LinUCB.txt",
                   "2018-05-20-10-17-25_SOR_LinUCB.txt"]


'''


# morlinucb_one_user_files = ["2018-01-23-23-50-12_MOR_LinUCB.txt", "2018-01-23-23-51-33_MOR_LinUCB.txt",
# "2018-01-23-23-52-56_MOR_LinUCB.txt","2018-01-23-23-54-18_MOR_LinUCB.txt","2018-01-24-00-10-51_MOR_LinUCB.txt"]
# paretoucb1_one_user_files =["2018-01-23-23-51-04_ParetoUCB1.txt","2018-01-23-23-52-27_ParetoUCB1.txt",
# "2018-01-23-23-53-51_ParetoUCB1.txt","2018-01-23-23-55-11_ParetoUCB1.txt","2018-01-24-00-12-58_ParetoUCB1.txt"]


os.chdir("/Users/nirandikawanigasekara/Documents/Python/MO-MAB/mo-mab")

paretoucb1_files = []
morlinucb_files = []
morlinucb_wop_files = []
sorlinucb_files = []

for filename in glob.glob("2018-06-26*.txt"):
    print(filename)
    if "MOR_LinUCB_wop" in filename:
        morlinucb_wop_files.append(filename)
    elif "ParetoUCB1" in filename:
        paretoucb1_files.append(filename)
    elif "SOR_LinUCB" in filename:
        sorlinucb_files.append(filename)
    elif "MOR_LinUCB" in filename:
        morlinucb_files.append(filename)


morlinucb_data = [x/10.0 for x in get_average(morlinucb_files, "MOR_LinUCB_ctr")]
paretoucb1_data = [x/10.0 for x in get_average(paretoucb1_files, "ParetoUCB1_ctr")]
morlinucb_wop_data = [ x/10.0 for x in get_average(morlinucb_wop_files, "MOR_LinUCB_wop_ctr")]
sorlinucb_data = [x/10.0 for x in get_average(sorlinucb_files, "SOR_LinUCB_ctr")]

print("PLOP")


print(t_test_ctr)

print(stats.ttest_rel(t_test_ctr['MOR_LinUCB_ctr'], t_test_ctr['MOR_LinUCB_wop_ctr']))
print(stats.ttest_rel(t_test_ctr['MOR_LinUCB_ctr'], t_test_ctr['ParetoUCB1_ctr']))
print(stats.ttest_rel(t_test_ctr['MOR_LinUCB_ctr'], t_test_ctr['SOR_LinUCB_ctr']))



print(np.mean(t_test_ctr['MOR_LinUCB_ctr'])/10)
print(np.mean(t_test_ctr['MOR_LinUCB_wop_ctr'])/10)
print(np.mean(t_test_ctr['ParetoUCB1_ctr'])/10)
print(np.mean(t_test_ctr['SOR_LinUCB_ctr'])/10)

