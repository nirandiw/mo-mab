import json

def write_predicted_y_(p, y, filename):
    with open(filename, 'a+') as f:
        f.write(p+":"+y)
    f.close()

def read_file_plot_1(filename, s):
    count =0
    info_list = None
    with open(filename, 'r') as f:
        for line in f:
            if '_ctr' in line:
                print(line)
                assert count == 0
                info_list = json.loads(line)
                print(info_list[s])
                count +=1
    f.close()
    return info_list[s]

def read_file_plot_2(filename, s):
    count =0
    info_list = None
    with open(filename, 'r') as f:
        for line in f:
            if '_reward_estimation' in line:
                assert count == 0
                info_list = json.loads(line)
                print(info_list[s])
                count +=1
    f.close()
    return info_list[s]

def read_file_plot_3_tot_clicks(s,filename):
    count =0
    info_list = None
    with open(filename, 'r') as f:
        for line  in f:
            if '_click_position' in line:
                assert count == 0
                info_list = json.loads(line)
                print(info_list[s][-1])
                count += 1
    f.close()
    return info_list[s][-1]


def read_file_plot_3_RBA_clicks(s,filename):
    count =0
    info_list = None
    with open(filename, 'r') as f:
        for line in f:
            if '_RBA' in line:
                assert count == 0
                info_list = json.loads(line)
                print(info_list[s][-1])
                count += 1
    f.close()
    return info_list[s][-1]


def read_file_plot_meta_data(file_n, param):
    count =0
    meta_data = []
    with open(file_n, 'r') as f:
        for line in f:
            if param in line:
                assert count == 0
                meta_data = json.loads(line)
                print(meta_data[param])
                count += 1
    return meta_data[param]

def read_file_plot_8(filename, s):
    count = 0
    info_list = None
    with open(filename, 'r') as f:
        for line in f:
            if '_click_position' in line:
            #if '_RBA' in line:
                assert count == 0
                info_list = json.loads(line)
                count += 1
    f.close()
    return info_list[s]


if __name__ == '__main__':
    read_file_plot_3_RBA_clicks('MOR_LinUCB_RBA')
    read_file_plot_3_tot_clicks('MOR_LinUCB_click_position')


