import Simulation as simulation
import TripAdvisorModule.Constants as cnst
import TripAdvisorModule.ParetoUCB1_Simulation_Tripadvisor as paretoucb
import TripAdvisorModule.MOR_LinUCB_wop_Simulation_Tripadvisor as morlinucb_wop
import TripAdvisorModule.Ranked_LinUCB_Tripadvisor as sorlinucb
import TripAdvisorModule.MORLogUCB_Tripadvisor as morlogucb
import TripAdvisorModule.Epsilon_greegy_Simulation as epsilongreedy
import matlab.engine


if __name__ == '__main__':
    item_ids = None

    for i in range(0, 10):
        sim = simulation.Simulation(-1)
        dict_m, dict_c = sim.get_optimal_coefficient_vector()
        dict_beta = sim.get_optimal_user_coefficient_vectors()
        dict_x_lists = {}
        dict_user_ids = {}
        matlab_eng = matlab.engine.start_matlab()
        item_ids = range(0,cnst.ARMS_COUNT)

        paretoucb_sim = paretoucb.ParetoUCB1Sim(item_ids)
        morlogucb_sim = morlogucb.MORLogUCB(item_ids)
        sorlinucb_sim = sorlinucb.SORSim(item_ids, 1)
        morlinucb_wop_sim = morlinucb_wop.MORLinUCBwopSim(item_ids)
        epsilon_greedy_sim = epsilongreedy.EpsilonGreedySim(item_ids, 0.4)

        algos = {'Ranked-PUCB1': paretoucb_sim,
                 'MORLogUCB': morlogucb_sim,
                 'Ranked-LinUCB':sorlinucb_sim,
                 'MORLinUCB': morlinucb_wop_sim,
                 'EpsilonGreedy': epsilon_greedy_sim}

        for t in range(1, cnst.T):
            x_list = sim.sample_context_vector()
            user_id = sim.get_random_val_from_list(dict_beta.keys())

            print("t: ", t, "x_list: ", x_list, "user_id: ", user_id)

            user_beta = dict_beta[user_id]

            for algo in algos.values():

                ranked_list = algo.recommend(x_list, user_id, None)
                rank = 0
                print(user_id)
                for dict_item in ranked_list:
                    recommended_a = dict_item.keys()[0]
                    if recommended_a is None:
                        print('check recommended_a is none')
                    array_rates = cnst.get_objective_rewards(dict_m, recommended_a, x_list, dict_c, sim)
                    is_click = cnst.get_real_f_k(array_rates, user_beta)
                    algo.update(rank, dict_item, array_rates, is_click, x_list, None, t, user_id)
                    rank = rank + 1

        for algo in algos.values():
            algo.dump_results()
            algo.get_ctr()
